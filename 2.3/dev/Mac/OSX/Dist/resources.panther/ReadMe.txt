This package will install the second build of
the MacPython 2.3 additions for Mac OS X 10.3. 

Installation requires approximately 3.3 MB of disk
space, ignore the message that it will take zero bytes.

You must install onto your current boot disk, even
though the installer does not enforce this, otherwise
things will not work.

This installer does not contain a Python engine, as
Apple already includes that in 10.3. It does contain
a set of programs to allow easy access to it for Mac 
users (an integrated development environment, a Python 
extension package manager) and the waste module. 

The installer puts the applications in MacPython-2.3 in
your Applications folder.

The PythonIDE application has a Help command that gets
you started quickly with MacPython and contains
references to other documentation.

Changes since the first build:
- The startup crash of the IDE some people experienced
  has been fixed. The IDE Scripts folder is now in
  ~/Library/Python/IDE-Scripts.
- The full Python documentation can now be installed
  through the Package Manager.

More information on MacPython can be found at
http://www.cwi.nl/~jack/macpython.html, more
information on Python in general at
http://www.python.org.
