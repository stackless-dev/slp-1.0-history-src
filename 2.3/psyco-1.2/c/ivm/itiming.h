 /***************************************************************/
/***                   Measuring processor time                ***/
 /***************************************************************/


/* Only implemented for processor-specific ways to measure time
   spent in Python, so it does not apply for the virtual machine.
   Psyco's profilers will revert to clock(). */
