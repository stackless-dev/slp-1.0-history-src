"""
runAll.py

Runs all testcases in files named test_*.py.
Should be run in the folder Stackless/unittests.
"""


import os, sys, glob, unittest


def makeSuite():
    "Build a test suite of all available test files."

    suite = unittest.TestSuite()
    sys.path.insert(0, '.')
    
    for filename in glob.glob('test_*.py'):
        modname = os.path.splitext(os.path.basename(filename))[0]
        module = __import__(modname)
        tests = unittest.TestLoader().loadTestsFromModule(module)
        suite.addTest(tests)

    return suite


if __name__ == '__main__':
    testSuite = makeSuite()
    unittest.TextTestRunner().run(testSuite)
