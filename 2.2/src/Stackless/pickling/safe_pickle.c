#include "Python.h"
#ifdef STACKLESS

#include "compile.h"

#include "core/stackless_impl.h"

/* safe pickling */

static int(*cPickle_save)(PyObject *, PyObject *, int) = NULL;

static PyObject *
pickle_callback(PyFrameObject *f, PyObject *retval)
{
	PyThreadState *ts = PyThreadState_GET();
	PyTaskletObject *cur = ts->st.current;
	PyCStackObject *cst = cur->cstate;
	PyObject *self, *args;
	int pers_save, ret;
	if (!PyArg_ParseTuple(retval, "OOi", &self, &args, &pers_save))
		return NULL;
	ret = cPickle_save(self, args, pers_save);
	cur->tempval = PyInt_FromLong(ret);
	/* jump back */
	ts->frame = f->f_back;
	Py_DECREF(f);
	slp_transfer_return(cst);
	/* never come here */
	return NULL;
}

static int pickle_M(PyObject *self, PyObject *args, int pers_save);

int
slp_safe_pickling(int(*save)(PyObject *, PyObject *, int),
		  PyObject *self, PyObject *args, int pers_save)
{
	PyThreadState *ts = PyThreadState_GET();
	PyTaskletObject *cur = ts->st.current;
	PyCStackObject *stub = ts->st.initial_stub;
	PyObject *retval = NULL;
	int ret = -1;
	PyObject *cb_args = NULL;
	PyBaseFrameObject *bf = NULL;
	PyCStackObject *cst;

	cPickle_save = save;

	if (ts->st.main == NULL)
		return pickle_M(self, args, pers_save);

	cb_args = Py_BuildValue("OOi", self, args, pers_save);

	if (cb_args == NULL)
		goto finally;

	cur->tempval = cb_args;
	bf = slp_baseframe_new(pickle_callback, 1, 0);
	if (bf == NULL)
		goto finally;
	ts->frame = (PyFrameObject *) bf;
	cst = cur->cstate;
	cur->cstate = NULL;
	if (slp_transfer(&cur->cstate, stub, cur))
		return -1; /* fatal */
	Py_XDECREF(cur->cstate);
	cur->cstate = cst;
	retval = cur->tempval;
	cur->tempval = NULL;
	if (retval == NULL)
		goto finally;
	if (PyInt_Check(retval))
		ret = PyInt_AsLong(retval);
finally:
	Py_XDECREF(retval);
	Py_XDECREF(cb_args);
	return ret;
}

/* safe unpickling is not needed */


/* 
 * the following stuff is only needed in the rare case that we are
 * run without any initialisation. In this case, we don't save stack
 * but use slp_eval_frame, which initializes everything.
 */

static PyObject *_self, *_args;
static int _pers_save;

static PyObject *
pickle_runmain(PyFrameObject *f, PyObject *retval)
{
	PyThreadState *ts = PyThreadState_GET();
	Py_XDECREF(retval);
	ts->frame = f->f_back;
	Py_DECREF(f);
	return PyInt_FromLong(cPickle_save(_self, _args, _pers_save));
}

static int
pickle_M(PyObject *self, PyObject *args, int pers_save)
{
	PyBaseFrameObject *bf = slp_baseframe_new(pickle_runmain, 0, 0);
	if (bf == NULL) return -1;
	_self = self;
	_args = args;
	_pers_save = pers_save;
	return slp_int_wrapper(slp_eval_frame((PyFrameObject *)bf));
}

#endif
