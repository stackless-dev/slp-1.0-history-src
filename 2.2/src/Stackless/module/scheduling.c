#include "Python.h"

#ifdef STACKLESS
#include "core/stackless_impl.h"

/*******************************************************************

  Exception handling revised.

  Every tasklet has its own exception state. The thread state
  exists only once for the current thread, so we need to simulate
  ownership.

  Whenever a tasklet is run for the first time, it should clear
  the exception variables and start with a clean state.

  When it dies normally, it should clean up these.

  When a transfer occours from one tasklet to another,
  the first tasklet should save its exception in local variables,
  and the other one should restore its own.

  When a tasklet dies with an uncaught exception, this will
  be passed on to the main tasklet. The main tasklet must
  clear its exception state, if any, and take over those of
  the tasklet resting in peace.

  2002-08-08 This was a misconception. This is not the current
  exception, but the one which ceval provides for exception
  handlers. That means, sharing of this variable is totally
  unrelated to the current error, and we always swap the data.

 ********************************************************************/

slp_schedule_hook_func *_slp_schedule_fasthook;
PyObject *_slp_schedule_hook;

static int
transfer_with_exc(PyCStackObject **cstprev, PyCStackObject *cst, PyTaskletObject *prev)
{
	PyThreadState *ts = PyThreadState_GET();
	PyObject *exc_type = ts->exc_type;
	PyObject *exc_value = ts->exc_value;
	PyObject *exc_traceback = ts->exc_traceback;
	int ret;

	ts->exc_type = ts->exc_value = ts->exc_traceback = NULL;
	ret = slp_transfer(cstprev, cst, prev);
	ts->exc_type = exc_type;
	ts->exc_value = exc_value;
	ts->exc_traceback = exc_traceback;
	return ret;
}

/* scheduler monitoring */

void
slp_schedule_callback(PyTaskletObject *prev, PyTaskletObject *next)
{
	PyObject *args;

	if (prev == NULL) prev = (PyTaskletObject *)Py_None;
	if (next == NULL) next = (PyTaskletObject *)Py_None;
	args = Py_BuildValue("(OO)", prev, next);
	if (args != NULL) {
		PyObject *type, *value, *traceback, *ret;

		PyErr_Fetch(&type, &value, &traceback);
		ret = PyObject_Call(_slp_schedule_hook, args, NULL);
		if (ret != NULL)
			PyErr_Restore(type, value, traceback);
		else {
			Py_XDECREF(type);
			Py_XDECREF(value);
			Py_XDECREF(traceback);
		}
		Py_XDECREF(ret);
		Py_DECREF(args);
	}
}

#define NOTIFY_SCHEDULE(prev, next, errflag) \
	if (_slp_schedule_fasthook != NULL) { \
		if (ts->st.schedlock) \
			return RUNTIME_ERROR( \
			    "Recursive scheduler call due to callbacks!", \
			    errflag); \
        ts->st.schedlock = 1; \
        _slp_schedule_fasthook(prev, next); \
        ts->st.schedlock = 0; \
    }

static void
kill_wrap_bad_guy(PyTaskletObject *prev, PyTaskletObject *bad_guy)
{
	/* 
	 * just in case a transfer didn't work, we pack the bad
	 * tasklet into the exception and remove it from the runnables.
	 *
	 */
	PyThreadState *ts = PyThreadState_GET();
	PyObject *newval = PyTuple_New(2);
	if (bad_guy->next != NULL) {
		ts->st.current = bad_guy;
		slp_current_remove();
	}
	/* restore last tasklet */
	if (prev->next == NULL)
		slp_current_insert(prev);
	ts->frame = prev->f.frame;
	ts->st.current = prev;
	if (newval != NULL) {
		/* merge bad guy into exception */
		PyObject *exc, *val, *tb;
		PyErr_Fetch(&exc, &val, &tb);
		PyTuple_SET_ITEM(newval, 0, val);
		PyTuple_SET_ITEM(newval, 1, (PyObject*)bad_guy);
		Py_INCREF(bad_guy);
		PyErr_Restore(exc, newval, tb);
	}
}

static PyObject *slp_schedule_nr_maybe(PyTaskletObject *prev, 
				       PyTaskletObject *next, PyObject *retval);

PyObject *
slp_schedule_task(PyTaskletObject *prev, PyTaskletObject *next, int stackless,
		  PyObject *retval) 
{
	PyThreadState *ts = PyThreadState_GET();
	PyCStackObject **cstprev;

	if (next == prev) {
		Py_XINCREF(retval);
		return retval;
	}

	/* stackless != 0 => try soft. stackless < 0 => don't hard switch */

	if (stackless) {
		retval = slp_schedule_nr_maybe(prev, next, retval);
		if (STACKLESS_UNWINDING(retval) || retval == NULL || stackless < 0)
			return retval;
	}

	/* since we change the stack we must assure that the protocol was met */
	STACKLESS_ASSERT();

	ts->st.ticker = ts->st.interval;

	if (prev != NULL) {
		prev->recursion_depth = ts->recursion_depth;
		/* note: nesting_level is handled in cstack_new */
		prev->flags = ts->st.flags;
		prev->f.frame = ts->frame;
		assert(prev->tempval == NULL);
		Py_XINCREF(retval);
		prev->tempval = retval;
		cstprev = &prev->cstate;
	}
	else
		cstprev = NULL;

	NOTIFY_SCHEDULE(prev, next, NULL);

	if (!next->flags.blocked) /* that must be done by the channel */
		ts->st.current = next;

	if (ts->exc_type == Py_None) {
		Py_XDECREF(ts->exc_type);
		ts->exc_type = NULL;
	}
	ts->recursion_depth = next->recursion_depth;
	ts->st.flags = next->flags;
	ts->frame = next->f.frame;
	next->f.frame = NULL;
    
	++ts->st.nesting_level;
	if ((ts->exc_type != NULL ? transfer_with_exc : slp_transfer)(
		    cstprev, next->cstate, prev) == 0) {
		--ts->st.nesting_level;
		retval = prev->tempval;
		prev->tempval = NULL;
		return retval;
	}
		else {
		--ts->st.nesting_level;
		kill_wrap_bad_guy(prev, next);
		return NULL;
	}
}

/* non-recursive scheduling */

typedef struct _exc_frame {
	PyBaseFrameObject bf;
	PyObject *exc_type;
	PyObject *exc_value;
	PyObject *exc_traceback;
} exc_frame;
#define EXC_FRAME_SIZE ((sizeof(exc_frame) - \
			 sizeof(PyBaseFrameObject))/sizeof(PyObject*))


static PyObject *
restore_exception(PyFrameObject *f, PyObject *retval)
{
	PyThreadState *ts = PyThreadState_GET();
	exc_frame *ef = (exc_frame *) f;

	f = ef->bf.f_back;
	ts->exc_type = ef->exc_type;
	ts->exc_value = ef->exc_value;
	ts->exc_traceback = ef->exc_traceback;
	ef->exc_type = ef->exc_traceback = ef->exc_value = NULL;
	Py_DECREF((PyFrameObject *)ef);
	ts->frame = f;
	return STACKLESS_PACK(retval);
}

/* jumping from a soft tasklet to a hard switched */

static PyObject *
jump_soft_to_hard(PyFrameObject *f, PyObject *retval)
{
	PyThreadState *ts = PyThreadState_GET();

	ts->frame = f->f_back;
	Py_DECREF(f);
	ts->st.current->tempval = retval;
	slp_transfer(NULL, ts->st.current->cstate, NULL);
	/* we either have an error or don't come back, so: */
	return NULL;
}

static PyObject *
slp_schedule_nr_maybe(PyTaskletObject *prev, PyTaskletObject *next, PyObject *retval)
{
	PyThreadState *ts = PyThreadState_GET();

	/* 
	 * we check if we can do a non-recursive transfer.
	 *
	 * retval ==  retval          -->  can't do it
	 * retval ==  wrapped retval  -->  ready to take off
	 * retval ==  NULL            -->  exception occoured
	 */

	if (!slp_enable_softswitch)
		return retval;
	if (ts->st.nesting_level != 0)
		return retval;
	/* we can't handle escape from a channel with soft switching, yet */
	if (next->flags.blocked)
		return retval;

	ts->st.ticker = ts->st.interval;

	if (prev != NULL) {
		prev->recursion_depth = ts->recursion_depth;
		prev->flags = ts->st.flags;
		assert(prev->tempval == NULL);
		Py_XINCREF(retval);
		prev->tempval = retval;
		if (prev->cstate != ts->st.initial_stub) {
			Py_DECREF(prev->cstate);
			prev->cstate = ts->st.initial_stub;
			Py_INCREF(prev->cstate);
		}

		/* handle exception */
		if (ts->exc_type == Py_None) {
			Py_XDECREF(ts->exc_type);
			ts->exc_type = NULL;
		}
		else if (ts->exc_type != NULL) {
			/* build a shadow frame */
			exc_frame *f = (exc_frame *) slp_baseframe_new(
			    restore_exception, 1, EXC_FRAME_SIZE);
			if (f == NULL)
				return NULL;
			f->exc_type = ts->exc_type;
			f->exc_value = ts->exc_value;
			f->exc_traceback = ts->exc_traceback;
			ts->frame = (PyFrameObject *) f;
			ts->exc_type = ts->exc_value =
				       ts->exc_traceback = NULL;
		}
		prev->f.frame = ts->frame;
	}

	if (next->topframe != NULL && next->topframe->f_back == NULL) {
		/* this is a new tasklet. Provide the runner */
		PyFrameObject *f = next->f.frame;
		PyFrameObject *runner;

		assert(f->f_back == NULL);
		runner = ts->st.tasklet_runner;
		f->f_back = runner;
		Py_INCREF(runner);
	}

	ts->frame = next->f.frame;
	next->f.frame = NULL;

	assert(next->cstate != NULL);
	if (next->cstate->nesting_level != 0) {
		/* create a helper frame to restore the target stack */
		ts->frame = (PyFrameObject *)
			    slp_baseframe_new(jump_soft_to_hard, 1, 0);
		if (ts->frame == NULL) {
			ts->frame = prev->f.frame;
			return NULL;
		}
	}

	ts->st.flags = next->flags;
	ts->recursion_depth = next->recursion_depth;

	NOTIFY_SCHEDULE(prev, next, NULL);

	ts->st.current = next;
	retval = next->tempval;
	next->tempval = NULL;

	return STACKLESS_PACK(retval);
}


static PyFrameObject *
load_state_from_task(PyTaskletObject *task)
{
	PyThreadState *ts = PyThreadState_GET();

	assert(PyTasklet_Check(task));
	ts->st.flags = task->flags;
	ts->recursion_depth = task->recursion_depth;
	assert(task->cstate != NULL);
	ts->st.nesting_level = task->cstate->nesting_level;
	ts->frame = task->f.frame;
	return task->f.frame;
}


/* 
 * reviving main.
 * it is (now) required, that main is always present.
 * reviving just means to bring it back into play.
 */
int
slp_revive_main(void) 
{
	PyThreadState *ts = PyThreadState_GET();

	assert(ts->st.main->f.frame != NULL);
	if (ts->st.main->next == NULL) {
		Py_INCREF(ts->st.main);
		slp_current_insert(ts->st.main);
		return 0;
	}
	return -1;
}


static PyObject * tasklet_end(PyFrameObject *runner, PyObject *retval);

int
initialize_main_and_current(PyFrameObject *f)
{
	PyThreadState *ts = PyThreadState_GET();
	PyTaskletObject *task; 
	PyObject *noargs;
	PyBaseFrameObject *runner;

	/* refuse executing main in an unhandled error context */
	if (! (PyErr_Occurred() == NULL || PyErr_Occurred() == Py_None) ) {
#ifdef _DEBUG
		PyObject *type, *value, *traceback;
		PyErr_Fetch(&type, &value, &traceback);
		Py_XINCREF(type);
		Py_XINCREF(value);
		Py_XINCREF(traceback);
		PyErr_Restore(type, value, traceback);
		printf("Pending error while entering Stackless subsystem:\n");
		PyErr_Print();
		printf("Above exception is re-raised to the caller.\n");
		PyErr_Restore(type, value, traceback);
#endif
        return 1;
	}

	/* create end-handler */
	if (ts->st.tasklet_runner == NULL) {
		runner = slp_baseframe_new(tasklet_end, 0, 0);
		if (runner == NULL)
			return -1;
		ts->st.tasklet_runner = (PyFrameObject *) runner;
		Py_INCREF(runner);
	}

	noargs = PyTuple_New(0);
	task = (PyTaskletObject *) PyTasklet_Type.tp_new(
		    &PyTasklet_Type, noargs, NULL);
	Py_DECREF(noargs);
	if (task == NULL) return -1;
	Py_INCREF(Py_None);
	task->tempval = Py_None;
	task->f.frame = f;
	task->topframe = f;
	Py_INCREF(f);
	assert(task->cstate != NULL);
	task->cstate->task = task;
	load_state_from_task(task);
	ts->st.main = task;
	Py_INCREF(task);
	slp_current_insert(task);
	ts->st.current = task;

	NOTIFY_SCHEDULE(NULL, task, -1);

	return 0;
}


static PyObject *
tasklet_end(PyFrameObject *runner, PyObject *retval)
{
	PyThreadState *ts = PyThreadState_GET();
	PyTaskletObject *task = ts->st.current;

	int ismain = task == ts->st.main;

	if (ismain) {
		/* 
		 * See whether we need to adjust main's context before
		 * returning
		 */
		if (ts->st.serial_last_jump != ts->st.serial) {
			task->tempval = retval;
			slp_transfer_return(ts->st.current->cstate);
		}
		/* this hack can attach us to ourselves :( */
		if (runner->f_back != NULL) {
			assert(runner->f_back == runner);
			Py_DECREF(runner);
			runner->f_back = NULL;
		}
	}

	/* these may need to be updated */
	task->f.frame = task->topframe;
	task->flags = ts->st.flags;
	task->recursion_depth = 0;

	/* 
	 * Deallocation might cause stack switches during tasklet_end!
	 * Therefore, we need to stay reentrant and keep current alive.
	 * The runner frame exists only once, unfortunately.
	 * We need to use topframe as current frame in order to
	 * store a cstack in case of a switch.
	 */
	ts->frame = task->topframe;

	/* 
	 * see whether we have a SystemExit, which is no error.
	 * Note that TaskletExit is a subclass.
	 */
	if (retval == NULL &&
	    PyErr_Occurred() &&
	    PyErr_ExceptionMatches(PyExc_SystemExit)) {
		PyErr_Clear();
		Py_INCREF(Py_None);
		retval = Py_None;
	}

	/* notify before destroying anything */
	NOTIFY_SCHEDULE(task, NULL, NULL);

	/* clear the tasklet, while leaving it in place */
	task->f.frame = NULL; /* avoid decref */
	task->topframe = NULL; /* avoid a kill() */
	task->ob_type->tp_clear((PyObject *)task);
	/* also clear the frame but leave it in place */
	++ts->recursion_depth;
	ts->frame->ob_type->tp_clear((PyObject *)ts->frame);
	--ts->recursion_depth;
	/*
	 * let the task dealloc the frame.
	 * Note that the frame *could* have been assigned
	 * already due to a switch during dealloc, so we
	 * just enforce it.
	 */
	task->f.frame = ts->frame;
	ts->frame = NULL;
	/* safely clear again, now with frame */
	task->ob_type->tp_clear((PyObject *)task);

	/* 
	 * clean up any current exception - this tasklet is dead.
	 * This only happens if we are killing tasklets in the middle
	 * of their execution.
         */
	if (ts->exc_type != NULL && ts->exc_type != Py_None) {
		Py_DECREF(ts->exc_type);
		Py_XDECREF(ts->exc_value);
		Py_XDECREF(ts->exc_traceback);
		ts->exc_type = ts->exc_value = ts->exc_traceback = NULL;
	}

	/*
	 * put the result back into the dead tasklet, to give
	 * possible referers access to the return value
	 */
	task->tempval = retval;

	ts->st.current = task;
	slp_current_remove();
	Py_DECREF(task);

	/* capture all exceptions */
	if (ismain) {
		/* 
		 * Main wants to exit. We clean up, but leave the
		 * runnables chain intact.
		 */
		ts->st.main = NULL;
		retval = task->tempval;
		Py_XINCREF(retval);
		Py_DECREF(task);
		return retval;
	}

	if (ts->st.runcount == 0) {
		if (slp_revive_main()) {
			/* main is blocked and nobody can send */
			if (ts->st.main->flags.blocked < 0)
				RUNTIME_ERROR("the main tasklet is receiving"
				    " without a sender available.", NULL);
			else
				RUNTIME_ERROR("the main tasklet is sending"
				    " without a receiver available.", NULL);
			/* fall through to error handling */
			retval = NULL;
		}
	}

	assert(ts->st.runcount > 0 || ts->st.main->flags.blocked);

	if (retval == NULL) {
		/*
		 * error handling: continue in the context of the main tasklet.
		 */
		slp_revive_main();
		/* propagate error */
		if (ts->st.main->tempval != NULL) {
			Py_DECREF(ts->st.main->tempval);
			ts->st.main->tempval = NULL;
		}
		return slp_schedule_task(NULL, ts->st.main, 1, NULL);
		/* we do not come back here */
	}

	assert(ts->st.current->f.frame != NULL);
	assert(ts->st.current->f.frame->ob_refcnt > 0);

	return slp_schedule_task(NULL, ts->st.current, 1, retval);
}

/* 
  the following functions only have to handle "real"
  tasklets, those which need to switch the C stack.
  The "soft" tasklets are handled by frame pushing.
  It is not so much simpler than I thought :-(
*/

PyObject *
slp_resume_tasklet(PyFrameObject *f)
{
	PyThreadState *ts = PyThreadState_GET();

	PyObject *retval = ts->st.current->tempval;
	ts->st.current->tempval = NULL;
	return slp_frame_dispatch_top(f, retval);
}

PyObject *
slp_run_tasklet(PyFrameObject *f)
{
	PyThreadState *ts = PyThreadState_GET();

	PyFrameObject *runner;
	if ( (ts->st.main == NULL) && initialize_main_and_current(f)) {
		ts->frame = NULL;
		return NULL;
	}

	runner = ts->st.tasklet_runner;
	assert(f->f_back == NULL ||f->f_back == runner);
	if (f->f_back == NULL) {
		f->f_back = runner;
		Py_INCREF(runner);
	}
	return slp_resume_tasklet(f);
}
#endif
