#include "Python.h"
#ifdef STACKLESS

#include "compile.h"
#include "frameobject.h"
#include "structmember.h"

#include "stackless_impl.h"
#include "pickling/prickelpit.h"

/* platform specific constants */
#include "platf/slp_platformselect.h"

/* Stackless extension for ceval.c */

/******************************************************

  Static Global Variables

*******************************************************/

/* the flag which decides whether we try to use soft switching */

int slp_enable_softswitch = 1;

/* 
 * flag whether the next call should try to be stackless.
 * The protocol is: This flag may be only set if the called
 * thing supports it. It doesn't matter whether it uses the
 * chance, but it *must* set it to zero before returning.
 * This flags in a way srves as a parameter that we don't have.
 */
int slp_try_stackless = 0;

/******************************************************

  The C Stack

 ******************************************************/

/* adjust slots to typical size for your system */

#define CSTACK_SLOTS        1024
#define CSTACK_MAXCACHE     100
#define CSTACK_GOODGAP      4096

static PyCStackObject *cstack_cache[CSTACK_SLOTS] = { NULL };
static int cstack_cachecount = 0;

/* this function will get called by PyStacklessEval_Fini */
static void slp_cstack_cacheclear(void)
{
	int i;
	PyCStackObject *stack;

	for (i=0; i < CSTACK_SLOTS; i++) {
		while (cstack_cache[i] != NULL) {
			stack = cstack_cache[i];
			cstack_cache[i] = (PyCStackObject *) stack->startaddr;
			PyMem_Free(stack);
		}
	}
	cstack_cachecount = 0;
}

static void
cstack_dealloc(PyCStackObject *cst)
{
	PyThreadState *ts = PyThreadState_GET();

	ts->st.cstack_chain = cst;
	SLP_CHAIN_REMOVE(PyCStackObject, &ts->st.cstack_chain, cst, next, 
			 prev);
	if (cst->ob_size >= CSTACK_SLOTS) {
		PyMem_Free(cst);
	}
	else {
		if (cstack_cachecount >= CSTACK_MAXCACHE)
		slp_cstack_cacheclear();
		cst->startaddr = (int *) cstack_cache[cst->ob_size];
		cstack_cache[cst->ob_size] = cst;
		++cstack_cachecount;
	}
}


PyCStackObject *
slp_cstack_new(PyCStackObject **cst, int *stackref, PyTaskletObject *task)
{
	PyThreadState *ts = PyThreadState_GET();
	int *stackbase = ts->st.cstack_base;
	int size = stackbase - stackref;

	if (size < 0) {
		PyErr_SetString(PyExc_RuntimeError, "negative stack size");
		return NULL;
	}
	if (*cst && (*cst)->ob_size == size && (*cst)->ob_refcnt == 1) {
		/* reuse it */
		return *cst;
	}
	    
	if (*cst != NULL) {
		if ((*cst)->task == task)
			(*cst)->task = NULL;
		Py_DECREF(*cst);
	}
	if (size < CSTACK_SLOTS && ((*cst) = cstack_cache[size])) {
		/* take stack from cache */
		cstack_cache[size] = (PyCStackObject *) (*cst)->startaddr;
		--cstack_cachecount;
	}
	else {
		/* PyObject_NewVar is inlined */
		*cst = (PyCStackObject *)
		       PyObject_MALLOC(sizeof(PyCStackObject) + 
				       (size-1) * sizeof(int*));
		if (*cst == NULL) return NULL;
	}
	(void) PyObject_INIT_VAR(*cst, &PyCStack_Type, size);

	(*cst)->startaddr = stackbase;
	(*cst)->next = (*cst)->prev = NULL;
	SLP_CHAIN_INSERT(PyCStackObject, &ts->st.cstack_chain, *cst, next, prev);
	(*cst)->serial = ts->st.serial;
	(*cst)->task = task;
	(*cst)->tstate = ts;
	(*cst)->nesting_level = ts->st.nesting_level;
	return *cst;
}

int
slp_cstack_save(PyCStackObject *cstprev)
{
	int stsizeb = (cstprev)->ob_size * sizeof(int*);

	memcpy((cstprev)->stack, (cstprev)->startaddr - 
				 (cstprev)->ob_size, stsizeb);
	return stsizeb;
}

void
slp_cstack_restore(PyCStackObject *cst)
{
	/* mark task as no longer responsible for cstack instance */
	cst->task = NULL;
	memcpy(cst->startaddr - cst->ob_size, &cst->stack,
	       (cst->ob_size) * sizeof(int*));
}


static char cstack_doc[] =
"A CStack object serves to save the stack slice which is involved\n\
during a recursive Python call. It will also be used for pickling\n\
of program state. This structure is highly platform dependant.\n\
Note: For inspection, str() can dump it as a string.\
";

#if SIZEOF_VOIDP == SIZEOF_INT
#define T_ADDR T_UINT
#else
#define T_ADDR T_ULONG
#endif


static PyMemberDef cstack_members[] = {
	{"size", T_INT, offsetof(PyCStackObject, ob_size), READONLY},
	{"next", T_OBJECT, offsetof(PyCStackObject, next), READONLY},
	{"prev", T_OBJECT, offsetof(PyCStackObject, prev), READONLY},
	{"task", T_OBJECT, offsetof(PyCStackObject, task), READONLY},
	{"startaddr", T_ADDR, offsetof(PyCStackObject, startaddr), READONLY},
	{0}
};

/* simple string interface for inspection */

static PyObject *
cstack_str(PyObject *o)
{
	PyCStackObject *cst = (PyCStackObject*)o;
	return PyString_FromStringAndSize((char*)&cst->stack,
	    cst->ob_size*sizeof(cst->stack[0]));
}

PyTypeObject PyCStack_Type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,
	"stackless.cstack",
	sizeof(PyCStackObject),
	sizeof(PyObject *),
	(destructor)cstack_dealloc,	/* tp_dealloc */
	0,				/* tp_print */
	0,				/* tp_getattr */
	0,				/* tp_setattr */
	0,				/* tp_compare */
	0,				/* tp_repr */
	0,				/* tp_as_number */
	0,				/* tp_as_sequence */
	0,				/* tp_as_mapping */
	0,				/* tp_hash */
	0,				/* tp_call */
	(reprfunc)cstack_str,		/* tp_str */
	PyObject_GenericGetAttr,	/* tp_getattro */
	PyObject_GenericSetAttr,	/* tp_setattro */
	0,				/* tp_as_buffer */
	Py_TPFLAGS_DEFAULT,		/* tp_flags */
	cstack_doc,			/* tp_doc */
	0,				/* tp_traverse */
	0,				/* tp_clear */
	0,				/* tp_richcompare */
	0,				/* tp_weaklistoffset */
	0,				/* tp_iter */
	0,				/* tp_iternext */
	0,				/* tp_methods */
	cstack_members,			/* tp_members */
	0,				/* tp_getset */
	0,				/* tp_base */
	0,				/* tp_dict */
	0,				/* tp_descr_get */
	0,				/* tp_descr_set */
	0,				/* tp_dictoffset */
	0,				/* tp_init */
	0,				/* tp_alloc */
	0,				/* tp_new */
	0,				/* tp_free */
};


static int
make_initial_stub(void)
{
	PyThreadState *ts = PyThreadState_GET();

	if (ts->st.initial_stub != NULL) {
		Py_DECREF(ts->st.initial_stub);
		ts->st.initial_stub = NULL;
	}
	++ts->st.serial;
	if (slp_transfer(&ts->st.initial_stub, NULL, NULL)) return -1;
	ts->st.initial_stub->serial = ts->st.serial;
	/* 
	 * from here, we always arrive with a compatible cstack
	 * that also can be used by main, if it is running
	 * in soft-switching mode.
	 * To insure that, it was necessary to re-create the
	 * initial stub for *every* run of a new main.
	 */

	return 0;
}

static PyObject *
climb_stack_and_eval_frame(PyFrameObject *f)
{
	/* 
	 * a similar case to climb_stack_and_transfer,
	 * but here we need to incorporate a gap in the
	 * stack into main and keep this gap on the stack.
	 * This way, initial_stub is always valid to be
	 * used to return to the main c stack.
	 */
	PyThreadState *ts = PyThreadState_GET();
	int probe;
	int needed = &probe - ts->st.cstack_base;
	/* in rare cases, the need might have vanished due to the recursion */
	int * goobledigoobs;

	if (needed > 0) {
		goobledigoobs = alloca(needed * sizeof(int));
		if (goobledigoobs == NULL)
			return NULL;
	}
	return slp_eval_frame(f);
}


PyObject *
slp_eval_frame(PyFrameObject *f)
{
	PyThreadState *ts = PyThreadState_GET();
	PyFrameObject *fprev = f->f_back;
	int * stackref;

	if (fprev == NULL && ts->st.main == NULL) {
		/* this is the initial frame, so mark the stack base */

		/* 
		 * careful, this caused me a major headache.
		 * it is *not* sufficient to just check for fprev == NULL.
		 * Reason: (observed with wxPython):
		 * A toplevel frame is run as a tasklet. When its frame
		 * is deallocated (in slp_tasklet_end), a Python object
		 * with a __del__ method is destroyed. This __del__
		 * will run as a toplevel frame, with fback == NULL!
		 */

		stackref = STACK_REFPLUS + (int*) &f;
			if (ts->st.cstack_base == NULL)
				ts->st.cstack_base = stackref-CSTACK_GOODGAP;
			if (stackref > ts->st.cstack_base)
				return climb_stack_and_eval_frame(f);

		ts->frame = f;
		if (make_initial_stub())
			return NULL;
		f = ts->frame;
		if (f == NULL)
			return NULL;
		if (f->f_back == NULL || f->f_back == ts->st.tasklet_runner) {
			/* this is really a new tasklet or main */
			/* I hope very much to get rid of this hack, soon */
			return slp_run_tasklet(f);
		}
		return slp_resume_tasklet(f);
	}
	Py_INCREF(Py_None);
	return slp_frame_dispatch(f, fprev, Py_None);
}

void slp_kill_tasks_with_stacks(struct _ts *tstate)
{
	PyThreadState *ts = PyThreadState_GET();
	PyObject *exception, *value, *tb;

	if (ts != tstate) {
		/* too bad, can't handle this, give up */
		return;
	}
	PyErr_Fetch(&exception, &value, &tb);
	while (1) {
		PyCStackObject *csfirst = ts->st.cstack_chain, *cs = csfirst;
		PyTaskletObject *t;

		if (cs == NULL)
			break;
		while (cs->task == NULL && cs->next != csfirst) {
			cs = cs->next;
		}
		if (cs->task == NULL)
			return;
		t = cs->task;
		cs->task = NULL;
		PyTasklet_Kill(t);
		PyErr_Clear();
	}
	PyErr_Restore(exception, value, tb);
}

void PyStackless_kill_tasks_with_stacks(void)
{
	slp_kill_tasks_with_stacks(PyThreadState_GET());
}

void
PyStacklessEval_Fini(void)
{
	slp_cstack_cacheclear();
}


/******************************************************

  Generator re-implementation for Stackless

*******************************************************/

typedef struct {
	PyObject_HEAD
	/* The gi_ prefix is intended to remind of generator-iterator. */

	PyFrameObject *gi_frame;

	/* True if generator is being executed. */ 
	int gi_running;

	/* List of weak reference. */
	PyObject *gi_weakreflist;
} genobject;

/*
 * Note:
 * Generators are quite a bit slower in Stackless, because
 * we are jumping in and out so much.
 * I had an implementation with no extra cframe, but it
 * was not faster, but considerably slower than this solution.
 */

typedef struct _gen_callback_frame {
	PyBaseFrameObject bf;
	genobject *gen;
} gen_callback_frame;
#define GEN_CALLBACK_FRAME_SIZE ((sizeof(gen_callback_frame) - \
				  sizeof(PyBaseFrameObject))/sizeof(PyObject*))

static PyObject* gen_iternext_callback(PyFrameObject *f, PyObject *retval);

PyObject *
slp_gen_iternext(PyObject *ob)
{
	STACKLESS_GETARG();
	genobject *gen = (genobject *) ob;
	PyThreadState *ts = PyThreadState_GET();
	PyFrameObject *f = gen->gi_frame;
	PyFrameObject *stopframe = ts->frame;
	PyObject *retval;

	if (gen->gi_running) {
		PyErr_SetString(PyExc_ValueError,
				"generator already executing");
		return NULL;
	}
	if (f->f_stacktop == NULL)
		return NULL;

	if (f->f_back == NULL &&
	    (f->f_back = (PyFrameObject *)
			 slp_baseframe_new(gen_iternext_callback, 0,
					   GEN_CALLBACK_FRAME_SIZE)) == NULL)
		return NULL;
	/* Generators always return to their most recent caller, not
	 * necessarily their creator. */
	Py_XINCREF(ts->frame);
	assert(f->f_back != NULL);
	assert(f->f_back->f_back == NULL);
	f->f_back->f_back = ts->frame;

	gen->gi_running = 1;

	Py_INCREF(Py_None);
	retval = Py_None;
	f->f_execute = PyEval_EvalFrame;

	/* make refcount compatible to frames for tasklet unpickling */
	Py_INCREF(f->f_back);

	Py_INCREF(gen);
	((gen_callback_frame *) f->f_back)->gen = gen;
	Py_INCREF(f);
	ts->frame = f;
	if (stackless)
		return STACKLESS_PACK(retval);
	return slp_frame_dispatch(f, stopframe, retval);
}

static PyObject*
gen_iternext_callback(PyFrameObject *f, PyObject *retval)
{
	PyThreadState *ts = PyThreadState_GET();
	gen_callback_frame *gcf = (gen_callback_frame*) f;
	genobject *gen = gcf->gen;

	gen->gi_running = 0;
	/* make refcount compatible to frames for tasklet unpickling */
	Py_DECREF(f);

	/* Don't keep the reference to f_back any longer than necessary.  It
	 * may keep a chain of frames alive or it could create a reference
	 * cycle. */
	ts->frame = f->f_back;
	Py_XDECREF(f->f_back);
	f->f_back = NULL;

	f = gen->gi_frame;
	/* If the generator just returned (as opposed to yielding), signal
	 * that the generator is exhausted. */
	if (retval == Py_None && f->f_stacktop == NULL) {
		Py_DECREF(retval);
		retval = NULL;
		/* are we awaited by a for_iter or called by next() ? */
		if (ts->frame->f_execute != PyEval_EvalFrame_iter) {
			/* do the missing part of the next call */
			if (!PyErr_Occurred())
				PyErr_SetNone(PyExc_StopIteration);
		}
	}
	gcf->gen = NULL;
	Py_DECREF(gen);
	return retval;
}


/******************************************************

  Rebirth of software stack avoidance

*******************************************************/

static PyObject *
unwind_repr(PyObject *op)
{
	return PyString_FromString(
		"The invisible unwind token. If you ever should see this,\n"
		"please report the error to tismer@tismer.com"
	);
}

/* dummy deallocator, just in case */
static void unwind_dealloc(PyObject *op) {
}

static PyTypeObject PyUnwindToken_Type = {
	PyObject_HEAD_INIT(&PyUnwindToken_Type)
	0,
	"UnwindToken",
	0,
	0,
	(destructor)unwind_dealloc, /*tp_dealloc*/ /*should never be called*/
	0,		/*tp_print*/
	0,		/*tp_getattr*/
	0,		/*tp_setattr*/
	0,		/*tp_compare*/
	(reprfunc)unwind_repr, /*tp_repr*/
	0,		/*tp_as_number*/
	0,		/*tp_as_sequence*/
	0,		/*tp_as_mapping*/
	0,		/*tp_hash */
};

static PyUnwindObject unwind_token = {
	PyObject_HEAD_INIT(&PyUnwindToken_Type)
	NULL
};

PyUnwindObject *Py_UnwindToken = &unwind_token;

/* 
    the frame dispatcher will execute frames and manage
    the frame stack until the "previous" frame reappears.
    The "Mario" code if you know that game :-)
 */

PyObject *
slp_frame_dispatch(PyFrameObject *f, PyFrameObject *stopframe, PyObject *retval)
{
	PyThreadState *ts = PyThreadState_GET();

	++ts->st.nesting_level;

/* 
    frame protocol:
    If a frame returns the Py_UnwindToken object, this 
    indicates that a different frame will be run. 
    Semantics of an appearing Py_UnwindToken:
    The true return value is in its tempval field.
    We always use the topmost tstate frame and bail
    out when we see the frame that issued the
    originating dispatcher call (which may be a NULL frame).
 */

	while (1) {

		retval = f->f_execute(f, retval);
		f = ts->frame;
		if (STACKLESS_UNWINDING(retval))
			STACKLESS_UNPACK(retval);
		if (f == stopframe)
			break;
	}
	--ts->st.nesting_level;
	/* see whether we need to trigger a pending interrupt */
	if (ts->st.flags.pending_irq)
		slp_check_pending_irq();
	return retval;
}

PyObject *
slp_frame_dispatch_top(PyFrameObject *f, PyObject *retval)
{
	PyThreadState *ts = PyThreadState_GET();

	if (f==NULL) return NULL;

	while (1) {

		retval = f->f_execute(f, retval);
		f = ts->frame;
		if (STACKLESS_UNWINDING(retval))
			STACKLESS_UNPACK(retval);
		if (f == NULL)
			break;
	}
    return retval;
}


#endif /* STACKLESS */
