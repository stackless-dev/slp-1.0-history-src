"""distutils

The main package for the Python Module Distribution Utilities.  Normally
used from a setup script as

   from distutils.core import setup

   setup (...)
"""

# This module should be kept compatible with Python 1.5.2.

__revision__ = "$Id$"

__version__ = "1.0.3"
