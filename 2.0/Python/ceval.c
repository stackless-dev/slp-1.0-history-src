
/* Execute compiled code */

/* XXX TO DO:
   XXX how to pass arguments to call_trace?
   XXX speed up searching for keywords by using a dictionary
   XXX document it!
   */

#include "Python.h"

#include "compile.h"
#include "frameobject.h"
#include "eval.h"
#include "opcode.h"

#ifdef macintosh
#include "macglue.h"
#endif

#include <ctype.h>

/* Turn this on if your compiler chokes on the big switch: */
/* #define CASE_TOO_BIG 1 */

#ifdef Py_DEBUG
/* For debugging the interpreter: */
#define LLTRACE  1	/* Low-level trace feature */
#define CHECKEXC 1	/* Double-check exception checking */
#endif

/* experimental integer overflow 2001/06/24 */
#ifdef Py_INT_IGNORE
#define INT_CHECK_FLAG 0
#else
#define INT_CHECK_FLAG 1
#endif

//#include "ceval_macro.h"

/* Forward declarations */

static PyObject *eval_code2_setup (PyCodeObject *,
				 PyObject *, PyObject *,
				 PyObject **, int,
				 PyObject **, int,
				 PyObject **, int,
				 PyObject *);
static PyObject *eval_code2_loop (PyFrameObject *, PyObject *);
#ifdef LLTRACE
static int prtrace (PyObject *, char *);
#endif
static void call_exc_trace(PyObject **, PyObject**, PyFrameObject *);
static int call_trace (PyObject **, PyObject **,
				PyFrameObject *, char *, PyObject *);
static PyObject *call_builtin(PyObject *, PyObject *, PyObject *);
static PyObject *call_function(PyObject *, PyObject *, PyObject *);
static PyObject *loop_subscript(PyObject *, int);
static PyObject *apply_slice(PyObject *, PyObject *, PyObject *);
static int assign_slice(PyObject *, PyObject *,
				  PyObject *, PyObject *);
static PyObject *cmp_outcome(int, PyObject *, PyObject *);
static PyObject *import_from(PyObject *, PyObject *);
static int import_all_from(PyObject *, PyObject *);
static PyObject *build_class(PyObject *, PyObject *, PyObject *);
static int exec_statement (PyFrameObject *,
				    PyObject *, PyObject *, PyObject *);
static void set_exc_info(PyThreadState *, PyObject *, PyObject *, PyObject *);
static void reset_exc_info(PyThreadState *);
static void format_exc_check_arg(PyObject *, char *, PyObject *);


/* counter objects using mutable integers */
/* besides the speedup, this allows to keep counters on stack copies */
/* This is modelled light-weight after Marc Lemburg's PyCounterObject */

typedef struct {
	PyObject_HEAD
	long ob_ival;
} PyLoopIndexObject;

extern DL_IMPORT(PyTypeObject) PyLoopIndex_Type;

#define PyLoopIndex_Check(op) ((op)->ob_type == &PyLoopIndex_Type)

extern PyObject *PyLoopIndex_FromLong (long);

#define PyLoopIndex_AS_LONG(op) (((PyLoopIndexObject *)(op))->ob_ival)
#define PyLoopIndex_INCREMENT(op) (((PyLoopIndexObject *)(op))->ob_ival++)


#define NAME_ERROR_MSG \
	"There is no variable named '%s'"
#define UNBOUNDLOCAL_ERROR_MSG \
	"Local variable '%.200s' referenced before assignment"

/* Dynamic execution profile */
#ifdef DYNAMIC_EXECUTION_PROFILE
#ifdef DXPAIRS
static long dxpairs[257][256];
#define dxp dxpairs[256]
#else
static long dxp[256];
#endif
#endif


#ifdef WITH_THREAD

#ifndef DONT_HAVE_ERRNO_H
#include <errno.h>
#endif
#include "pythread.h"

extern int _PyThread_Started; /* Flag for Py_Exit */

static PyThread_type_lock interpreter_lock = 0;
static long main_thread = 0;

void
PyEval_InitThreads(void)
{
	if (interpreter_lock)
		return;
	_PyThread_Started = 1;
	interpreter_lock = PyThread_allocate_lock();
	PyThread_acquire_lock(interpreter_lock, 1);
	main_thread = PyThread_get_thread_ident();
}

void
PyEval_AcquireLock(void)
{
	PyThread_acquire_lock(interpreter_lock, 1);
}

void
PyEval_ReleaseLock(void)
{
	PyThread_release_lock(interpreter_lock);
}

void
PyEval_AcquireThread(PyThreadState *tstate)
{
	if (tstate == NULL)
		Py_FatalError("PyEval_AcquireThread: NULL new thread state");
	PyThread_acquire_lock(interpreter_lock, 1);
	if (PyThreadState_Swap(tstate) != NULL)
		Py_FatalError(
			"PyEval_AcquireThread: non-NULL old thread state");
}

void
PyEval_ReleaseThread(PyThreadState *tstate)
{
	if (tstate == NULL)
		Py_FatalError("PyEval_ReleaseThread: NULL thread state");
	if (PyThreadState_Swap(NULL) != tstate)
		Py_FatalError("PyEval_ReleaseThread: wrong thread state");
	PyThread_release_lock(interpreter_lock);
}
/* This function is called from PyOS_AfterFork to ensure that newly
   created child processes don't hold locks referring to threads which
   are not running in the child process.  (This could also be done using
   pthread_atfork mechanism, at least for the pthreads implementation.) */

void
PyEval_ReInitThreads(void)
{
	if (!interpreter_lock)
		return;
	/*XXX Can't use PyThread_free_lock here because it does too
	  much error-checking.  Doing this cleanly would require
	  adding a new function to each thread_*.h.  Instead, just
	  create a new lock and waste a little bit of memory */
	interpreter_lock = PyThread_allocate_lock();
	PyThread_acquire_lock(interpreter_lock, 1);
	main_thread = PyThread_get_thread_ident();
}
#endif

/* Functions save_thread and restore_thread are always defined so
   dynamically loaded modules needn't be compiled separately for use
   with and without threads: */

PyThreadState *
PyEval_SaveThread(void)
{
	PyThreadState *tstate = PyThreadState_Swap(NULL);
	if (tstate == NULL)
		Py_FatalError("PyEval_SaveThread: NULL tstate");
#ifdef WITH_THREAD
	if (interpreter_lock)
		PyThread_release_lock(interpreter_lock);
#endif
	return tstate;
}

void
PyEval_RestoreThread(PyThreadState *tstate)
{
	if (tstate == NULL)
		Py_FatalError("PyEval_RestoreThread: NULL tstate");
#ifdef WITH_THREAD
	if (interpreter_lock) {
		int err = errno;
		PyThread_acquire_lock(interpreter_lock, 1);
		errno = err;
	}
#endif
	PyThreadState_Swap(tstate);
}


/* Mechanism whereby asynchronously executing callbacks (e.g. UNIX
   signal handlers or Mac I/O completion routines) can schedule calls
   to a function to be called synchronously.
   The synchronous function is called with one void* argument.
   It should return 0 for success or -1 for failure -- failure should
   be accompanied by an exception.

   If registry succeeds, the registry function returns 0; if it fails
   (e.g. due to too many pending calls) it returns -1 (without setting
   an exception condition).

   Note that because registry may occur from within signal handlers,
   or other asynchronous events, calling malloc() is unsafe!

#ifdef WITH_THREAD
   Any thread can schedule pending calls, but only the main thread
   will execute them.
#endif

   XXX WARNING!  ASYNCHRONOUSLY EXECUTING CODE!
   There are two possible race conditions:
   (1) nested asynchronous registry calls;
   (2) registry calls made while pending calls are being processed.
   While (1) is very unlikely, (2) is a real possibility.
   The current code is safe against (2), but not against (1).
   The safety against (2) is derived from the fact that only one
   thread (the main thread) ever takes things out of the queue.

   XXX Darn!  With the advent of thread state, we should have an array
   of pending calls per thread in the thread state!  Later...
*/

#define NPENDINGCALLS 32
static struct {
	int (*func) (void *);
	void *arg;
} pendingcalls[NPENDINGCALLS];
static volatile int pendingfirst = 0;
static volatile int pendinglast = 0;
static volatile int things_to_do = 0;

int
Py_AddPendingCall(int (*func)(void *), void *arg)
{
	static int busy = 0;
	int i, j;
	/* XXX Begin critical section */
	/* XXX If you want this to be safe against nested
	   XXX asynchronous calls, you'll have to work harder! */
	if (busy)
		return -1;
	busy = 1;
	i = pendinglast;
	j = (i + 1) % NPENDINGCALLS;
	if (j == pendingfirst)
		return -1; /* Queue full */
	pendingcalls[i].func = func;
	pendingcalls[i].arg = arg;
	pendinglast = j;
	things_to_do = 1; /* Signal main loop */
	busy = 0;
	/* XXX End critical section */
	return 0;
}

int
Py_MakePendingCalls(void)
{
	static int busy = 0;
#ifdef WITH_THREAD
	if (main_thread && PyThread_get_thread_ident() != main_thread)
		return 0;
#endif
	if (busy)
		return 0;
	busy = 1;
	things_to_do = 0;
	for (;;) {
		int i;
		int (*func) (void *);
		void *arg;
		i = pendingfirst;
		if (i == pendinglast)
			break; /* Queue empty */
		func = pendingcalls[i].func;
		arg = pendingcalls[i].arg;
		pendingfirst = (i + 1) % NPENDINGCALLS;
		if (func(arg) < 0) {
			busy = 0;
			things_to_do = 1; /* We're not done yet */
			return -1;
		}
	}
	busy = 0;
	return 0;
}


/* The interpreter's recursion limit */

static int recursion_limit = 1000;

int
Py_GetRecursionLimit(void)
{
	return recursion_limit;
}

void
Py_SetRecursionLimit(int new_limit)
{
	recursion_limit = new_limit;
}

/* Status code for main loop (reason for stack unwind) */

/* extended the why_code to handle calls as well */
enum why_code {
		WHY_NOT,	    /* No error */
		WHY_EXCEPTION,	/* Exception occurred */
		WHY_RERAISE,	/* Exception re-raised by 'finally' */
		WHY_RETURN,		/* 'return' statement */
		WHY_BREAK,		/* 'break' statement */
		WHY_CALL,		/* falling back to call a new frame */
		WHY_INTERRUPT,	/* micro thread dispatch */
};

static enum why_code do_raise (PyObject *, PyObject *, PyObject *);
static int unpack_sequence (PyObject *, int, PyObject **);


/* Backward compatible interface */

PyObject *
PyEval_EvalCode(PyCodeObject *co, PyObject *globals, PyObject *locals)
{
	PyObject *frame;
	frame=eval_code2_setup(co,
			  globals, locals,
			  (PyObject **)NULL, 0,
			  (PyObject **)NULL, 0,
			  (PyObject **)NULL, 0,
			  (PyObject *)NULL);
	if (frame != NULL)
		return PyEval_Frame_Dispatch();
	else
		return NULL;
}


PyObject *
PyEval_EvalCode_nr(PyCodeObject *co, PyObject *globals, PyObject *locals)
{
	PyObject *frame;
	frame = eval_code2_setup(co,
			  globals, locals,
			  (PyObject **)NULL, 0,
			  (PyObject **)NULL, 0,
			  (PyObject **)NULL, 0,
			  (PyObject *)NULL);
	if (frame != NULL)
		return Py_UnwindToken;
	else
		return NULL;
}




/* 
	no longer the Interpreter main loop.
	For stackless Python, this is split into
	a number of functions.
 */

/* 
this function returns a prepared frame object.
Callers might want to modify the frame. Then,
they may forget it (since it is on the frame stack)
and should return Py_UnwindToken instead.
*/

static PyObject *
eval_code2_setup(PyCodeObject *co, PyObject *globals, PyObject *locals,
				 PyObject **args, int argcount, PyObject **kws, /* length: 2*kwcount */
				 int kwcount, PyObject **defs, int defcount, PyObject *owner)
{
#ifdef DXPAIRS
	int lastopcode = 0;
#endif
	register PyObject *x;	/* Result object -- NULL if error */
	register PyObject *u;
	register PyFrameObject *f; /* Current frame */
	register PyObject **fastlocals;
	PyThreadState *tstate = PyThreadState_GET();
	unsigned char *first_instr;
#ifdef LLTRACE
	int lltrace;
#endif
#if defined(Py_DEBUG) || defined(LLTRACE)
	/* Make it easier to find out where we are with a debugger */
	char *filename = PyString_AsString(co->co_filename);
#endif

/* Code access macros */

#define GETCONST(i)	Getconst(f, i)
#define GETNAME(i)	Getname(f, i)
#define GETNAMEV(i)	Getnamev(f, i)
#define INSTR_OFFSET()	(next_instr - first_instr)

/* #define NEXTOP() (*next_instr++) */
//CT BEGIN
#define NEXTOP() (*next_instr) /* CT no increment but in op */
//CT END

/* #define NEXTARG()	(next_instr += 2, (next_instr[-1]<<8) | next_instr[-2]) */
//CT BEGIN
#define NEXTARG()	(next_instr += 3, (((unsigned short*)(&next_instr[-2]))[0]))
#define THISARG()	((next_instr[-1]<<8) + next_instr[-2])
#define PREPARE(op) (HAS_ARG(op) ? (oparg=NEXTARG()) : (next_instr+=1, 0))
//CT END
#define THISARG()	((next_instr[-1]<<8) + next_instr[-2])

#define JUMPTO(x)	(next_instr = first_instr + (x))
#define JUMPBY(x)	(next_instr += (x))

/* Stack manipulation macros */

#define STACK_LEVEL()	(stack_pointer - f->f_valuestack)
#define EMPTY()		(STACK_LEVEL() == 0)
#define TOP()		(stack_pointer[-1])
#define BASIC_PUSH(v)	(*stack_pointer++ = (v))
#define BASIC_POP()	(*--stack_pointer)

#ifdef LLTRACE
#define PUSH(v)		(BASIC_PUSH(v), lltrace && prtrace(TOP(), "push"))
#define POP()		(lltrace && prtrace(TOP(), "pop"), BASIC_POP())
#else
#define PUSH(v)		BASIC_PUSH(v)
#define POP()		BASIC_POP()
#endif

/* Local variable macros */

#define GETLOCAL(i)	(fastlocals[i])
#define SETLOCAL(i, value)	do { Py_XDECREF(GETLOCAL(i)); \
				     GETLOCAL(i) = value; } while (0)

/* Start of code */

#ifdef USE_STACKCHECK
	if (tstate->recursion_depth%10 == 0 && PyOS_CheckStack()) {
		PyErr_SetString(PyExc_MemoryError, "Stack overflow");
		return NULL;
	}
#endif

	if (globals == NULL) {
		PyErr_SetString(PyExc_SystemError, "eval_code2: NULL globals");
		return NULL;
	}

#ifdef LLTRACE
	lltrace = PyDict_GetItemString(globals, "__lltrace__") != NULL;
#endif

	f = PyFrame_New(
			tstate,			/*back*/
			co,				/*code*/
			globals,		/*globals*/
			locals);		/*locals*/
	if (f == NULL)
		return NULL;

	f->f_execute = eval_code2_loop;
	tstate->frame = f;
	fastlocals = f->f_localsplus;

	if (co->co_argcount > 0 ||
	    co->co_flags & (CO_VARARGS | CO_VARKEYWORDS)) {
		int i;
		int n = argcount;
		PyObject *kwdict = NULL;
		if (co->co_flags & CO_VARKEYWORDS) {
			kwdict = PyDict_New();
			if (kwdict == NULL)
				goto fail;
			i = co->co_argcount;
			if (co->co_flags & CO_VARARGS)
				i++;
			SETLOCAL(i, kwdict);
		}
		if (argcount > co->co_argcount) {
			if (!(co->co_flags & CO_VARARGS)) {
				PyErr_Format(PyExc_TypeError,
				"too many arguments; expected %d, got %d",
					     co->co_argcount, argcount);
				goto fail;
			}
			n = co->co_argcount;
		}
		for (i = 0; i < n; i++) {
			x = args[i];
			Py_INCREF(x);
			SETLOCAL(i, x);
		}
		if (co->co_flags & CO_VARARGS) {
			u = PyTuple_New(argcount - n);
			if (u == NULL)
				goto fail;
			SETLOCAL(co->co_argcount, u);
			for (i = n; i < argcount; i++) {
				x = args[i];
				Py_INCREF(x);
				PyTuple_SET_ITEM(u, i-n, x);
			}
		}
		for (i = 0; i < kwcount; i++) {
			PyObject *keyword = kws[2*i];
			PyObject *value = kws[2*i + 1];
			int j;
			if (keyword == NULL || !PyString_Check(keyword)) {
				PyErr_SetString(PyExc_TypeError,
						"keywords must be strings");
				goto fail;
			}
			/* XXX slow -- speed up using dictionary? */
			for (j = 0; j < co->co_argcount; j++) {
				PyObject *nm = PyTuple_GET_ITEM(
					co->co_varnames, j);
				if (PyObject_Compare(keyword, nm) == 0)
					break;
			}
			/* Check errors from Compare */
			if (PyErr_Occurred())
				goto fail;
			if (j >= co->co_argcount) {
				if (kwdict == NULL) {
					PyErr_Format(PyExc_TypeError,
					 "unexpected keyword argument: %.400s",
					 PyString_AsString(keyword));
					goto fail;
				}
				PyDict_SetItem(kwdict, keyword, value);
			}
			else {
				if (GETLOCAL(j) != NULL) {
					PyErr_Format(PyExc_TypeError,
						"keyword parameter redefined: %.400s",
							PyString_AsString(keyword));
					goto fail;
				}
				Py_INCREF(value);
				SETLOCAL(j, value);
			}
		}
		if (argcount < co->co_argcount) {
			int m = co->co_argcount - defcount;
			for (i = argcount; i < m; i++) {
				if (GETLOCAL(i) == NULL) {
					PyErr_Format(PyExc_TypeError,
				"not enough arguments; expected %d, got %d",
						     m, i);
					goto fail;
				}
			}
			if (n > m)
				i = n - m;
			else
				i = 0;
			for (; i < defcount; i++) {
				if (GETLOCAL(m+i) == NULL) {
					PyObject *def = defs[i];
					Py_INCREF(def);
					SETLOCAL(m+i, def);
				}
			}
		}
	}
	else {
		if (argcount > 0 || kwcount > 0) {
			PyErr_SetString(PyExc_TypeError,
					"no arguments expected");
			goto fail;
		}
	}

	if (tstate->sys_tracefunc != NULL) {
		/* tstate->sys_tracefunc, if defined, is a function that
		   will be called  on *every* entry to a code block.
		   Its return value, if not None, is a function that
		   will be called at the start of each executed line
		   of code.  (Actually, the function must return
		   itself in order to continue tracing.)
		   The trace functions are called with three arguments:
		   a pointer to the current frame, a string indicating
		   why the function is called, and an argument which
		   depends on the situation.  The global trace function
		   (sys.trace) is also called whenever an exception
		   is detected. */
		if (call_trace(&tstate->sys_tracefunc,
			       &f->f_trace, f, "call",
			       Py_None/*XXX how to compute arguments now?*/)) {
			/* Trace function raised an error */
			goto fail;
		}
	}

	if (tstate->sys_profilefunc != NULL) {
		/* Similar for sys_profilefunc, except it needn't return
		   itself and isn't called for "line" events */
		if (call_trace(&tstate->sys_profilefunc,
			       (PyObject**)0, f, "call",
			       Py_None/*XXX*/)) {
			goto fail;
		}
	}

	if (++tstate->recursion_depth > recursion_limit) {
		--tstate->recursion_depth;
		PyErr_SetString(PyExc_RuntimeError,
				"Maximum recursion depth exceeded");
		Py_XINCREF(f->f_back); /* CT 990620 */
		tstate->frame = f->f_back;
		Py_DECREF(f);
		return NULL;
	}

	_PyCode_GETCODEPTR(co, &first_instr);
	f->f_first_instr = first_instr;
	f->f_next_instr = first_instr;

	return (PyObject *) f ;

  fail: /* Jump here from prelude on failure */
	
	/* Restore previous frame and release the current one */

	Py_XINCREF(f->f_back); /* CT 990620 */
	tstate->frame = f->f_back;
	Py_DECREF(f);
	
	return NULL;
}
/* end of eval_code2_setup */


#define HANDLE_EXCEPTION {goto handle_exception;}
#define HANDLE_why(ret) {why = ret; goto handle_why;}
#define HANDLE_RETURN {goto handle_return;}
#define HANDLE_BREAK {goto handle_break;}
#define HANDLE_RERAISE {goto handle_reraise;}
#define HANDLE_CALL {goto handle_call;}
#define HANDLE_NOT {why = WHY_NOT; goto handle_why;}
#define HANDLE_INTERRUPT {goto handle_interrupt;}
#define HANDLE_LOCKED_FRAME {goto handle_locked_frame;}

#define FAST_CONTINUE {goto fast_opcode;}

static PyObject *
eval_code2_loop(PyFrameObject *f, PyObject *passed_retval)
/* passing a function return value back in */
{
#ifdef DXPAIRS
	int lastopcode = 0;
#endif
	register unsigned char *next_instr = f->f_next_instr;
	register int opcode;	/* Current opcode */
	register int oparg=0;	    /* Current opcode argument, if any */
	register PyObject **stack_pointer = f->f_stackpointer;
	register enum why_code why; /* Reason for block stack unwind */
	register int err=0;   	/* Error status -- nonzero if error */
	register PyObject *x;	/* Result object -- NULL if error */
	register PyObject *v;	/* Temporary objects popped off stack */
	register PyObject *w;
	register PyObject *u;
	register PyObject *t;
	register PyObject *stream = NULL;    /* for PRINT opcodes */
	register PyObject **fastlocals = f->f_localsplus;
	PyCodeObject *co = f->f_code;
	PyObject *retval = NULL;	/* Return value */
	PyThreadState *tstate = f->f_tstate;
	unsigned char *first_instr = f->f_first_instr;
#ifdef LLTRACE
	int lltrace = PyDict_GetItemString(f->f_globals, "__lltrace__") != NULL;
#endif
#if defined(Py_DEBUG) || defined(LLTRACE)
	/* Make it easier to find out where we are with a debugger */
	char *filename = PyString_AsString(co->co_filename);
#endif

	/* see if we are expecting a result */
	{
		int statusflags = f->f_statusflags;
		if (statusflags) {
			/* 20010105 we will NEVER run a locked frame */
			if (statusflags & FRAME_IS_LOCKED)
				HANDLE_LOCKED_FRAME;
			/* 991123 needed to capture a frame earlier, when the flags are still set */
			if (f->f_callguard != NULL) {
				err = f->f_callguard(f, 1); /* inside loop */
				if(err) {
					if(err==-42) {
						return passed_retval;
					}
					else
						HANDLE_EXCEPTION;
				}
			}
			f->f_statusflags = 0;
			if (statusflags & WANTS_RETURN_VALUE) {
				/* we must check if a value was not NULL */
				if (passed_retval != NULL) {
					if (statusflags & DROPS_RETURN_VALUE)
						Py_DECREF(passed_retval); /* this was an exec, don't push it */
					else
						PUSH(passed_retval); /* we are back from a function call */
				}
				else {
					HANDLE_EXCEPTION;
				}
			}
			else
				Py_XDECREF(passed_retval);

			if (statusflags & LOCALS_NEED_UPDATE)
				PyFrame_LocalsToFast(f, 0);
			/* stuff like this should later go into a general tail call
			   protocol. But this needs a lot of thought...
			 */
		}
	}
	
	for (;;) {

		/* Do periodic things.  Doing this every time through
		   the loop would add too much overhead, so we do it
		   only every Nth instruction.  We also do it if
		   ``things_to_do'' is set, i.e. when an asynchronous
		   event needs attention (e.g. a signal handler or
		   async I/O handler); see Py_AddPendingCall() and
		   Py_MakePendingCalls() above. */


		if (things_to_do || --tstate->ticker < 0) {

			/* y2k0302 added extra ticker support for microthreads */

			if (tstate->uthread_func 
				   && !tstate->uthread_lock
				/* y2k0326 inhibit uthreads in an exception context */
				   && !tstate->curexc_type) {
				int ticks = tstate->interp->checkinterval - tstate->ticker;
				int mt = tstate->uthread_ticker -= ticks;
				if (mt <= 0) {
					HANDLE_INTERRUPT;
				}
			}

			/* standard ticker code */

			tstate->ticker = tstate->interp->checkinterval;

			if (things_to_do) {
				if (Py_MakePendingCalls() < 0) {
					HANDLE_EXCEPTION;
				}
			}


#if !defined(HAVE_SIGNAL_H) || defined(macintosh)
			/* If we have true signals, the signal handler
			   will call Py_AddPendingCall() so we don't
			   have to call sigcheck().  On the Mac and
			   DOS, alas, we have to call it. */
			if (PyErr_CheckSignals()) {
				HANDLE_EXCEPTION;
			}
#endif

#ifdef WITH_THREAD
			if (interpreter_lock) {
				/* Give another thread a chance */

				if (PyThreadState_Swap(NULL) != tstate)
					Py_FatalError("ceval: tstate mix-up");
				PyThread_release_lock(interpreter_lock);

				/* Other threads may run now */

				PyThread_acquire_lock(interpreter_lock, 1);
				if (PyThreadState_Swap(tstate) != NULL)
					Py_FatalError("ceval: orphan tstate");
			}
#endif
		}

		/* Extract opcode and argument */

#if defined(Py_DEBUG) || defined(LLTRACE)
		f->f_lasti = INSTR_OFFSET();
#endif

fast_opcode:

		opcode = NEXTOP();
/*    	if (HAS_ARG(opcode)) */
/*  		oparg = NEXTARG(); */
//CT BEGIN
#ifdef LLTRACE
		if (HAS_ARG(opcode))
			oparg = THISARG();
#endif
//CT END

/* dispatch_opcode: */
	/* handled locally in extended_arg */

#ifdef LLTRACE
		if (HAS_ARG(opcode))
			oparg = THISARG();
#endif

#ifdef DYNAMIC_EXECUTION_PROFILE
#ifdef DXPAIRS
		dxpairs[lastopcode][opcode]++;
		lastopcode = opcode;
#endif
		dxp[opcode]++;
#endif

#ifdef LLTRACE
		/* Instruction tracing */
		
		if (lltrace) {
			if (HAS_ARG(opcode)) {
				printf("%d: %d, %d\n",
					(int) (INSTR_OFFSET()),
					opcode, oparg);
			}
			else {
				printf("%d: %d\n",
					(int) (INSTR_OFFSET()), opcode);
			}
		}
#endif

		/* Main switch on opcode */
		
		switch (opcode) {
		
		/* BEWARE!
		   It is essential that any operation that fails sets either
		   x to NULL, err to nonzero, or why to anything but WHY_NOT,
		   and that no operation that succeeds does this! */

		/* The above comment is no longer true in Stackless.
		   All errors must be handled in the switch cases. Exceptions
		   are handled by HANDLE_EXCEPTION.
		   Avoid any references to x and err outside a switch case. */
		
		case POP_TOP:
			PREPARE(POP_TOP);
			v = POP();
			Py_DECREF(v);
			FAST_CONTINUE;
		
		case ROT_TWO:
			PREPARE(ROT_TWO);
			v = POP();
			w = POP();
			PUSH(v);
			PUSH(w);
			FAST_CONTINUE;

		case ROT_THREE:
			PREPARE(ROT_THREE);
			v = POP();
			w = POP();
			x = POP();
			PUSH(v);
			PUSH(x);
			PUSH(w);
			FAST_CONTINUE;

		case ROT_FOUR:
			PREPARE(ROT_FOUR);
			u = POP();
			v = POP();
			w = POP();
			x = POP();
			PUSH(u);
			PUSH(x);
			PUSH(w);
			PUSH(v);
			FAST_CONTINUE;

		case DUP_TOP:
			PREPARE(DUP_TOP);
			v = TOP();
			Py_INCREF(v);
			PUSH(v);
			FAST_CONTINUE;
		
		case DUP_TOPX:
			PREPARE(DUP_TOPX);
			switch (oparg) {
			case 1:
				x = TOP();
				Py_INCREF(x);
				PUSH(x);
				FAST_CONTINUE;
			case 2:
				x = POP();
				Py_INCREF(x);
				w = TOP();
				Py_INCREF(w);
				PUSH(x);
				PUSH(w);
				PUSH(x);
				FAST_CONTINUE;
			case 3:
				x = POP();
				Py_INCREF(x);
				w = POP();
				Py_INCREF(w);
				v = TOP();
				Py_INCREF(v);
				PUSH(w);
				PUSH(x);
				PUSH(v);
				PUSH(w);
				PUSH(x);
				FAST_CONTINUE;
			case 4:
				x = POP();
				Py_INCREF(x);
				w = POP();
				Py_INCREF(w);
				v = POP();
				Py_INCREF(v);
				u = TOP();
				Py_INCREF(u);
				PUSH(v);
				PUSH(w);
				PUSH(x);
				PUSH(u);
				PUSH(v);
				PUSH(w);
				PUSH(x);
				FAST_CONTINUE;
			case 5:
				x = POP();
				Py_INCREF(x);
				w = POP();
				Py_INCREF(w);
				v = POP();
				Py_INCREF(v);
				u = POP();
				Py_INCREF(u);
				t = TOP();
				Py_INCREF(t);
				PUSH(u);
				PUSH(v);
				PUSH(w);
				PUSH(x);
				PUSH(t);
				PUSH(u);
				PUSH(v);
				PUSH(w);
				PUSH(x);
				FAST_CONTINUE;
			default:
				Py_FatalError("invalid argument to DUP_TOPX"
					      " (bytecode corruption?)");
			}
			HANDLE_EXCEPTION;

		case UNARY_POSITIVE:
			PREPARE(UNARY_POSITIVE);
			v = POP();
			x = PyNumber_Positive(v);
			Py_DECREF(v);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case UNARY_NEGATIVE:
			PREPARE(UNARY_NEGATIVE);
			v = POP();
			x = PyNumber_Negative(v);
			Py_DECREF(v);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case UNARY_NOT:
			PREPARE(UNARY_NOT);
			v = POP();
			err = PyObject_IsTrue(v);
			Py_DECREF(v);
			if (err == 0) {
				Py_INCREF(Py_True);
				PUSH(Py_True);
				FAST_CONTINUE;
			}
			else if (err > 0) {
				Py_INCREF(Py_False);
				PUSH(Py_False);
				FAST_CONTINUE;
			}
			HANDLE_EXCEPTION;
		
		case UNARY_CONVERT:
			PREPARE(UNARY_CONVERT);
			v = POP();
			x = PyObject_Repr(v);
			Py_DECREF(v);
			PUSH(x);
			if (x != NULL) continue;
			HANDLE_EXCEPTION;
			
		case UNARY_INVERT:
			PREPARE(UNARY_INVERT);
			v = POP();
			x = PyNumber_Invert(v);
			Py_DECREF(v);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case BINARY_POWER:
			PREPARE(BINARY_POWER);
			w = POP();
			v = POP();
			x = PyNumber_Power(v, w, Py_None);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case BINARY_MULTIPLY:
			PREPARE(BINARY_MULTIPLY);
			w = POP();
			v = POP();
			x = PyNumber_Multiply(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case BINARY_DIVIDE:
			PREPARE(BINARY_DIVIDE);
			w = POP();
			v = POP();
			x = PyNumber_Divide(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case BINARY_MODULO:
			PREPARE(BINARY_MODULO);
			w = POP();
			v = POP();
			x = PyNumber_Remainder(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case BINARY_ADD:
			PREPARE(BINARY_ADD);
			w = POP();
			v = POP();
			if (PyInt_Check(v) && PyInt_Check(w)) {
				/* INLINE: int + int */
				register long a, b, i;
				a = PyInt_AS_LONG(v);
				b = PyInt_AS_LONG(w);
				i = a + b;
				if (INT_CHECK_FLAG && (i^a) < 0 && (i^b) < 0) {
					PyErr_SetString(PyExc_OverflowError,
							"integer addition");
					x = NULL;
				}
				else {
					x = PyInt_FromLong(i);
					Py_DECREF(v);
					Py_DECREF(w);
					PUSH(x);
					if (x != NULL) FAST_CONTINUE;
					HANDLE_EXCEPTION;
				}
			}
			else
				x = PyNumber_Add(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			/* might be slow on sequence add, so: */
			if (x != NULL) continue;
			HANDLE_EXCEPTION;
		
		case BINARY_SUBTRACT:
			PREPARE(BINARY_SUBTRACT);
			w = POP();
			v = POP();
			if (PyInt_Check(v) && PyInt_Check(w)) {
				/* INLINE: int - int */
				register long a, b, i;
				a = PyInt_AS_LONG(v);
				b = PyInt_AS_LONG(w);
				i = a - b;
				if (INT_CHECK_FLAG && (i^a) < 0 && (i^~b) < 0) {
					PyErr_SetString(PyExc_OverflowError,
							"integer subtraction");
					x = NULL;
				}
				else
					x = PyInt_FromLong(i);
			}
			else
				x = PyNumber_Subtract(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case BINARY_SUBSCR:
			PREPARE(BINARY_SUBSCR);
			w = POP();
			v = POP();
			if (PyList_Check(v) && PyInt_Check(w)) {
				/* INLINE: list[int] */
				long i = PyInt_AsLong(w);
				if (i < 0)
					i += PyList_GET_SIZE(v);
				if (i < 0 ||
				    i >= PyList_GET_SIZE(v)) {
					PyErr_SetString(PyExc_IndexError,
						"list index out of range");
					x = NULL;
				}
				else {
					x = PyList_GET_ITEM(v, i);
					Py_INCREF(x);
					/* quick op */
					Py_DECREF(v);
					Py_DECREF(w);
					PUSH(x);
					FAST_CONTINUE;
				}
			}
			else
				x = PyObject_GetItem(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case BINARY_LSHIFT:
			PREPARE(BINARY_LSHIFT);
			w = POP();
			v = POP();
			x = PyNumber_Lshift(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case BINARY_RSHIFT:
			PREPARE(BINARY_RSHIFT);
			w = POP();
			v = POP();
			x = PyNumber_Rshift(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case BINARY_AND:
			PREPARE(BINARY_AND);
			w = POP();
			v = POP();
			x = PyNumber_And(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case BINARY_XOR:
			PREPARE(BINARY_XOR);
			w = POP();
			v = POP();
			x = PyNumber_Xor(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case BINARY_OR:
			PREPARE(BINARY_OR);
			w = POP();
			v = POP();
			x = PyNumber_Or(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case INPLACE_POWER:
			PREPARE(INPLACE_POWER);
			w = POP();
			v = POP();
			x = PyNumber_InPlacePower(v, w, Py_None);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) continue;
			HANDLE_EXCEPTION;
		
		case INPLACE_MULTIPLY:
			PREPARE(INPLACE_MULTIPLY);
			w = POP();
			v = POP();
			x = PyNumber_InPlaceMultiply(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case INPLACE_DIVIDE:
			PREPARE(INPLACE_DIVIDE);
			w = POP();
			v = POP();
			x = PyNumber_InPlaceDivide(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case INPLACE_MODULO:
			PREPARE(INPLACE_MODULO);
			w = POP();
			v = POP();
			x = PyNumber_InPlaceRemainder(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case INPLACE_ADD:
			PREPARE(INPLACE_ADD);
			w = POP();
			v = POP();
			if (PyInt_Check(v) && PyInt_Check(w)) {
				/* INLINE: int + int */
				register long a, b, i;
				a = PyInt_AS_LONG(v);
				b = PyInt_AS_LONG(w);
				i = a + b;
				if ((i^a) < 0 && (i^b) < 0) {
					PyErr_SetString(PyExc_OverflowError,
							"integer addition");
					x = NULL;
				}
				else
					x = PyInt_FromLong(i);
			}
			else
				x = PyNumber_InPlaceAdd(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case INPLACE_SUBTRACT:
			PREPARE(INPLACE_SUBTRACT);
			w = POP();
			v = POP();
			if (PyInt_Check(v) && PyInt_Check(w)) {
				/* INLINE: int - int */
				register long a, b, i;
				a = PyInt_AS_LONG(v);
				b = PyInt_AS_LONG(w);
				i = a - b;
				if ((i^a) < 0 && (i^~b) < 0) {
					PyErr_SetString(PyExc_OverflowError,
							"integer subtraction");
					x = NULL;
				}
				else
					x = PyInt_FromLong(i);
			}
			else
				x = PyNumber_InPlaceSubtract(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case INPLACE_LSHIFT:
			PREPARE(INPLACE_LSHIFT);
			w = POP();
			v = POP();
			x = PyNumber_InPlaceLshift(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case INPLACE_RSHIFT:
			PREPARE(INPLACE_RSHIFT);
			w = POP();
			v = POP();
			x = PyNumber_InPlaceRshift(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case INPLACE_AND:
			PREPARE(INPLACE_AND);
			w = POP();
			v = POP();
			x = PyNumber_InPlaceAnd(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case INPLACE_XOR:
			PREPARE(INPLACE_XOR);
			w = POP();
			v = POP();
			x = PyNumber_InPlaceXor(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case INPLACE_OR:
			PREPARE(INPLACE_OR);
			w = POP();
			v = POP();
			x = PyNumber_InPlaceOr(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;


		case SLICE+0:
		case SLICE+1:
		case SLICE+2:
		case SLICE+3:
			PREPARE(SLICE+3);
			if ((opcode-SLICE) & 2)
				w = POP();
			else
				w = NULL;
			if ((opcode-SLICE) & 1)
				v = POP();
			else
				v = NULL;
			u = POP();
			x = apply_slice(u, v, w);
			Py_DECREF(u);
			Py_XDECREF(v);
			Py_XDECREF(w);
			PUSH(x);
			if (x != NULL) continue;
			HANDLE_EXCEPTION;
		
		case STORE_SLICE+0:
		case STORE_SLICE+1:
		case STORE_SLICE+2:
		case STORE_SLICE+3:
			PREPARE(STORE_SLICE+3);
			if ((opcode-STORE_SLICE) & 2)
				w = POP();
			else
				w = NULL;
			if ((opcode-STORE_SLICE) & 1)
				v = POP();
			else
				v = NULL;
			u = POP();
			t = POP();
			err = assign_slice(u, v, w, t); /* u[v:w] = t */
			Py_DECREF(t);
			Py_DECREF(u);
			Py_XDECREF(v);
			Py_XDECREF(w);
			if (err == 0) continue;
			HANDLE_EXCEPTION;
		
		case DELETE_SLICE+0:
		case DELETE_SLICE+1:
		case DELETE_SLICE+2:
		case DELETE_SLICE+3:
			PREPARE(DELETE_SLICE+3);
			if ((opcode-DELETE_SLICE) & 2)
				w = POP();
			else
				w = NULL;
			if ((opcode-DELETE_SLICE) & 1)
				v = POP();
			else
				v = NULL;
			u = POP();
			err = assign_slice(u, v, w, (PyObject *)NULL);
							/* del u[v:w] */
			Py_DECREF(u);
			Py_XDECREF(v);
			Py_XDECREF(w);
			if (err == 0) continue;
			HANDLE_EXCEPTION;
		
		case STORE_SUBSCR:
			PREPARE(STORE_SUBSCR);
			w = POP();
			v = POP();
			u = POP();
			/* v[w] = u */
			err = PyObject_SetItem(v, w, u);
			Py_DECREF(u);
			Py_DECREF(v);
			Py_DECREF(w);
			if (err == 0) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case DELETE_SUBSCR:
			PREPARE(DELETE_SUBSCR);
			w = POP();
			v = POP();
			/* del v[w] */
			err = PyObject_DelItem(v, w);
			Py_DECREF(v);
			Py_DECREF(w);
			if (err == 0) FAST_CONTINUE;
			HANDLE_EXCEPTION;

		case PRINT_EXPR:
			PREPARE(PRINT_EXPR);
			v = POP();
			err = 0;
			/* Print value except if None */
			/* After printing, also assign to '_' */
			/* Before, set '_' to None to avoid recursion */

			if (v != Py_None &&
				(err = PyDict_SetItemString(
					f->f_builtins, "_", Py_None)) == 0) {
				err = Py_FlushLine();
				if (err == 0) {
					x = PySys_GetObject("stdout");
					if (x == NULL) {
						PyErr_SetString(
							PyExc_RuntimeError,
							"lost sys.stdout");
						err = -1;
					}
				}
				if (err == 0)
					err = PyFile_WriteObject(v, x, 0);
				if (err == 0) {
					PyFile_SoftSpace(x, 1);
					err = Py_FlushLine();
				}
				if (err == 0) {
					err = PyDict_SetItemString(
						f->f_builtins, "_", v);
				}
			}
			Py_DECREF(v);
			if (err == 0) continue;
			HANDLE_EXCEPTION;
				
		case PRINT_ITEM_TO:
		case PRINT_ITEM:
			PREPARE(PRINT_ITEM);
			if (opcode == PRINT_ITEM_TO)
				w = stream = POP();
			else
				w = NULL;

			v = POP();
			err = 0;
			if (stream == NULL || stream == Py_None) {
				w = PySys_GetObject("stdout");
				if (w == NULL) {
					PyErr_SetString(PyExc_RuntimeError,
							"lost sys.stdout");
					err = -1;
				}
			}
			if (w != NULL && PyFile_SoftSpace(w, 1))
				err = PyFile_WriteString(" ", w);
			if (err == 0)
				err = PyFile_WriteObject(v, w, Py_PRINT_RAW);
			if (err == 0 && PyString_Check(v)) {
				/* XXX move into writeobject() ? */
				char *s = PyString_AsString(v);
				int len = PyString_Size(v);
				if (len > 0 &&
				    isspace(Py_CHARMASK(s[len-1])) &&
				    s[len-1] != ' ')
					PyFile_SoftSpace(w, 0);
			}
			Py_DECREF(v);
			Py_XDECREF(stream);
			stream = NULL;
			if (err == 0)
				continue;
			HANDLE_EXCEPTION;

		case PRINT_NEWLINE_TO:
		case PRINT_NEWLINE:
			PREPARE(PRINT_NEWLINE);
			if (opcode == PRINT_NEWLINE_TO)
				w = stream = POP();
			else
				w = NULL;

			if (stream == NULL || stream == Py_None) {
				w = PySys_GetObject("stdout");
				if (w == NULL) {
					PyErr_SetString(PyExc_RuntimeError,
							"lost sys.stdout");
					HANDLE_EXCEPTION;
				}
			}
			if (w != NULL) {
				err = PyFile_WriteString("\n", w);
				if (err == 0) {
					PyFile_SoftSpace(w, 0);
					Py_XDECREF(stream);
					stream = NULL;
					continue;
				}
			}
			Py_XDECREF(stream);
			stream = NULL;
			HANDLE_EXCEPTION;

		case BREAK_LOOP:
			PREPARE(BREAK_LOOP);
			HANDLE_BREAK;
			
		case RAISE_VARARGS:
			PREPARE(RAISE_VARARGS);
			u = v = w = NULL;
			switch (oparg) {
			case 3:
				u = POP(); /* traceback */
				/* Fallthrough */
			case 2:
				v = POP(); /* value */
				/* Fallthrough */
			case 1:
				w = POP(); /* exc */
			case 0: /* Fallthrough */
				HANDLE_why (do_raise(w, v, u));
				
			default:
				PyErr_SetString(PyExc_SystemError,
					   "bad RAISE_VARARGS oparg");
				HANDLE_EXCEPTION;
			}
			break;
		
		case LOAD_LOCALS:
			PREPARE(LOAD_LOCALS);
			if ((x = f->f_locals) == NULL) {
				PyErr_SetString(PyExc_SystemError,
						"no locals");
				HANDLE_EXCEPTION;
			}
			Py_INCREF(x);
			PUSH(x);
			FAST_CONTINUE;
		
		case RETURN_VALUE:
			PREPARE(RETURN_VALUE);
			retval = POP();
			HANDLE_RETURN;

		case EXEC_STMT:
			PREPARE(EXEC_STMT);
			w = POP();
			v = POP();
			u = POP();
			err = exec_statement(f, u, v, w);
			Py_DECREF(u);
			Py_DECREF(v);
			Py_DECREF(w);
			if (err == -42) {

				f->f_statusflags |= DROPS_RETURN_VALUE;
				HANDLE_CALL;
			}
			if (err == 0) continue;
			HANDLE_EXCEPTION;
		
		case POP_BLOCK:
			PREPARE(POP_BLOCK);
			{
				PyTryBlock *b = PyFrame_BlockPop(f);
				while (STACK_LEVEL() > b->b_level) {
					v = POP();
					Py_DECREF(v);
				}
			}
			FAST_CONTINUE;
		
		case END_FINALLY:
			PREPARE(END_FINALLY);
			v = POP();
			if (PyInt_Check(v)) {
				if ((enum why_code) PyInt_AsLong(v) == WHY_RETURN) {
					retval = POP();
					Py_DECREF(v);
					HANDLE_RETURN;
				}
			}
			else if (PyString_Check(v) || PyClass_Check(v)) {
				w = POP();
				u = POP();
				PyErr_Restore(v, w, u);
				HANDLE_RERAISE;
			}
			else if (v != Py_None) {
				PyErr_SetString(PyExc_SystemError,
					"'finally' pops bad exception");
				Py_DECREF(v);
				HANDLE_EXCEPTION;
			}
			Py_DECREF(v);
			FAST_CONTINUE;
		
		case BUILD_CLASS:
			PREPARE(BUILD_CLASS);
			u = POP();
			v = POP();
			w = POP();
			x = build_class(u, v, w);
			PUSH(x);
			Py_DECREF(u);
			Py_DECREF(v);
			Py_DECREF(w);
			if (x != NULL) continue;
			HANDLE_EXCEPTION;
		
		case STORE_NAME:
			PREPARE(STORE_NAME);
			w = GETNAMEV(oparg);
			v = POP();
			if ((x = f->f_locals) == NULL) {
				PyErr_SetString(PyExc_SystemError,
						"no locals");
				HANDLE_EXCEPTION;
			}
			err = PyDict_SetItem(x, w, v);
			Py_DECREF(v);
			if (err == 0) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case DELETE_NAME:
			PREPARE(DELETE_NAME);
			w = GETNAMEV(oparg);
			if ((x = f->f_locals) == NULL) {
				PyErr_SetString(PyExc_SystemError,
						"no locals");
				HANDLE_EXCEPTION;
			}
			if ((err = PyDict_DelItem(x, w)) != 0) {
				format_exc_check_arg(PyExc_NameError, 
							NAME_ERROR_MSG ,w);
				HANDLE_EXCEPTION;
			}
			FAST_CONTINUE;

		case UNPACK_SEQUENCE:
			PREPARE(UNPACK_SEQUENCE);
			v = POP();
			if (PyTuple_Check(v)) {
				if (PyTuple_Size(v) != oparg) {
					PyErr_SetString(PyExc_ValueError,
						 "unpack tuple of wrong size");
					Py_DECREF(v);
					HANDLE_EXCEPTION;
				}
				else {
					for (; --oparg >= 0; ) {
						w = PyTuple_GET_ITEM(v, oparg);
						Py_INCREF(w);
						PUSH(w);
					}
				}
			}
			else if (PyList_Check(v)) {
				if (PyList_Size(v) != oparg) {
					PyErr_SetString(PyExc_ValueError,
						  "unpack list of wrong size");
					Py_DECREF(v);
					HANDLE_EXCEPTION;
				}
				else {
					for (; --oparg >= 0; ) {
						w = PyList_GET_ITEM(v, oparg);
						Py_INCREF(w);
						PUSH(w);
					}
				}
			}
			else if (PySequence_Check(v)) {
				if (unpack_sequence(v, oparg,
						    stack_pointer + oparg))
					stack_pointer += oparg;
				else {
					Py_DECREF(v);
					HANDLE_EXCEPTION;
				}
			}
			else {
				PyErr_SetString(PyExc_TypeError,
						"unpack non-sequence");
				Py_DECREF(v);
				HANDLE_EXCEPTION;
			}
			Py_DECREF(v);
			continue;
		
		case STORE_ATTR:
			PREPARE(STORE_ATTR);
			w = GETNAMEV(oparg);
			v = POP();
			u = POP();
			err = PyObject_SetAttr(v, w, u); /* v.w = u */
			Py_DECREF(v);
			Py_DECREF(u);
			if (err == 0) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case DELETE_ATTR:
			PREPARE(DELETE_ATTR);
			w = GETNAMEV(oparg);
			v = POP();
			err = PyObject_SetAttr(v, w, (PyObject *)NULL);
							/* del v.w */
			Py_DECREF(v);
			if (err == 0) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case STORE_GLOBAL:
			PREPARE(STORE_GLOBAL);
			w = GETNAMEV(oparg);
			v = POP();
			err = PyDict_SetItem(f->f_globals, w, v);
			Py_DECREF(v);
			if (err == 0) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case DELETE_GLOBAL:
			PREPARE(DELETE_GLOBAL);
			w = GETNAMEV(oparg);
			if ((err = PyDict_DelItem(f->f_globals, w)) != 0)
				format_exc_check_arg(
				    PyExc_NameError, NAME_ERROR_MSG ,w);
			if (err == 0) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case LOAD_CONST:
			PREPARE(LOAD_CONST);
			x = GETCONST(oparg);
			Py_INCREF(x);
			PUSH(x);
			FAST_CONTINUE;

		case LOAD_NAME:
			PREPARE(LOAD_NAME);
			w = GETNAMEV(oparg);
			if ((x = f->f_locals) == NULL) {
				PyErr_SetString(PyExc_SystemError,
						"no locals");
				HANDLE_EXCEPTION;
			}
			x = PyDict_GetItem(x, w);
			if (x == NULL) {
				x = PyDict_GetItem(f->f_globals, w);
				if (x == NULL) {
					x = PyDict_GetItem(f->f_builtins, w);
					if (x == NULL) {
						format_exc_check_arg(
							    PyExc_NameError, 
							    NAME_ERROR_MSG ,w);
						HANDLE_EXCEPTION;
					}
				}
			}
			Py_INCREF(x);
			PUSH(x);
			FAST_CONTINUE;
		
		case LOAD_GLOBAL:
			PREPARE(LOAD_GLOBAL);
			w = GETNAMEV(oparg);
			x = PyDict_GetItem(f->f_globals, w);
			if (x == NULL) {
				x = PyDict_GetItem(f->f_builtins, w);
				if (x == NULL) {
					format_exc_check_arg(
						    PyExc_NameError, 
						    NAME_ERROR_MSG ,w);
					HANDLE_EXCEPTION;
				}
			}
			Py_INCREF(x);
			PUSH(x);
			FAST_CONTINUE;

		case LOAD_FAST:
			PREPARE(LOAD_FAST);
			x = GETLOCAL(oparg);
			if (x == NULL) {
				format_exc_check_arg(
					PyExc_UnboundLocalError,
					UNBOUNDLOCAL_ERROR_MSG,
					PyTuple_GetItem(co->co_varnames, oparg)
					);
				HANDLE_EXCEPTION;
			}
			Py_INCREF(x);
			PUSH(x);
			FAST_CONTINUE;

		case STORE_FAST:
			PREPARE(STORE_FAST);
			v = POP();
			SETLOCAL(oparg, v);
			FAST_CONTINUE;

		case DELETE_FAST:
			PREPARE(DELETE_FAST);
			x = GETLOCAL(oparg);
			if (x == NULL) {
				format_exc_check_arg(
					PyExc_UnboundLocalError,
					UNBOUNDLOCAL_ERROR_MSG,
					PyTuple_GetItem(co->co_varnames, oparg)
					);
				HANDLE_EXCEPTION;
			}
			SETLOCAL(oparg, NULL);
			FAST_CONTINUE;
		
		case BUILD_TUPLE:
			PREPARE(BUILD_TUPLE);
			x = PyTuple_New(oparg);
			if (x != NULL) {
				for (; --oparg >= 0;) {
					w = POP();
					PyTuple_SET_ITEM(x, oparg, w);
				}
				PUSH(x);
				FAST_CONTINUE;
			}
			HANDLE_EXCEPTION;
		
		case BUILD_LIST:
			PREPARE(BUILD_LIST);
			x =  PyList_New(oparg);
			if (x != NULL) {
				for (; --oparg >= 0;) {
					w = POP();
					PyList_SET_ITEM(x, oparg, w);
				}
				PUSH(x);
				FAST_CONTINUE;
			}
			HANDLE_EXCEPTION;
		
		case BUILD_MAP:
			PREPARE(BUILD_MAP);
			x = PyDict_New();
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case LOAD_ATTR:
			PREPARE(LOAD_ATTR);
			w = GETNAMEV(oparg);
			v = POP();
			x = PyObject_GetAttr(v, w);
			Py_DECREF(v);
			PUSH(x);
			if (x != NULL) FAST_CONTINUE;
			HANDLE_EXCEPTION;
		
		case COMPARE_OP:
			PREPARE(COMPARE_OP);
			w = POP();
			v = POP();
			if (PyInt_Check(v) && PyInt_Check(w)) {
				/* INLINE: cmp(int, int) */
				register long a, b;
				register int res;
				a = PyInt_AS_LONG(v);
				b = PyInt_AS_LONG(w);
				switch (oparg) {
				case LT: res = a <  b; break;
				case LE: res = a <= b; break;
				case EQ: res = a == b; break;
				case NE: res = a != b; break;
				case GT: res = a >  b; break;
				case GE: res = a >= b; break;
				case IS: res = v == w; break;
				case IS_NOT: res = v != w; break;
				default: goto slow_compare;
				}
				x = res ? Py_True : Py_False;
				Py_INCREF(x);
				/* fast path */
				Py_DECREF(v);
				Py_DECREF(w);
				PUSH(x);
				FAST_CONTINUE;
			}
			else {
			  slow_compare:
				x = cmp_outcome(oparg, v, w);
			}
			Py_DECREF(v);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) continue;
			HANDLE_EXCEPTION;
		
		case IMPORT_NAME:
			PREPARE(IMPORT_NAME);
			w = GETNAMEV(oparg);
			x = PyDict_GetItemString(f->f_builtins, "__import__");
			if (x == NULL) {
				PyErr_SetString(PyExc_ImportError,
						"__import__ not found");
				HANDLE_EXCEPTION;
			}
			u = POP();
			w = Py_BuildValue("(OOOO)",
				    w,
				    f->f_globals,
				    f->f_locals == NULL ?
					  Py_None : f->f_locals,
				    u);
			Py_DECREF(u);
			if (w == NULL) {
				HANDLE_EXCEPTION;
			}
			x = PyEval_CallObject(x, w);
			Py_DECREF(w);
			PUSH(x);
			if (x != NULL) continue;
			HANDLE_EXCEPTION;

		case IMPORT_STAR:
			PREPARE(IMPORT_STAR);
			v = POP();
			PyFrame_FastToLocals(f);
			if ((x = f->f_locals) == NULL) {
				PyErr_SetString(PyExc_SystemError,
						"no locals");
				HANDLE_EXCEPTION;
			}
			err = import_all_from(x, v);
			PyFrame_LocalsToFast(f, 0);
			Py_DECREF(v);
			if (err == 0) continue;
			HANDLE_EXCEPTION;

		case IMPORT_FROM:
			PREPARE(IMPORT_FROM);
			w = GETNAMEV(oparg);
			v = TOP();
			x = import_from(v, w);
			PUSH(x);
			if (x != NULL) continue;
			HANDLE_EXCEPTION;

		case JUMP_FORWARD:
			PREPARE(JUMP_FORWARD);
			JUMPBY(oparg);
			FAST_CONTINUE;
		
		case JUMP_IF_FALSE:
			PREPARE(JUMP_IF_FALSE);
			err = PyObject_IsTrue(TOP());
			if (err > 0)
				;
			else if (err == 0)
				JUMPBY(oparg);
			else
				HANDLE_EXCEPTION;
			FAST_CONTINUE;
		
		case JUMP_IF_TRUE:
			PREPARE(JUMP_IF_TRUE);
			err = PyObject_IsTrue(TOP());
			if (err > 0)
				JUMPBY(oparg);
			else if (err == 0)
				;
			else
				HANDLE_EXCEPTION;
			FAST_CONTINUE;
		
		case JUMP_ABSOLUTE: 
			PREPARE(JUMP_ABSOLUTE);
			JUMPTO(oparg);
			continue;
		
		case FOR_LOOP:
			PREPARE(FOR_LOOP);
			/* for v in s: ...
			   On entry: stack contains s, i.
			   On exit: stack contains s, i+1, s[i];
			   but if loop exhausted:
			   	s, i are popped, and we jump */
			w = POP(); /* Loop index */
			v = POP(); /* Sequence object */

			/* counter will become a mutable LoopIndex */
			if (!PyLoopIndex_Check(w)) {
				x = NULL;
				if (PyInt_Check(w)) {
					x = PyLoopIndex_FromLong(PyInt_AS_LONG(w));
				}
				if (x == NULL) {
					Py_DECREF(w);
					Py_DECREF(v);
					HANDLE_EXCEPTION;  /* this is an error */
				}
				Py_DECREF(w);
				w = x;
			}
			u = loop_subscript(v, PyLoopIndex_AS_LONG(w));
			if (u != NULL) {
				PUSH(v);
				PyLoopIndex_INCREMENT(w);
				PUSH(w);
				PUSH(u);
			}
			else {
				Py_DECREF(v);
				Py_DECREF(w);
				/* A NULL can mean "s exhausted"
				   but also an error: */
				if (PyErr_Occurred()) {
					HANDLE_EXCEPTION;
				}
				else {
					JUMPBY(oparg);
				}
			}
			FAST_CONTINUE;
		
		case SETUP_LOOP:
		case SETUP_EXCEPT:
		case SETUP_FINALLY:
			PREPARE(SETUP_FINALLY);
			PyFrame_BlockSetup(f, opcode, INSTR_OFFSET() + oparg,
						STACK_LEVEL());
			FAST_CONTINUE;
		
		case SET_LINENO:
			PREPARE(SET_LINENO);
#ifdef LLTRACE
			if (lltrace)
				printf("--- %s:%d \n", filename, oparg);
#endif
			f->f_lineno = oparg;
			if (f->f_trace == NULL)
				FAST_CONTINUE;
			/* Trace each line of code reached */
			f->f_lasti = INSTR_OFFSET();
			err = call_trace(&f->f_trace, &f->f_trace,
					 f, "line", Py_None);
			if (err == 0) FAST_CONTINUE;
			HANDLE_EXCEPTION;

		case CALL_FUNCTION:
		case CALL_FUNCTION_VAR:
		case CALL_FUNCTION_KW:
		case CALL_FUNCTION_VAR_KW:
			PREPARE(CALL_FUNCTION_VAR_KW);
		{
			int na = oparg & 0xff;
			int nk = (oparg>>8) & 0xff;
		    int flags = (opcode - CALL_FUNCTION) & 3;
		    int n = na + 2*nk + (flags & 1) + ((flags >> 1) & 1);
			PyObject **pfunc = stack_pointer - n - 1;
			PyObject *func = *pfunc;
			PyObject *self = NULL;
			PyObject *class = NULL;
			f->f_lasti = INSTR_OFFSET(); /* For tracing */

			if (PyMethod_Check(func)) {
				self = PyMethod_Self(func);
				class = PyMethod_Class(func);
				func = PyMethod_Function(func);
				Py_INCREF(func);
				if (self != NULL) {
					Py_INCREF(self);
					Py_DECREF(*pfunc);
					*pfunc = self;
					na++;
					n++;
				}
				else {
					/* Unbound methods must be called with an
					   instance of the class (or a derived
					   class) as first argument */
					if (na > 0 && (self = stack_pointer[-n]) != NULL 
						&& PyInstance_Check(self) 
						&& PyClass_IsSubclass((PyObject *)
						(((PyInstanceObject *)self)->in_class),
						class))
						/* Handy-dandy */ ;
					else {
						PyErr_SetString(PyExc_TypeError,
							"unbound method must be called with class instance 1st argument");
						HANDLE_EXCEPTION;
					}
				}
			}
			else
				Py_INCREF(func);
			if (PyFunction_Check(func) && flags == 0) {
				PyObject *co = PyFunction_GetCode(func);
				PyObject *globals = PyFunction_GetGlobals(func);
				PyObject *argdefs = PyFunction_GetDefaults(func);
				PyObject **d;
				int nd;
				if (argdefs != NULL) {
					d = &PyTuple_GET_ITEM(argdefs, 0);
					nd = ((PyTupleObject *)argdefs)->ob_size;
				}
				else {
					d = NULL;
					nd = 0;
				}
				x = eval_code2_setup((PyCodeObject *)co, globals,
									 (PyObject *)NULL, stack_pointer-n, na,
									 stack_pointer-2*nk, nk, d, nd,
									 class);

				if (x != NULL)
					x = Py_UnwindToken;
			}
			else {
				int nstar = 0;
				PyObject *callargs;
				PyObject *stararg = 0;
				PyObject *kwdict = NULL;
				if (flags & 2) {
					kwdict = POP();
					if (!PyDict_Check(kwdict)) {
						PyErr_SetString(PyExc_TypeError,
							"** argument must be a dictionary");
						goto extcall_fail;
					}
				}
				if (flags & 1) {
					stararg = POP();
					if (!PySequence_Check(stararg)) {
						PyErr_SetString(PyExc_TypeError,
							"* argument must be a sequence");
						goto extcall_fail;
					}
					/* Convert abstract sequence to concrete tuple */
					if (!PyTuple_Check(stararg)) {
						PyObject *t = NULL;
						t = PySequence_Tuple(stararg);
						if (t == NULL) {
							goto extcall_fail;
						}
						Py_DECREF(stararg);
						stararg = t;
					}
					nstar = PyTuple_GET_SIZE(stararg);
					if (nstar < 0) {
						goto extcall_fail;
					}
				}
				if (nk > 0) {
					if (kwdict == NULL) {
						kwdict = PyDict_New();
						if (kwdict == NULL) {
							goto extcall_fail;
						}
					}
					else {
						PyObject *d = PyDict_Copy(kwdict);
						if (d == NULL) {
							goto extcall_fail;
						}
						Py_DECREF(kwdict);
						kwdict = d;
					}
					err = 0;
					while (--nk >= 0) {
						PyObject *value = POP();
						PyObject *key = POP();
						if (PyDict_GetItem(kwdict, key) != NULL) {
							err = 1;
							PyErr_Format(PyExc_TypeError,
								"keyword parameter redefined: %.400s",
								PyString_AsString(key));
							Py_DECREF(key);
							Py_DECREF(value);
							goto extcall_fail;
						}
						err = PyDict_SetItem(kwdict, key, value);
						Py_DECREF(key);
						Py_DECREF(value);
						if (err)
							HANDLE_EXCEPTION;
					}
					if (err) {
					  extcall_fail:
						Py_XDECREF(kwdict);
						Py_XDECREF(stararg);
						Py_DECREF(func);
						HANDLE_EXCEPTION;
					}
				}
				callargs = PyTuple_New(na + nstar);
				if (callargs == NULL) {
					HANDLE_EXCEPTION;
				}
				if (stararg) {
					int i;
					for (i = 0; i < nstar; i++) {
						PyObject *a = PyTuple_GET_ITEM(stararg, i);
						Py_INCREF(a);
						PyTuple_SET_ITEM(callargs, na + i, a);
					}
					Py_DECREF(stararg);
				}
				while (--na >= 0) {
					w = POP();
					PyTuple_SET_ITEM(callargs, na, w);
				}
				x = PyEval_CallObjectWithKeywords_nr(func,
					callargs,
					kwdict);  
				Py_DECREF(callargs);
				Py_XDECREF(kwdict);
			}
			Py_DECREF(func);
			while (stack_pointer > pfunc) {
				w = POP();
				Py_DECREF(w);
			}


			/*	stackless support: 
				We always leave the interpreter loop, when
				a Py_UnwindToken is returned.
			*/
	
			if (x == Py_UnwindToken) {
				HANDLE_CALL;
			}
			else if (f->f_callguard != NULL) {
				/* also protect normal calls 990712 */
				f->f_stackpointer = stack_pointer;
				f->f_next_instr = next_instr;
				f->f_temp_val = x;
				f->f_statusflags |= WANTS_RETURN_VALUE;
				err = f->f_callguard(f, 2);
				if(err) {
					if(err==-42) {
						return Py_UnwindToken;
					}
					else
						HANDLE_EXCEPTION;
				}
				f->f_statusflags &= ~WANTS_RETURN_VALUE;
			}
			PUSH(x);
			if (x != NULL) continue;
			HANDLE_EXCEPTION;
		}

		case MAKE_FUNCTION:
			PREPARE(MAKE_FUNCTION);
			v = POP(); /* code object */
			x = PyFunction_New(v, f->f_globals);
			Py_DECREF(v);
			/* XXX Maybe this should be a separate opcode? */
			if (x != NULL && oparg > 0) {
				v = PyTuple_New(oparg);
				if (v == NULL) {
					Py_DECREF(x);
					HANDLE_EXCEPTION;
				}
				while (--oparg >= 0) {
					w = POP();
					PyTuple_SET_ITEM(v, oparg, w);
				}
				err = PyFunction_SetDefaults(x, v);
				Py_DECREF(v);
				if (err)
					HANDLE_EXCEPTION;
			}
			PUSH(x);
			continue;

		case BUILD_SLICE:
			PREPARE(BUILD_SLICE);
			if (oparg == 3)
				w = POP();
			else
				w = NULL;
			v = POP();
			u = POP();
			x = PySlice_New(u, v, w);
			Py_DECREF(u);
			Py_DECREF(v);
			Py_XDECREF(w);
			PUSH(x);
			if (x != NULL) continue;
			HANDLE_EXCEPTION;

		case EXTENDED_ARG:
			PREPARE(EXTENDED_ARG);
			opcode = NEXTOP();
			oparg = oparg<<16 | NEXTARG();
/*  		goto dispatch_opcode; */
/*  		break; */

			switch (opcode) {

			case BUILD_TUPLE:
				x = PyTuple_New(oparg);
				if (x != NULL) {
					for (; --oparg >= 0;) {
						w = POP();
						PyTuple_SET_ITEM(x, oparg, w);
					}
					PUSH(x);
					FAST_CONTINUE;
				}
				HANDLE_EXCEPTION;
			
			case BUILD_LIST:
				x =  PyList_New(oparg);
				if (x != NULL) {
					for (; --oparg >= 0;) {
						w = POP();
						PyList_SET_ITEM(x, oparg, w);
					}
					PUSH(x);
					FAST_CONTINUE;
				}
				HANDLE_EXCEPTION;
			
			case JUMP_FORWARD:
				JUMPBY(oparg);
				FAST_CONTINUE;
			
			case JUMP_IF_FALSE:
				err = PyObject_IsTrue(TOP());
				if (err > 0)
					;
				else if (err == 0)
					JUMPBY(oparg);
				else
					HANDLE_EXCEPTION;
				FAST_CONTINUE;
			
			case JUMP_IF_TRUE:
				err = PyObject_IsTrue(TOP());
				if (err > 0)
					JUMPBY(oparg);
				else if (err == 0)
					;
				else
					HANDLE_EXCEPTION;
				FAST_CONTINUE;
			
			case JUMP_ABSOLUTE: 
				JUMPTO(oparg);
				continue;
			
			case FOR_LOOP:
				/* for v in s: ...
				   On entry: stack contains s, i.
				   On exit: stack contains s, i+1, s[i];
				   but if loop exhausted:
				   	s, i are popped, and we jump */
				w = POP(); /* Loop index */
				v = POP(); /* Sequence object */
	
				/* counter will become a mutable LoopIndex */
				if (!PyLoopIndex_Check(w)) {
					x = NULL;
					if (PyInt_Check(w)) {
						x = PyLoopIndex_FromLong(PyInt_AS_LONG(w));
					}
					if (x == NULL) {
						Py_DECREF(w);
						Py_DECREF(v);
						HANDLE_EXCEPTION;  /* this is an error */
					}
					Py_DECREF(w);
					w = x;
				}
				u = loop_subscript(v, PyLoopIndex_AS_LONG(w));
				if (u != NULL) {
					PUSH(v);
					PyLoopIndex_INCREMENT(w);
					PUSH(w);
					PUSH(u);
				}
				else {
					Py_DECREF(v);
					Py_DECREF(w);
					/* A NULL can mean "s exhausted"
					   but also an error: */
					if (PyErr_Occurred()) {
						HANDLE_EXCEPTION;
					}
					else {
						JUMPBY(oparg);
					}
				}
				FAST_CONTINUE;
			
			case SETUP_LOOP:
			case SETUP_EXCEPT:
			case SETUP_FINALLY:
				PyFrame_BlockSetup(f, opcode, INSTR_OFFSET() + oparg,
							STACK_LEVEL());
				FAST_CONTINUE;
			
			case SET_LINENO:
	#ifdef LLTRACE
				if (lltrace)
					printf("--- %s:%d \n", filename, oparg);
	#endif
				f->f_lineno = oparg;
				if (f->f_trace == NULL)
					FAST_CONTINUE;
				/* Trace each line of code reached */
				f->f_lasti = INSTR_OFFSET();
				err = call_trace(&f->f_trace, &f->f_trace,
						 f, "line", Py_None);
				if (err == 0) FAST_CONTINUE;
				HANDLE_EXCEPTION;
	
			default:
				PyErr_Format(PyExc_SystemError, "opcode %d not extended", opcode);
				HANDLE_EXCEPTION;
			}

		/* unused opcodes go here */
#ifdef WIN32
		default: __assume(0);
#endif
		case   0:;case   6:;case   7:;case   8:;case   9:;case  14:;case  16:;case  17:;case  18:;case  26:;
		case  27:;case  28:;case  29:;case  34:;case  35:;case  36:;case  37:;case  38:;case  39:;case  44:;
		case  45:;case  46:;case  47:;case  48:;case  49:;case  54:;case  68:;case  69:;case  81:;case  86:;
		case  93:;case  94:;case 109:;case 115:;case 117:;case 118:;case 119:;case 123:;case 128:;case 129:;
		case 134:;case 135:;case 136:;case 137:;case 138:;case 139:;case 144:;case 145:;case 146:;case 147:;
		case 148:;case 149:;case 150:;case 151:;case 152:;case 153:;case 154:;case 155:;case 156:;case 157:;
		case 158:;case 159:;case 160:;case 161:;case 162:;case 163:;case 164:;case 165:;case 166:;case 167:;
		case 168:;case 169:;case 170:;case 171:;case 172:;case 173:;case 174:;case 175:;case 176:;case 177:;
		case 178:;case 179:;case 180:;case 181:;case 182:;case 183:;case 184:;case 185:;case 186:;case 187:;
		case 188:;case 189:;case 190:;case 191:;case 192:;case 193:;case 194:;case 195:;case 196:;case 197:;
		case 198:;case 199:;case 200:;case 201:;case 202:;case 203:;case 204:;case 205:;case 206:;case 207:;
		case 208:;case 209:;case 210:;case 211:;case 212:;case 213:;case 214:;case 215:;case 216:;case 217:;
		case 218:;case 219:;case 220:;case 221:;case 222:;case 223:;case 224:;case 225:;case 226:;case 227:;
		case 228:;case 229:;case 230:;case 231:;case 232:;case 233:;case 234:;case 235:;case 236:;case 237:;
		case 238:;case 239:;case 240:;case 241:;case 242:;case 243:;case 244:;case 245:;case 246:;case 247:;
		case 248:;case 249:;case 250:;case 251:;case 252:;case 253:;case 254:;case 255:;

/*  	default: */
			fprintf(stderr,
				"XXX lineno: %d, opcode: %d\n",
				f->f_lineno, opcode);
			PyErr_SetString(PyExc_SystemError, "unknown opcode");
			HANDLE_EXCEPTION;

#ifdef CASE_TOO_BIG
		}
#endif

		} /* switch */

handle_locked_frame:
		/* this is a bad situation. Someone tried to run a frame
		   that has spawned a recursion. We set an error and abandon
		   the current frame chain, hoping that a dispatcher
		   frees the frame, which then handles the exception.
		 */
		PyErr_SetString(PyExc_SystemError, "Attempt to run a locked frame!");
		Py_XDECREF(passed_retval);
		Py_DECREF(f);
		tstate->frame = NULL;
		return NULL;

handle_return:
		why = WHY_RETURN;
		goto unwind_stacks;

handle_break:
		why = WHY_BREAK;
		goto unwind_stacks;

handle_why:
		switch ( why ) {

		case WHY_NOT:
			/* this case should have vanished on 19-01-Y2K CT */
			PyErr_SetString(PyExc_SystemError, "WHY_NOT deprecated");
			HANDLE_EXCEPTION;

		/* uthread support y2k0302 */
		case WHY_INTERRUPT:
handle_interrupt:
			v = tstate->uthread_func;
			tstate->uthread_func = NULL;
			f->f_stackpointer = stack_pointer;
			f->f_next_instr = next_instr;
			x = PyEval_CallObject_nr(v, NULL);
			Py_DECREF(v);
			if (x == NULL)
				HANDLE_EXCEPTION;
			if (x != Py_UnwindToken)
				Py_DECREF(x);
			if (f->f_callguard != NULL) {
				err = f->f_callguard(f, 1); /* inside loop */
				if(err) {
					if(err==-42) {
						return Py_UnwindToken;
					}
					else
						HANDLE_EXCEPTION;
				}
			}
			continue;

		/* stackless support: leave for a call */
		case WHY_CALL:
handle_call:
			f->f_stackpointer = stack_pointer;
			f->f_next_instr = next_instr;
			f->f_statusflags |= WANTS_RETURN_VALUE;
			if (f->f_callguard != NULL) {
				/* allow to save the stack or something */
				if (f->f_callguard(f, 0)) 
					return NULL;
			}
			return Py_UnwindToken;


		/* Double-check exception status */
		
		case WHY_EXCEPTION:
handle_exception:
			if (!PyErr_Occurred()) {
				PyErr_SetString(PyExc_SystemError,
					"error return without exception set");
				HANDLE_EXCEPTION;
			}
			why = WHY_EXCEPTION;
			break;

		case WHY_RERAISE:
			if (!PyErr_Occurred()) {
				PyErr_SetString(PyExc_SystemError,
					"error return without exception set");
				HANDLE_EXCEPTION;
			}
			goto handle_reraise;
		
		case WHY_BREAK:
			goto handle_break;

		case WHY_RETURN:
			goto handle_return;
		}

		/* Log traceback info if this is a real exception */

/*  	f->f_lasti = INSTR_OFFSET() - 1; */
/*  	if(HAS_ARG(opcode)) */
/*  		f->f_lasti -= 2; */
		f->f_lasti = -INSTR_OFFSET(); /* adjust later */
		PyTraceBack_Here(f);

		if (f->f_trace)
			call_exc_trace(&f->f_trace, &f->f_trace, f);
		if (tstate->sys_profilefunc)
			call_exc_trace(&tstate->sys_profilefunc,
					   (PyObject**)0, f);

		/* For the rest, treat WHY_RERAISE as WHY_EXCEPTION */
		
handle_reraise:
		why = WHY_EXCEPTION;

unwind_stacks:

		/* Unwind stacks if a (pseudo) exception occurred */
		
		while (why != WHY_NOT && f->f_iblock > 0) {
			PyTryBlock *b = PyFrame_BlockPop(f);
			while (STACK_LEVEL() > b->b_level) {
				v = POP();
				Py_XDECREF(v);
			}
			if (b->b_type == SETUP_LOOP && why == WHY_BREAK) {
				why = WHY_NOT;
				JUMPTO(b->b_handler);
			}
			if (b->b_type == SETUP_FINALLY ||
				(b->b_type == SETUP_EXCEPT &&
				 why == WHY_EXCEPTION)) {
				if (why == WHY_EXCEPTION) {
					PyObject *exc, *val, *tb;
					PyErr_Fetch(&exc, &val, &tb);
					if (val == NULL) {
						val = Py_None;
						Py_INCREF(val);
					}
					/* Make the raw exception data
					   available to the handler,
					   so a program can emulate the
					   Python main loop.  Don't do
					   this for 'finally'. */
					if (b->b_type == SETUP_EXCEPT) {
						PyErr_NormalizeException(
							&exc, &val, &tb);
						set_exc_info(tstate,
								 exc, val, tb);
					}
					PUSH(tb);
					PUSH(val);
					PUSH(exc);
				}
				else {
					if (why == WHY_RETURN)
						PUSH(retval);
					v = PyInt_FromLong((long)why);
					PUSH(v);
				}
				why = WHY_NOT;
				JUMPTO(b->b_handler);
			}
		} /* unwind stack */

		/* End the loop if we still have an error (or return) */
		
		if (why != WHY_NOT)
			break;
		
	} /* main loop */
	
	/* Pop remaining stack entries */
	
	while (!EMPTY()) {
		v = POP();
		Py_XDECREF(v);
	}
	f->f_stackpointer = stack_pointer;	/* CT990621 frame consistency */
	f->f_next_instr = f->f_first_instr;	/* CT991121 be always consistent */
	
	if (why != WHY_RETURN) {
		retval = NULL;
		/* CT991120 make sure that we propagate an exception */
		if (f->f_back)
			f->f_back->f_statusflags |= WANTS_RETURN_VALUE;
	}
	
	if (f->f_trace) {
		if (why == WHY_RETURN) {
			if (call_trace(&f->f_trace, &f->f_trace, f,
				       "return", retval)) {
				Py_XDECREF(retval);
				retval = NULL;
				why = WHY_EXCEPTION;
			}
		}
	}
	
	if (tstate->sys_profilefunc && why == WHY_RETURN) {
		if (call_trace(&tstate->sys_profilefunc, (PyObject**)0,
			       f, "return", retval)) {
			Py_XDECREF(retval);
			retval = NULL;
			why = WHY_EXCEPTION;
		}
	}

	reset_exc_info(tstate);

	--tstate->recursion_depth;

	/* Restore previous frame and release the current one */

	Py_XINCREF(f->f_back); /* CT 990620 */
	tstate->frame = f->f_back;
	Py_DECREF(f);
	
	return retval;
}

/* 
    the frame dispatcher will execute frames and manage
    the frame stack until the initial frame returns.
    The "Mario" code if you know that game :-)
*/

PyObject *
PyEval_Frame_Dispatch(void)
{
    PyObject * result;
    PyFrameObject *f, *fprev;
    PyThreadState *tstate = PyThreadState_GET();
	int irqmask = tstate->uthread_lock;
	tstate->uthread_lock = 1;

    f = tstate->frame;
    if (f==NULL) return NULL;

	fprev = f->f_back;
	/* This frame is on the C stack, so have an extra reference. */
	if (fprev) {
		Py_INCREF(fprev);
		fprev->f_statusflags |= FRAME_IS_LOCKED;
	}
	++tstate->nesting_level;

/* 
    frame protocol:
    If a frame returns the Py_UnwindToken object,
    this indicates that a different frame will be
    run. 
    Semantics of an appearing Py_UnwindToken:
	The true return value is in the f_temp_val
	field.
	We always use the topmost tstate frame and bail
    out when we see the frame that issued the
	dispatcher call (this may be NULL).
*/

    result = f->f_temp_val;
	/* just in case we're re-entering */
	if (result)
		f->f_statusflags |= WANTS_RETURN_VALUE;
    f->f_temp_val = NULL;

    while (1) {

        result = f->f_execute(f, result) ;
        f = tstate->frame;
		if (f==fprev)
			break;
		else if (f==NULL) {
			/* this may happen after introducing uthreads.
			   the frame chain was consumed by recursive
			   interpreters, and now we have to unwind.
			 */
			f = tstate->frame = fprev;
			Py_INCREF(f);
			break;
		}
        else if (result == Py_UnwindToken) {
            /* this is actually the topmost frame. */
            /* pick an optional return value */
            result = f->f_temp_val;
            f->f_temp_val = NULL;
        }
    }
	if (fprev) {
		Py_DECREF(fprev);
		fprev->f_statusflags &= ~FRAME_IS_LOCKED;
	}
	--tstate->nesting_level;
	tstate->uthread_lock = irqmask;
    return result;
}


static void
set_exc_info(PyThreadState *tstate, PyObject *type, PyObject *value, PyObject *tb)
{
	PyFrameObject *frame;
	PyObject *tmp_type, *tmp_value, *tmp_tb;

	frame = tstate->frame;
	if (frame->f_exc_type == NULL) {
		/* This frame didn't catch an exception before */
		/* Save previous exception of this thread in this frame */
		if (tstate->exc_type == NULL) {
			Py_INCREF(Py_None);
			tstate->exc_type = Py_None;
		}
		tmp_type = frame->f_exc_type;
		tmp_value = frame->f_exc_value;
		tmp_tb = frame->f_exc_traceback;
		Py_XINCREF(tstate->exc_type);
		Py_XINCREF(tstate->exc_value);
		Py_XINCREF(tstate->exc_traceback);
		frame->f_exc_type = tstate->exc_type;
		frame->f_exc_value = tstate->exc_value;
		frame->f_exc_traceback = tstate->exc_traceback;
		Py_XDECREF(tmp_type);
		Py_XDECREF(tmp_value);
		Py_XDECREF(tmp_tb);
	}
	/* Set new exception for this thread */
	tmp_type = tstate->exc_type;
	tmp_value = tstate->exc_value;
	tmp_tb = tstate->exc_traceback;
	Py_XINCREF(type);
	Py_XINCREF(value);
	Py_XINCREF(tb);
	tstate->exc_type = type;
	tstate->exc_value = value;
	tstate->exc_traceback = tb;
	Py_XDECREF(tmp_type);
	Py_XDECREF(tmp_value);
	Py_XDECREF(tmp_tb);
	/* For b/w compatibility */
	PySys_SetObject("exc_type", type);
	PySys_SetObject("exc_value", value);
	PySys_SetObject("exc_traceback", tb);
}

static void
reset_exc_info(PyThreadState *tstate)
{
	PyFrameObject *frame;
	PyObject *tmp_type, *tmp_value, *tmp_tb;
	frame = tstate->frame;
	if (frame->f_exc_type != NULL) {
		/* This frame caught an exception */
		tmp_type = tstate->exc_type;
		tmp_value = tstate->exc_value;
		tmp_tb = tstate->exc_traceback;
		Py_XINCREF(frame->f_exc_type);
		Py_XINCREF(frame->f_exc_value);
		Py_XINCREF(frame->f_exc_traceback);
		tstate->exc_type = frame->f_exc_type;
		tstate->exc_value = frame->f_exc_value;
		tstate->exc_traceback = frame->f_exc_traceback;
		Py_XDECREF(tmp_type);
		Py_XDECREF(tmp_value);
		Py_XDECREF(tmp_tb);
		/* For b/w compatibility */
		PySys_SetObject("exc_type", frame->f_exc_type);
		PySys_SetObject("exc_value", frame->f_exc_value);
		PySys_SetObject("exc_traceback", frame->f_exc_traceback);
	}
	tmp_type = frame->f_exc_type;
	tmp_value = frame->f_exc_value;
	tmp_tb = frame->f_exc_traceback;
	frame->f_exc_type = NULL;
	frame->f_exc_value = NULL;
	frame->f_exc_traceback = NULL;
	Py_XDECREF(tmp_type);
	Py_XDECREF(tmp_value);
	Py_XDECREF(tmp_tb);
}

/* Logic for the raise statement (too complicated for inlining).
   This *consumes* a reference count to each of its arguments. */
static enum why_code
do_raise(PyObject *type, PyObject *value, PyObject *tb)
{
	if (type == NULL) {
		/* Reraise */
		PyThreadState *tstate = PyThreadState_Get();
		type = tstate->exc_type == NULL ? Py_None : tstate->exc_type;
		value = tstate->exc_value;
		tb = tstate->exc_traceback;
		Py_XINCREF(type);
		Py_XINCREF(value);
		Py_XINCREF(tb);
	}
		
	/* We support the following forms of raise:
	   raise <class>, <classinstance>
	   raise <class>, <argument tuple>
	   raise <class>, None
	   raise <class>, <argument>
	   raise <classinstance>, None
	   raise <string>, <object>
	   raise <string>, None

	   An omitted second argument is the same as None.

	   In addition, raise <tuple>, <anything> is the same as
	   raising the tuple's first item (and it better have one!);
	   this rule is applied recursively.

	   Finally, an optional third argument can be supplied, which
	   gives the traceback to be substituted (useful when
	   re-raising an exception after examining it).  */

	/* First, check the traceback argument, replacing None with
	   NULL. */
	if (tb == Py_None) {
		Py_DECREF(tb);
		tb = NULL;
	}
	else if (tb != NULL && !PyTraceBack_Check(tb)) {
		PyErr_SetString(PyExc_TypeError,
			   "raise 3rd arg must be traceback or None");
		goto raise_error;
	}

	/* Next, replace a missing value with None */
	if (value == NULL) {
		value = Py_None;
		Py_INCREF(value);
	}

	/* Next, repeatedly, replace a tuple exception with its first item */
	while (PyTuple_Check(type) && PyTuple_Size(type) > 0) {
		PyObject *tmp = type;
		type = PyTuple_GET_ITEM(type, 0);
		Py_INCREF(type);
		Py_DECREF(tmp);
	}

	if (PyString_Check(type))
		;

	else if (PyClass_Check(type))
		PyErr_NormalizeException(&type, &value, &tb);

	else if (PyInstance_Check(type)) {
		/* Raising an instance.  The value should be a dummy. */
		if (value != Py_None) {
			PyErr_SetString(PyExc_TypeError,
			  "instance exception may not have a separate value");
			goto raise_error;
		}
		else {
			/* Normalize to raise <class>, <instance> */
			Py_DECREF(value);
			value = type;
			type = (PyObject*) ((PyInstanceObject*)type)->in_class;
			Py_INCREF(type);
		}
	}
	else {
		/* Not something you can raise.  You get an exception
		   anyway, just not what you specified :-) */
		PyErr_SetString(PyExc_TypeError,
		    "exceptions must be strings, classes, or instances");
		goto raise_error;
	}
	PyErr_Restore(type, value, tb);
	if (tb == NULL)
		return WHY_EXCEPTION;
	else
		return WHY_RERAISE;
 raise_error:
	Py_XDECREF(value);
	Py_XDECREF(type);
	Py_XDECREF(tb);
	return WHY_EXCEPTION;
}

static int
unpack_sequence(PyObject *v, int argcnt, PyObject **sp)
{
	int i;
	PyObject *w;
	
	for (i = 0; i < argcnt; i++) {
		if (! (w = PySequence_GetItem(v, i))) {
			if (PyErr_ExceptionMatches(PyExc_IndexError))
				PyErr_SetString(PyExc_ValueError,
					      "unpack sequence of wrong size");
			goto finally;
		}
		*--sp = w;
	}
	/* we better get an IndexError now */
	if (PySequence_GetItem(v, i) == NULL) {
		if (PyErr_ExceptionMatches(PyExc_IndexError)) {
			PyErr_Clear();
			return 1;
		}
		/* some other exception occurred. fall through to finally */
	}
	else
		PyErr_SetString(PyExc_ValueError,
				"unpack sequence of wrong size");
	/* fall through */
finally:
	for (; i > 0; i--, sp++)
		Py_DECREF(*sp);

	return 0;
}


#ifdef LLTRACE
static int
prtrace(PyObject *v, char *str)
{
	printf("%s ", str);
	if (PyObject_Print(v, stdout, 0) != 0)
		PyErr_Clear(); /* Don't know what else to do */
	printf("\n");
	return 1;
}
#endif

static void
call_exc_trace(PyObject **p_trace, PyObject **p_newtrace, PyFrameObject *f)
{
	PyObject *type, *value, *traceback, *arg;
	int err;
	PyErr_Fetch(&type, &value, &traceback);
	if (value == NULL) {
		value = Py_None;
		Py_INCREF(value);
	}
	arg = Py_BuildValue("(OOO)", type, value, traceback);
	if (arg == NULL) {
		PyErr_Restore(type, value, traceback);
		return;
	}
	err = call_trace(p_trace, p_newtrace, f, "exception", arg);
	Py_DECREF(arg);
	if (err == 0)
		PyErr_Restore(type, value, traceback);
	else {
		Py_XDECREF(type);
		Py_XDECREF(value);
		Py_XDECREF(traceback);
	}
}

/* PyObject **p_trace: in/out; may not be NULL;
 				may not point to NULL variable initially
  PyObject **p_newtrace: in/out; may be NULL;
  				may point to NULL variable;
 				may be same variable as p_newtrace */

static int
call_trace(PyObject **p_trace, PyObject **p_newtrace, PyFrameObject *f,
	   char *msg, PyObject *arg)
{
	PyThreadState *tstate = f->f_tstate;
	PyObject *args, *what;
	PyObject *res = NULL;
	
	if (tstate->tracing) {
		/* Don't do recursive traces */
		if (p_newtrace) {
			Py_XDECREF(*p_newtrace);
			*p_newtrace = NULL;
		}
		return 0;
	}
	
	args = PyTuple_New(3);
	if (args == NULL)
		goto cleanup;
	what = PyString_FromString(msg);
	if (what == NULL)
		goto cleanup;
	Py_INCREF(f);
	PyTuple_SET_ITEM(args, 0, (PyObject *)f);
	PyTuple_SET_ITEM(args, 1, what);
	if (arg == NULL)
		arg = Py_None;
	Py_INCREF(arg);
	PyTuple_SET_ITEM(args, 2, arg);
	tstate->tracing++;
	PyFrame_FastToLocals(f);
	res = PyEval_CallObject(*p_trace, args); /* May clear *p_trace! */
	PyFrame_LocalsToFast(f, 1);
	tstate->tracing--;
 cleanup:
	Py_XDECREF(args);
	if (res == NULL) {
		/* The trace proc raised an exception */
		PyTraceBack_Here(f);
		Py_XDECREF(*p_trace);
		*p_trace = NULL;
		if (p_newtrace) {
			Py_XDECREF(*p_newtrace);
			*p_newtrace = NULL;
		}
		/* to be extra double plus sure we don't get recursive
		 * calls inf either tracefunc or profilefunc gets an
		 * exception, zap the global variables.
		 */
		Py_XDECREF(tstate->sys_tracefunc);
		tstate->sys_tracefunc = NULL;
		Py_XDECREF(tstate->sys_profilefunc);
		tstate->sys_profilefunc = NULL;
		return -1;
	}
	else {
		if (p_newtrace) {
			Py_XDECREF(*p_newtrace);
			if (res == Py_None)
				*p_newtrace = NULL;
			else {
				Py_INCREF(res);
				*p_newtrace = res;
			}
		}
		Py_DECREF(res);
		return 0;
	}
}

PyObject *
PyEval_GetBuiltins(void)
{
	PyThreadState *tstate = PyThreadState_Get();
	PyFrameObject *current_frame = tstate->frame;
	if (current_frame == NULL)
		return tstate->interp->builtins;
	else
		return current_frame->f_builtins;
}

PyObject *
PyEval_GetLocals(void)
{
	PyFrameObject *current_frame = PyThreadState_Get()->frame;
	if (current_frame == NULL)
		return NULL;
	PyFrame_FastToLocals(current_frame);
	return current_frame->f_locals;
}

PyObject *
PyEval_GetGlobals(void)
{
	PyFrameObject *current_frame = PyThreadState_Get()->frame;
	if (current_frame == NULL)
		return NULL;
	else
		return current_frame->f_globals;
}

PyObject *
PyEval_GetFrame(void)
{
	PyFrameObject *current_frame = PyThreadState_Get()->frame;
	return (PyObject *)current_frame;
}

int
PyEval_GetRestricted(void)
{
	PyFrameObject *current_frame = PyThreadState_Get()->frame;
	return current_frame == NULL ? 0 : current_frame->f_restricted;
}

int
Py_FlushLine(void)
{
	PyObject *f = PySys_GetObject("stdout");
	if (f == NULL)
		return 0;
	if (!PyFile_SoftSpace(f, 0))
		return 0;
	return PyFile_WriteString("\n", f);
}


/* External interface to call any callable object.
   The arg must be a tuple or NULL. */

#undef PyEval_CallObject
/* for backward compatibility: export this interface */

PyObject *
PyEval_CallObject(PyObject *func, PyObject *arg)
{
	return PyEval_CallObjectWithKeywords(func, arg, (PyObject *)NULL);
}
#define PyEval_CallObject(func,arg) \
        PyEval_CallObjectWithKeywords(func, arg, (PyObject *)NULL)

#define PyEval_CallObject_nr(func,arg) \
        PyEval_CallObjectWithKeywords_nr(func, arg, (PyObject *)NULL)

PyObject *
PyEval_CallObjectWithKeywords(PyObject *func, PyObject *arg, PyObject *kw)
{
	PyObject *retval = PyEval_CallObjectWithKeywords_nr(func, arg, kw);
	if (retval == Py_UnwindToken) {
		retval = PyEval_Frame_Dispatch();
	}
	return retval;
}

PyObject *
PyEval_CallObjectWithKeywords_nr(PyObject *func, PyObject *arg, PyObject *kw)
{
	ternaryfunc call;
	PyObject *result;

	if (arg == NULL)
		arg = PyTuple_New(0);
	else if (!PyTuple_Check(arg)) {
		PyErr_SetString(PyExc_TypeError,
				"argument list must be a tuple");
		return NULL;
	}
	else
		Py_INCREF(arg);

	if (kw != NULL && !PyDict_Check(kw)) {
		PyErr_SetString(PyExc_TypeError,
				"keyword list must be a dictionary");
		Py_DECREF(arg);
		return NULL;
	}

	if ((call = func->ob_type->tp_call) != NULL)
		result = (*call)(func, arg, kw);
	else if (PyMethod_Check(func) || PyFunction_Check(func))
		result = call_function(func, arg, kw);
	else
		result = call_builtin(func, arg, kw);

	Py_DECREF(arg);
	
	if (result == NULL && !PyErr_Occurred())
		PyErr_SetString(PyExc_SystemError,
			"NULL result without error in call_object");

	return result;
}

static PyObject *
call_builtin(PyObject *func, PyObject *arg, PyObject *kw)
{
	if (PyCFunction_Check(func)) {
		PyCFunction meth = PyCFunction_GetFunction(func);
		PyObject *self = PyCFunction_GetSelf(func);
		int flags = PyCFunction_GetFlags(func);
		if (!(flags & METH_VARARGS)) {
			int size = PyTuple_Size(arg);
			if (size == 1)
				arg = PyTuple_GET_ITEM(arg, 0);
			else if (size == 0)
				arg = NULL;
		}
		if (flags & METH_KEYWORDS)
			return (*(PyCFunctionWithKeywords)meth)(self, arg, kw);
		if (kw != NULL && PyDict_Size(kw) != 0) {
			PyErr_SetString(PyExc_TypeError,
				   "this function takes no keyword arguments");
			return NULL;
		}
		return (*meth)(self, arg);
	}
	if (PyClass_Check(func)) {
		return PyInstance_New(func, arg, kw);
	}
	if (PyInstance_Check(func)) {
	        PyObject *res, *call = PyObject_GetAttrString(func,"__call__");
		if (call == NULL) {
			PyErr_Clear();
			PyErr_SetString(PyExc_AttributeError,
				   "no __call__ method defined");
			return NULL;
		}
		res = PyEval_CallObjectWithKeywords_nr(call, arg, kw);
		Py_DECREF(call);
		return res;
	}
	PyErr_Format(PyExc_TypeError, "call of non-function (type %.400s)",
		     func->ob_type->tp_name);
	return NULL;
}

static PyObject *
call_function(PyObject *func, PyObject *arg, PyObject *kw)
{
	PyObject *class = NULL; /* == owner */
	PyObject *argdefs;
	PyObject **d, **k;
	int nk, nd;
	PyObject *result;
	
	if (kw != NULL && !PyDict_Check(kw)) {
		PyErr_BadInternalCall();
		return NULL;
	}
	
	if (PyMethod_Check(func)) {
		PyObject *self = PyMethod_Self(func);
		class = PyMethod_Class(func);
		func = PyMethod_Function(func);
		if (self == NULL) {
			/* Unbound methods must be called with an instance of
			   the class (or a derived class) as first argument */
			if (PyTuple_Size(arg) >= 1) {
				self = PyTuple_GET_ITEM(arg, 0);
				if (self != NULL &&
				    PyInstance_Check(self) &&
				    PyClass_IsSubclass((PyObject *)
				      (((PyInstanceObject *)self)->in_class),
					       class))
					/* Handy-dandy */ ;
				else
					self = NULL;
			}
			if (self == NULL) {
				PyErr_SetString(PyExc_TypeError,
	   "unbound method must be called with class instance 1st argument");
				return NULL;
			}
			Py_INCREF(arg);
		}
		else {
			int argcount = PyTuple_Size(arg);
			PyObject *newarg = PyTuple_New(argcount + 1);
			int i;
			if (newarg == NULL)
				return NULL;
			Py_INCREF(self);
			PyTuple_SET_ITEM(newarg, 0, self);
			for (i = 0; i < argcount; i++) {
				PyObject *v = PyTuple_GET_ITEM(arg, i);
				Py_XINCREF(v);
				PyTuple_SET_ITEM(newarg, i+1, v);
			}
			arg = newarg;
		}
		if (!PyFunction_Check(func)) {
			result = PyEval_CallObjectWithKeywords_nr(func, arg, kw);
			Py_DECREF(arg);
			return result;
		}
	}
	else {
		if (!PyFunction_Check(func)) {
			PyErr_Format(PyExc_TypeError,
				     "call of non-function (type %s)",
				     func->ob_type->tp_name);
			return NULL;
		}
		Py_INCREF(arg);
	}
	
	argdefs = PyFunction_GetDefaults(func);
	if (argdefs != NULL && PyTuple_Check(argdefs)) {
		d = &PyTuple_GET_ITEM((PyTupleObject *)argdefs, 0);
		nd = PyTuple_Size(argdefs);
	}
	else {
		d = NULL;
		nd = 0;
	}
	
	if (kw != NULL) {
		int pos, i;
		nk = PyDict_Size(kw);
		k = PyMem_NEW(PyObject *, 2*nk);
		if (k == NULL) {
			PyErr_NoMemory();
			Py_DECREF(arg);
			return NULL;
		}
		pos = i = 0;
		while (PyDict_Next(kw, &pos, &k[i], &k[i+1]))
			i += 2;
		nk = i/2;
		/* XXX This is broken if the caller deletes dict items! */
	}
	else {
		k = NULL;
		nk = 0;
	}

	result = eval_code2_setup(
							  (PyCodeObject *)PyFunction_GetCode(func),
							  PyFunction_GetGlobals(func), (PyObject *)NULL,
							  &PyTuple_GET_ITEM(arg, 0), PyTuple_Size(arg),
							  k, nk,
							  d, nd,
							  class);

	Py_DECREF(arg);
	if (k != NULL)
		PyMem_DEL(k);

	if (result != NULL)
		result = Py_UnwindToken;

	return result;
}

#define SLICE_ERROR_MSG \
	"standard sequence type does not support step size other than one"

static PyObject *
loop_subscript(PyObject *v, int i) /* CT 990708 now just an int */
{
	PySequenceMethods *sq = v->ob_type->tp_as_sequence;
	if (sq == NULL || sq->sq_item == NULL) {
		PyErr_SetString(PyExc_TypeError, "loop over non-sequence");
		return NULL;
	}
	v = (*sq->sq_item)(v, i);
	if (v)
		return v;
	if (PyErr_ExceptionMatches(PyExc_IndexError))
		PyErr_Clear();
	return NULL;
}

/* Extract a slice index from a PyInt or PyLong, the index is bound to
   the range [-INT_MAX+1, INTMAX]. Returns 0 and an exception if there is
   and error. Returns 1 on success.*/

int
_PyEval_SliceIndex(PyObject *v, int *pi)
{
	if (v != NULL) {
		long x;
		if (PyInt_Check(v)) {
			x = PyInt_AsLong(v);
		} else if (PyLong_Check(v)) {
			x = PyLong_AsLong(v);
			if (x==-1 && PyErr_Occurred()) {
				PyObject *long_zero;

				if (!PyErr_ExceptionMatches( PyExc_OverflowError ) ) {
					/* It's not an overflow error, so just 
					   signal an error */
					return 0;
				}

				/* It's an overflow error, so we need to 
				   check the sign of the long integer, 
				   set the value to INT_MAX or 0, and clear 
				   the error. */

				/* Create a long integer with a value of 0 */
				long_zero = PyLong_FromLong( 0L );
				if (long_zero == NULL) return 0;

				/* Check sign */
				if (PyObject_Compare(long_zero, v) < 0)
					x = INT_MAX;
				else
					x = 0;
				
				/* Free the long integer we created, and clear the
				   OverflowError */
				Py_DECREF(long_zero);
				PyErr_Clear();
			}
		} else {
			PyErr_SetString(PyExc_TypeError,
					"slice index must be int");
			return 0;
		}
		/* Truncate -- very long indices are truncated anyway */
		if (x > INT_MAX)
			x = INT_MAX;
		else if (x < -INT_MAX)
			x = 0;
		*pi = x;
	}
	return 1;
}

static PyObject *
apply_slice(PyObject *u, PyObject *v, PyObject *w) /* return u[v:w] */
{
	int ilow = 0, ihigh = INT_MAX;
	if (!_PyEval_SliceIndex(v, &ilow))
		return NULL;
	if (!_PyEval_SliceIndex(w, &ihigh))
		return NULL;
	return PySequence_GetSlice(u, ilow, ihigh);
}

static int
assign_slice(PyObject *u, PyObject *v, PyObject *w, PyObject *x) /* u[v:w] = x */
{
	int ilow = 0, ihigh = INT_MAX;
	if (!_PyEval_SliceIndex(v, &ilow))
		return -1;
	if (!_PyEval_SliceIndex(w, &ihigh))
		return -1;
	if (x == NULL)
		return PySequence_DelSlice(u, ilow, ihigh);
	else
		return PySequence_SetSlice(u, ilow, ihigh, x);
}

static PyObject *
cmp_outcome(int op, register PyObject *v, register PyObject *w)
{
	register int cmp;
	register int res = 0;
	switch (op) {
	case IS:
	case IS_NOT:
		res = (v == w);
		if (op == (int) IS_NOT)
			res = !res;
		break;
	case IN:
	case NOT_IN:
		res = PySequence_Contains(w, v);
		if (res < 0)
			return NULL;
		if (op == (int) NOT_IN)
			res = !res;
		break;
	case EXC_MATCH:
		res = PyErr_GivenExceptionMatches(v, w);
		break;
	default:
		cmp = PyObject_Compare(v, w);
		if (cmp && PyErr_Occurred())
			return NULL;
		switch (op) {
		case LT: res = cmp <  0; break;
		case LE: res = cmp <= 0; break;
		case EQ: res = cmp == 0; break;
		case NE: res = cmp != 0; break;
		case GT: res = cmp >  0; break;
		case GE: res = cmp >= 0; break;
		/* XXX no default? (res is initialized to 0 though) */
		}
	}
	v = res ? Py_True : Py_False;
	Py_INCREF(v);
	return v;
}

static PyObject *
import_from(PyObject *v, PyObject *name)
{
	PyObject *w, *x;
	if (!PyModule_Check(v)) {
		PyErr_SetString(PyExc_TypeError,
				"import-from requires module object");
		return NULL;
	}
	w = PyModule_GetDict(v); /* TDB: can this not fail ? */
	x = PyDict_GetItem(w, name);
	if (x == NULL) {
		PyErr_Format(PyExc_ImportError,
			     "cannot import name %.230s",
			     PyString_AsString(name));
	} else
		Py_INCREF(x);
	return x;
}

static int
import_all_from(PyObject *locals, PyObject *v)
{
	int pos = 0, err;
	PyObject *name, *value;
	PyObject *w;

	if (!PyModule_Check(v)) {
		PyErr_SetString(PyExc_TypeError,
				"import-from requires module object");
		return -1;
	}
	w = PyModule_GetDict(v); /* TBD: can this not fail ? */

	while (PyDict_Next(w, &pos, &name, &value)) {
		if (!PyString_Check(name) ||
			PyString_AsString(name)[0] == '_')
				continue;
		Py_INCREF(value);
		err = PyDict_SetItem(locals, name, value);
		Py_DECREF(value);
		if (err != 0)
			return -1;
	}
	return 0;
}

static PyObject *
build_class(PyObject *methods, PyObject *bases, PyObject *name)
{
	int i, n;
	if (!PyTuple_Check(bases)) {
		PyErr_SetString(PyExc_SystemError,
				"build_class with non-tuple bases");
		return NULL;
	}
	if (!PyDict_Check(methods)) {
		PyErr_SetString(PyExc_SystemError,
				"build_class with non-dictionary");
		return NULL;
	}
	if (!PyString_Check(name)) {
		PyErr_SetString(PyExc_SystemError,
				"build_class with non-string name");
		return NULL;
	}
	n = PyTuple_Size(bases);
	for (i = 0; i < n; i++) {
		PyObject *base = PyTuple_GET_ITEM(bases, i);
		if (!PyClass_Check(base)) {
			/* Call the base's *type*, if it is callable.
			   This code is a hook for Donald Beaudry's
			   and Jim Fulton's type extensions.  In
			   unextended Python it will never be triggered
			   since its types are not callable.
			   Ditto: call the bases's *class*, if it has
			   one.  This makes the same thing possible
			   without writing C code.  A true meta-object
			   protocol! */
			PyObject *basetype = (PyObject *)base->ob_type;
			PyObject *callable = NULL;
			if (PyCallable_Check(basetype))
				callable = basetype;
			else
				callable = PyObject_GetAttrString(
					base, "__class__");
			if (callable) {
				PyObject *args;
				PyObject *newclass = NULL;
				args = Py_BuildValue(
					"(OOO)", name, bases, methods);
				if (args != NULL) {
					newclass = PyEval_CallObject(
						callable, args);
					Py_DECREF(args);
				}
				if (callable != basetype) {
					Py_DECREF(callable);
				}
				return newclass;
			}
			PyErr_SetString(PyExc_TypeError,
				"base is not a class object");
			return NULL;
		}
	}
	return PyClass_New(bases, methods, name);
}

/* 
turning exec into a stackless version is a little harder,
since it does not use the object protocol.
We use the value "-42" as a Frame object marker instead.
*/

static int
exec_statement(PyFrameObject *f, PyObject *prog, PyObject *globals,
			   PyObject *locals)
{
	int n;
	PyObject *v;
	int plain = 0;

	if (PyTuple_Check(prog) && globals == Py_None && locals == Py_None &&
	    ((n = PyTuple_Size(prog)) == 2 || n == 3)) {
		/* Backward compatibility hack */
		globals = PyTuple_GetItem(prog, 1);
		if (n == 3)
			locals = PyTuple_GetItem(prog, 2);
		prog = PyTuple_GetItem(prog, 0);
	}
	if (globals == Py_None) {
		globals = PyEval_GetGlobals();
		if (locals == Py_None) {
			locals = PyEval_GetLocals();
			plain = 1;
		}
	}
	else if (locals == Py_None)
		locals = globals;
	if (!PyString_Check(prog) &&
		!PyUnicode_Check(prog) &&
	    !PyCode_Check(prog) &&
	    !PyFile_Check(prog)) {
		PyErr_SetString(PyExc_TypeError,
			   "exec 1st arg must be string, code or file object");
		return -1;
	}
	if (!PyDict_Check(globals) || !PyDict_Check(locals)) {
		PyErr_SetString(PyExc_TypeError,
		    "exec 2nd/3rd args must be dict or None");
		return -1;
	}
	if (PyDict_GetItemString(globals, "__builtins__") == NULL)
		PyDict_SetItemString(globals, "__builtins__", f->f_builtins);
	if (PyCode_Check(prog)) {
		v = PyEval_EvalCode_nr((PyCodeObject *) prog, globals, locals);
	}
	else if (PyFile_Check(prog)) {
		FILE *fp = PyFile_AsFile(prog);
		char *name = PyString_AsString(PyFile_Name(prog));
		v = PyRun_File_nr(fp, name, Py_file_input, globals, locals);
	}
	else {
		char *str;
		if (PyString_AsStringAndSize(prog, &str, NULL))
			return -1;
		v = PyRun_String_nr(str, Py_file_input, globals, locals);
	}
	if (plain) {
		if (v != Py_UnwindToken)
			PyFrame_LocalsToFast(f, 0);
		else
			f->f_statusflags |= LOCALS_NEED_UPDATE;
	}
	if (v == NULL)
		return -1;
	if (v == Py_UnwindToken)
		return -42;
	Py_DECREF(v);
	return 0;
}

static void 
format_exc_check_arg(PyObject *exc, char *format_str, PyObject *obj)
{
	char *obj_str;

	if (!obj)
		return;

	obj_str = PyString_AsString(obj);
	if (!obj_str)
		return;

	PyErr_Format(exc, format_str, obj_str);
}

#ifdef DYNAMIC_EXECUTION_PROFILE

PyObject *
getarray(long a[256])
{
	int i;
	PyObject *l = PyList_New(256);
	if (l == NULL) return NULL;
	for (i = 0; i < 256; i++) {
		PyObject *x = PyInt_FromLong(a[i]);
		if (x == NULL) {
			Py_DECREF(l);
			return NULL;
		}
		PyList_SetItem(l, i, x);
	}
	for (i = 0; i < 256; i++)
		a[i] = 0;
	return l;
}

PyObject *
_Py_GetDXProfile(PyObject *self, PyObject *args)
{
#ifndef DXPAIRS
	return getarray(dxp);
#else
	int i;
	PyObject *l = PyList_New(257);
	if (l == NULL) return NULL;
	for (i = 0; i < 257; i++) {
		PyObject *x = getarray(dxpairs[i]);
		if (x == NULL) {
			Py_DECREF(l);
			return NULL;
		}
		PyList_SetItem(l, i, x);
	}
	return l;
#endif
}

#endif


/* loop index support added at the end (this source is long enough already) */

static PyLoopIndexObject *loopindex_free_list = NULL;

static PyFrame_FiniChainFunc * other_PyFrame_FiniChain = NULL;

void
PyLoopIndex_Fini(void)
{
	PyLoopIndexObject *li = loopindex_free_list;

	while (li != NULL) {
	    PyLoopIndexObject *v = li;
	    li = *(PyLoopIndexObject **)li;
	    free(v);
	}
	/* allow PyFrame_Fini to continue with another one */
	PyFrame_FiniChain = other_PyFrame_FiniChain;
}


PyObject *
PyLoopIndex_FromLong(long ival)
{
	register PyLoopIndexObject *v;
	if (loopindex_free_list != NULL) {
		v = loopindex_free_list;
		loopindex_free_list = *(PyLoopIndexObject **)loopindex_free_list;
	}
	else {
		v = (PyLoopIndexObject *)PyObject_Malloc(sizeof(PyLoopIndexObject));
		if (!v)
			return NULL;
		/* initialize Fini */
		{
			static int initdone = 0;
			if (!initdone) {
				other_PyFrame_FiniChain = PyFrame_FiniChain;
				PyFrame_FiniChain = PyLoopIndex_Fini;
				initdone = 1;
			}
		}
	}
	v->ob_type = &PyLoopIndex_Type;
	v->ob_ival = ival;
	_Py_NewReference((PyObject*)v);
	return (PyObject *) v;
}


static void
loopindex_dealloc(PyLoopIndexObject *v)
{
	*(PyLoopIndexObject **)v = loopindex_free_list;
	loopindex_free_list = v;
}


static PyObject *
loopindex_repr(PyLoopIndexObject *v)
{
	char buf[24];
	sprintf(buf, "(%ld++)", v->ob_ival);
	return PyString_FromString(buf);
}



PyTypeObject PyLoopIndex_Type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,
	"loopindex",
	sizeof(PyLoopIndexObject),
	0,
	(destructor)loopindex_dealloc, 	/*tp_dealloc*/
	0, 								/*tp_print*/
	0,								/*tp_getattr*/
	0,								/*tp_setattr*/
	0,						 		/*tp_compare*/
	(reprfunc)loopindex_repr,		/*tp_repr*/
	0,								/*tp_as_number*/
	0,								/*tp_as_sequence*/
	0,								/*tp_as_mapping*/
	0,								/*tp_hash*/
	0	/* sentinel */
};


