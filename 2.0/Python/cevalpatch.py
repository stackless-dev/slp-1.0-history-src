# ceval.c patcher V0.0.1
# pirx 990111-990112
# adjusted for SLPY 20000101

import string, os

# Guido, adjust these, please

DEVEL_PREFIX=r'D:\stackless\src\2.0'
INPUT_PATH=os.path.join(DEVEL_PREFIX,r'Python\ceval_pre.c')
INPUT_OPATH=os.path.join(DEVEL_PREFIX,r'Python-2.0\include\opcode.h')
OUTPUT_PATH=os.path.join(DEVEL_PREFIX,r'Python\ceval.c')


source = open(INPUT_PATH).read()

class rewriter:

    def __init__(self):
        self.replace_once = []   # once in the source
        self.replace_case = []   # in all case switches
        self.have_argument, self.opcodes = getopcodes()
        self.opnames = {}
        for code, name in self.opcodes.items():
            self.opnames[name] = code
        self.split_short_long = 0
        self.record_extended = {}

    def run(self, inname, outname):
        self.in_lines = open(inname, "r").readlines()
        self.pre_lines = []
        self.short_lines = []
        self.long_lines = []
        self.post_lines = []
        self.extended = []
        self.target = self.pre_lines
        self.dropnext=0
        self.context = 0
        self.thiscode = 0
        self.lastcode = 0
        self.indent = 0
        self.pending = ""
        self.re_encode()
        self.out_lines = self.pre_lines + self.short_lines + self.long_lines + self.post_lines
        open(outname, "w").writelines(self.out_lines)

    def extend(self, names):
        for name in string.split(names):
            self.record_extended[name] = name

    def get_extended(self):
        return string.join(self.extended, "")

    def matchon(self, text):
        current = self.current
        actline = string.strip(current)
        if actline[:len(text)]==text:
            return 1
            
    def set_context(self):
        if self.matchon("switch (opcode) {"):
            self.context = 1
            if self.split_short_long:
                self.target = self.short_lines
        elif self.matchon("} /* switch */"):
            self.context = 0
            if self.split_short_long:
                self.target = self.post_lines

    def set_target(self, context):
        if self.split_short_long:
            self.target = (self.short_lines, self.long_lines) [
                self.opnames[context] >= self.have_argument ]
    
    def adjust_case(self):
        if not self.context: 
            return
        line = self.current
        if self.matchon("case "):
            parts = string.split(line)
            if len(parts)==2:
                code = parts[1][:-1]
                if code[0] in string.uppercase:
                    self.lastcode = self.thiscode = code
                    self.set_target(code)
                    self.app(line)
                    if self.record_extended.has_key(self.thiscode):
                        self.extended.append("\t"+line)
                    return 1
        elif self.split_short_long:
            return # no special prefetching
        elif self.lastcode:
            p = string.index(line, string.strip(line))
            if not self.indent:
               self.indent = line[:p]
            self.app("%sPREPARE(%s);\n" % (self.indent, self.lastcode))
            self.lastcode = 0
        if self.thiscode:
            oldline = line
            if self.record_extended.has_key(self.thiscode):
                self.extended.append("\t"+line)
            for pattern, substitute in self.replace_case:
                line = string.replace(line, pattern, substitute)
            if line <> oldline:
                line = line % self.__dict__
                self.app(line)
                return 1
    
    def app(self, block):
        self.target.append(block)

    def comm(self, docomm=1):
        txt = self.current
        if docomm:
            txt = string.rstrip(txt)
            if txt[0] =="\t": txt = "/*  %s */\n" % txt[1:]
            elif txt[:3]=="   ": txt = "/* %s */\n" % txt[3:]
            else: txt = "/* %s */\n" % txt
        self.app(txt)

    def re_encode(self):
        for line in self.in_lines:
            self.current = line
            if self.dropnext:
                self.dropnext=self.dropnext-1
                self.comm()
                continue
            if self.pending:
                self.app(self.pending)
                self.pending = ""
            self.set_context()
            if self.adjust_case():
                continue
            else:
                self.pending=line
                if self.replace_once:
                    pattern, substitute, remove = self.replace_once[0]
                    if self.matchon(pattern):
                        if callable(substitute):
                            substitute = substitute(self)
                        self.pending = substitute
                        self.comm(remove)
                        self.dropnext = max(remove-1, 0)
                        del self.replace_once[0]
        if self.pending:
            self.app(self.pending)
            
    def once(self, pattern, substitute, remove=1):
        """configure a replacement"""
        self.replace_once.append( (pattern, substitute, remove) )

    def allcase(self, pattern, substitute):
        """text replacements for all cases"""
        self.replace_case.append( (pattern, substitute) )

def getopcodes():
    res = {}
    last = 0
    special = "HAVE_ARGUMENT"
    for line in open(INPUT_OPATH).readlines():
        if string.find(line, "uses") >= 0:
            parts = string.split(line)
            for word in parts:
                try:
                    pre, post = string.split(word, "-")
                    pre, post = map(int, (pre, post))
                    for i in range(pre-1, post+1):
                        res[i] = "%s+%i" % (name, i-code)
                    break
                except:
                    continue
            continue
        if string.find(line, "/*") >= 0:
            line = line[:string.find(line, "/*")]
        words = string.split(line)
        if len(words) <> 3: continue
        define, name, code = words
        if define <> "#define": continue
        code = int(code)
        if name != special:
            res[code] = name
        else:
            have_argument = code;
    return have_argument, res

def build_undef(short_long=0):
    have_arg, codes = getopcodes()
    del codes[0] # defined but unused
    unused = {}
    for code in range(256):
        if not codes.has_key(code):
            unused[code] = code
    if short_long == 1:
        for i in range(have_arg, 256):
            unused[i] = i
    if short_long == 2:
        for i in range(0, have_arg):
            unused[i] = i
    unused = unused.keys()
    unused.sort()
    blocklen = 10
    res = []
    for i in range(0, len(unused), blocklen):
        part = unused[i:i+blocklen]
        s = ""
        for code in part:
            s = s + "case %3d:;" % code
        s = "\t\t%s\n" % string.strip(s)
        res.append(s)
    return string.join(res, "")

coder = rewriter()

OPTIMIZE_PREFETCH, GROUP_OPCODES = range(2)

mode = OPTIMIZE_PREFETCH

if mode == OPTIMIZE_PREFETCH:
    coder.once ( "#define NEXTOP","""\
//CT BEGIN
#define NEXTOP() (*next_instr) /* CT no increment but in op */
//CT END
""")
    coder.once ( "#define NEXTARG","""\
//CT BEGIN
#define NEXTARG()	(next_instr += 3, (((unsigned short*)(&next_instr[-2]))[0]))
#define THISARG()	((next_instr[-1]<<8) + next_instr[-2])
#define PREPARE(op) (HAS_ARG(op) ? (oparg=NEXTARG()) : (next_instr+=1, 0))
//CT END
""")
    coder.once ( "if (HAS_ARG(opcode))","""\
//CT BEGIN
#ifdef LLTRACE
		if (HAS_ARG(opcode))
			oparg = THISARG();
#endif
//CT END
""",        2 ) # replace 2 lines

    coder.once ( "dispatch_opcode:","""\
\t/* handled locally in extended_arg */
""")
    
    coder.once("goto dispatch_opcode", (lambda self, txt="""\

\t\t\tswitch (opcode) {

%s\t\t\tdefault:
\t\t\t\tPyErr_Format(PyExc_SystemError, "opcode %%d not extended", opcode);
\t\t\t\tHANDLE_EXCEPTION;
\t\t\t}
""": txt % self.get_extended()),        2 )
    
    coder.once ( "/* unused opcodes go here */", """\
#ifdef WIN32
\t\tdefault: __assume(0);
#endif
""" + build_undef(), 0)
    coder.once ("default:", "")

    coder.once ("f->f_lasti = INSTR_OFFSET","""\
\t\tf->f_lasti = -INSTR_OFFSET(); /* adjust later */
""", 3)
    
    coder.extend("SET_LINENO")
    coder.extend("SETUP_LOOP SETUP_EXCEPT SETUP_FINALLY")
    coder.extend("JUMP_FORWARD JUMP_IF_FALSE JUMP_IF_TRUE JUMP_ABSOLUTE")    
    coder.extend("BUILD_TUPLE BUILD_LIST")
    coder.extend("FOR_LOOP")

if mode == GROUP_OPCODES:
    coder.split_short_long = 1
               
if __name__ == "__main__":
    coder.run(INPUT_PATH, OUTPUT_PATH)
            