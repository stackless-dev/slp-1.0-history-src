# Microsoft Developer Studio Project File - Name="slpy20" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102
# TARGTYPE "Win32 (ALPHA) Dynamic-Link Library" 0x0602

CFG=slpy20 - Win32 Alpha Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "slpy20.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "slpy20.mak" CFG="slpy20 - Win32 Alpha Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "slpy20 - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "slpy20 - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "slpy20 - Win32 Alpha Debug" (based on "Win32 (ALPHA) Dynamic-Link Library")
!MESSAGE "slpy20 - Win32 Alpha Release" (based on "Win32 (ALPHA) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "slpy20"
# PROP Scc_LocalPath ".."

!IF  "$(CFG)" == "slpy20 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "."
# PROP Intermediate_Dir "x86-temp-release\slpy20"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
F90=df.exe
CPP=cl.exe
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /Zi /O2 /I "Include" /I "Python-2.0.1\Include" /I "Python-2.0.1\PC" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "USE_DL_EXPORT" /D "Py_INT_IGNORE" /YX /FD /Zm200 /c
# SUBTRACT CPP /Fr
MTL=midl.exe
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
RSC=rc.exe
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /i "Python-2.0.1\Include" /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 largeint.lib kernel32.lib user32.lib advapi32.lib shell32.lib /nologo /base:"0x1e100000" /subsystem:windows /dll /map /debug /machine:I386 /nodefaultlib:"libc" /out:"./python20.dll"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "."
# PROP Intermediate_Dir "x86-temp-debug\slpy20"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
F90=df.exe
CPP=cl.exe
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /I "Include" /I "Python-2.0.1\Include" /I "Python-2.0.1\PC" /D "_DEBUG" /D "USE_DL_EXPORT" /D "WIN32" /D "_WINDOWS" /YX /FD /Zm200 /c
MTL=midl.exe
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
RSC=rc.exe
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /i "Python-2.0.1\Include" /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 largeint.lib kernel32.lib user32.lib advapi32.lib shell32.lib /nologo /base:"0x1e100000" /subsystem:windows /dll /debug /machine:I386 /nodefaultlib:"libc" /out:"./python20_d.dll" /pdbtype:sept
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "."
# PROP Intermediate_Dir "alpha-temp-debug\slpy20"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
CPP=cl.exe
# ADD BASE CPP /nologo /Gt0 /W3 /GX /Zi /Od /I "Python-2.0.1\Include" /I "Python-2.0.1\PC" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "USE_DL_EXPORT" /YX /FD /MTd /c
# ADD CPP /nologo /Gt0 /W3 /GX /Zi /Od /I "Python-2.0.1\Include" /I "Python-2.0.1\PC" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "USE_DL_EXPORT" /YX /FD /MDd /c
MTL=midl.exe
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
RSC=rc.exe
# ADD BASE RSC /l 0x409 /i "Python-2.0.1\Include" /d "_DEBUG"
# ADD RSC /l 0x409 /i "Python-2.0.1\Include" /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 wsock32.lib largeint.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib /nologo /base:"0x1e100000" /subsystem:windows /dll /debug /machine:ALPHA /nodefaultlib:"libc" /out:"./slpy20_d.dll" /pdbtype:sept
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 largeint.lib kernel32.lib user32.lib advapi32.lib /nologo /base:"0x1e100000" /subsystem:windows /dll /debug /machine:ALPHA /nodefaultlib:"libc" /out:"./slpy20_d.dll" /pdbtype:sept
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "."
# PROP Intermediate_Dir "alpha-temp-release\slpy20"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
CPP=cl.exe
# ADD BASE CPP /nologo /MT /Gt0 /W3 /GX /Zi /O2 /I "Python-2.0.1\Include" /I "Python-2.0.1\PC" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "USE_DL_EXPORT" /YX /FD /c
# ADD CPP /nologo /MD /Gt0 /W3 /GX /O2 /I "Python-2.0.1\Include" /I "Python-2.0.1\PC" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "USE_DL_EXPORT" /YX /FD /c
MTL=midl.exe
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
RSC=rc.exe
# ADD BASE RSC /l 0x409 /i "Python-2.0.1\Include" /d "NDEBUG"
# ADD RSC /l 0x409 /i "Python-2.0.1\Include" /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 wsock32.lib largeint.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib /nologo /base:"0x1e100000" /subsystem:windows /dll /debug /machine:ALPHA /nodefaultlib:"libc"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 largeint.lib kernel32.lib user32.lib advapi32.lib /nologo /base:"0x1e100000" /subsystem:windows /dll /debug /machine:ALPHA /nodefaultlib:"libc" /out:"python20.dll"
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "slpy20 - Win32 Release"
# Name "slpy20 - Win32 Debug"
# Name "slpy20 - Win32 Alpha Debug"
# Name "slpy20 - Win32 Alpha Release"
# Begin Source File

SOURCE="Python-2.0.1\Modules\_codecsmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\_localemodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\abstract.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Parser\acceler.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\arraymodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\audioop.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\binascii.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Parser\bitset.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Python\bltinmodule.c

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\bufferobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Python\ceval.c

!IF  "$(CFG)" == "slpy20 - Win32 Release"

# ADD CPP /G6 /W3 /O2 /FAs

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Python\ceval_pre.c

!IF  "$(CFG)" == "slpy20 - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\common\changelog.txt
# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\classobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\cmathmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\cobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\codecs.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\compile.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\complexobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\PC\config.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\cPickle.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\cStringIO.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\dictobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\PC\dl_nt.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\dynload_win.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\errnomodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\errors.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\exceptions.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\fileobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\floatobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\fpectlmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\fpetestmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Objects\frameobject.c

!IF  "$(CFG)" == "slpy20 - Win32 Release"

# ADD CPP /O2

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\frozen.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\funcobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\gcmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\getargs.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\getbuildinfo.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

# ADD CPP /D BUILD=8

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

# ADD CPP /D BUILD=8

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\getcompiler.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\getcopyright.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\getmtime.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\getopt.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\PC\getpathp.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\getplatform.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\getversion.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\graminit.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Parser\grammar1.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\imageop.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\import.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\PC\import_nt.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

# ADD CPP /I "Python-2.0.1\Python"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

# ADD CPP /I "Python-2.0.1\Python"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\importdl.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\intobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Parser\listnode.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\listobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\longobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\main.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\marshal.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\mathmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\md5c.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\md5module.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Parser\metagrammar.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\methodobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\modsupport.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\moduleobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\PC\msvcrtmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Parser\myreadline.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\mystrtoul.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\newmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Parser\node.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\object.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\operator.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Parser\parser.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Parser\parsetok.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\pcremodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\posixmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\pyfpe.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\pypcre.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Python\pystate.c

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\PC\python_nt.rc"
# End Source File
# Begin Source File

SOURCE=.\Python\pythonrun.c

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\rangeobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\regexmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\regexpr.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\rgbimgmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\rotormodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\shamodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\signalmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\sliceobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\soundex.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\stringobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\stropmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\structmember.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\structmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\sysmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Python\thread.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\threadmodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\timemodule.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Parser\tokenizer.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Python\traceback.c

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\tupleobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\typeobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\unicodectype.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Objects\unicodeobject.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE="Python-2.0.1\Modules\yuvconvert.c"

!IF  "$(CFG)" == "slpy20 - Win32 Release"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Debug"

!ELSEIF  "$(CFG)" == "slpy20 - Win32 Alpha Release"

!ENDIF 

# End Source File
# End Target
# End Project
