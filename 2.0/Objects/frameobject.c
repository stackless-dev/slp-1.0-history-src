
/* Frame object implementation */

#include "Python.h"

#include "compile.h"
#include "frameobject.h"
#include "opcode.h"
#include "structmember.h"

#define OFF(x) offsetof(PyFrameObject, x)

static struct memberlist frame_memberlist[] = {
	{"f_back",          T_OBJECT,	OFF(f_back),		RO},
	{"f_code",          T_OBJECT,	OFF(f_code),		RO},
	{"f_builtins",      T_OBJECT,	OFF(f_builtins),	RO},
	{"f_globals",       T_OBJECT,	OFF(f_globals),		RO},
	{"f_locals",        T_OBJECT,	OFF(f_locals),		RO},
	{"f_lasti",         T_INT,		OFF(f_lasti),		RO},
	{"f_lineno",        T_INT,		OFF(f_lineno),		RO},
	{"f_restricted",    T_INT,		OFF(f_restricted),	RO},
	{"f_trace",         T_OBJECT,	OFF(f_trace)},
	{"f_exc_type",      T_OBJECT,	OFF(f_exc_type)},
	{"f_exc_value",     T_OBJECT,	OFF(f_exc_value)},
	{"f_exc_traceback", T_OBJECT,	OFF(f_exc_traceback)},

	/* some more debugging aid for stackless Python */
	{"f_valuestack",    T_INT,		OFF(f_valuestack),	RO},
	{"f_tstate",        T_INT,		OFF(f_tstate),		RO},
	{"f_iblock",        T_INT,		OFF(f_iblock),		RO},
	/* f_blockstack not exposed */
	{"f_nlocals",       T_INT,		OFF(f_nlocals),		RO},
	{"f_stacksize",     T_INT,		OFF(f_stacksize),	RO},

	/* stackless Python specific fields */
	{"f_referers",		T_INT,		OFF(f_referers),	RO},
	{"f_first_instr",   T_INT,		OFF(f_first_instr),	RO},
	{"f_next_instr",    T_INT,		OFF(f_next_instr),	RO},
	{"f_stackpointer",  T_INT,		OFF(f_stackpointer),RO},
	{"f_statusflags",   T_INT,		OFF(f_statusflags),	RO},
	{"f_hold_ref",      T_OBJECT,	OFF(f_hold_ref),	RO},
	/* f_temp_val is not an object since it's transient */
	{"f_temp_val",      T_INT,		OFF(f_temp_val),	RO},
	{"f_execute",       T_INT,		OFF(f_execute),		RO},
	{"f_callguard",     T_INT,		OFF(f_callguard),	RO},
	/* other interpreters like map or throw_continuation */
	{"f_memory",        T_INT,		OFF(f_memory),		RO},
	{"f_node",          T_INT,		OFF(f_node),		RO},
	{"f_co",            T_INT,		OFF(f_co),			RO},
	{"f_coframes",      T_INT,		OFF(f_coframes),	RO},
	{"f_depth",         T_INT,		OFF(f_depth),		RO},
	{"f_age",			T_INT,		OFF(f_age),			RO},
	{"f_reg3",          T_INT,		OFF(f_reg3),		RO},
	/* end of stackless Python specifics */

	{"f_localsplus",   T_INT,		OFF(f_localsplus),	RO},
	{NULL}	/* Sentinel */
};

static PyObject *
frame_getattr(PyFrameObject *f, char *name)
{
	if (strcmp(name, "f_locals") == 0)
		PyFrame_FastToLocals(f);
	else if (strcmp(name, "__members__") == 0) {
		PyObject *ret, *lis, *other;
		lis = PyMember_Get((char *)f, frame_memberlist, name);
		other = Py_BuildValue("[s]", "locked");
		if(lis)
			ret = PySequence_Concat(lis, other);
		Py_XDECREF(lis);
		Py_XDECREF(other);
		return ret;
	}
	else if (strcmp(name, "locked") == 0) {
		int locked = f->f_statusflags & FRAME_IS_LOCKED ? 1 : 0;
		return PyInt_FromLong(locked);
	}
	else if (strcmp(name, "f_lasti") == 0 && f->f_lasti < 0) {
		f->f_lasti = PyFrame_PreviousInstr(f, f->f_lasti);
	}
	return PyMember_Get((char *)f, frame_memberlist, name);
}

static int
frame_setattr(PyFrameObject *f, char *name, PyObject *value)
{
	return PyMember_Set((char *)f, frame_memberlist, name, value);
}

/* Stack frames are allocated and deallocated at a considerable rate.
   In an attempt to improve the speed of function calls, we maintain a
   separate free list of stack frames (just like integers are
   allocated in a special way -- see intobject.c).  When a stack frame
   is on the free list, only the following members have a meaning:
	ob_type		== &Frametype
	f_back		next item on free list, or NULL
	f_nlocals	number of locals
	f_stacksize	size of value stack
   Note that the value and block stacks are preserved -- this can save
   another malloc() call or two (and two free() calls as well!).
   Also note that, unlike for integers, each frame object is a
   malloc'ed object in its own right -- it is only the actual calls to
   malloc() that we are trying to save here, not the administration.
   After all, while a typical program may make millions of calls, a
   call depth of more than 20 or 30 is probably already exceptional
   unless the program contains run-away recursion.  I hope.
*/

static PyFrameObject *free_list = NULL;

static void
frame_dealloc(PyFrameObject *f)
{
	int i;
	PyObject **fastlocals;
	PyFrameObject *back;
	PyObject **stack_pointer;

	Py_TRASHCAN_SAFE_BEGIN(f)
	if (f->f_dealloc && f->f_dealloc(f, &free_list))
		return;

	/* Kill all local variables */
	fastlocals = f->f_localsplus;
	for (i = f->f_nlocals; --i >= 0; ++fastlocals) {
		Py_XDECREF(*fastlocals);
	}

	/* Kill also stack entries, if this frame was not left by return */
	stack_pointer = f->f_stackpointer;
	while (--stack_pointer >= f->f_valuestack)
		Py_DECREF(*stack_pointer);

	back = f->f_back;
	Py_XDECREF(f->f_code);
	Py_XDECREF(f->f_builtins);
	Py_XDECREF(f->f_globals);
	Py_XDECREF(f->f_locals);
	Py_XDECREF(f->f_trace);
	Py_XDECREF(f->f_exc_type);
	Py_XDECREF(f->f_exc_value);
	Py_XDECREF(f->f_exc_traceback);
	Py_XDECREF(f->f_hold_ref);	/* stackless support */
	/* f_temp_val not touched */
	if (f->f_memory != NULL)
		PyMem_DEL(f->f_memory);
	f->f_back = free_list;
	free_list = f;
	if (back) {
		--back->f_referers;
		Py_XDECREF(back);
	}
	Py_TRASHCAN_SAFE_END(f)
}


PyTypeObject PyFrame_Type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,
	"frame",
	sizeof(PyFrameObject),
	0,
	(destructor)frame_dealloc, /*tp_dealloc*/
	0,		/*tp_print*/
	(getattrfunc)frame_getattr, /*tp_getattr*/
	(setattrfunc)frame_setattr, /*tp_setattr*/
	0,		/*tp_compare*/
	0,		/*tp_repr*/
	0,		/*tp_as_number*/
	0,		/*tp_as_sequence*/
	0,		/*tp_as_mapping*/
};

PyFrameObject *
PyFrame_New(PyThreadState *tstate, PyCodeObject *code,
			PyObject *globals, PyObject *locals)
{
	PyFrameObject *back = tstate->frame;
	static PyObject *builtin_object;
	PyFrameObject *f;
	PyObject *builtins;
	int extras;

	if (builtin_object == NULL) {
		builtin_object = PyString_InternFromString("__builtins__");
		if (builtin_object == NULL)
			return NULL;
	}
	if ((back != NULL && !PyFrame_Check(back)) ||
	    code == NULL || !PyCode_Check(code) ||
	    globals == NULL || !PyDict_Check(globals) ||
	    (locals != NULL && !PyDict_Check(locals))) {
		PyErr_BadInternalCall();
		return NULL;
	}
	extras = code->co_stacksize + code->co_nlocals;
	if (back == NULL || back->f_globals != globals) {
		builtins = PyDict_GetItem(globals, builtin_object);
		if (builtins != NULL && PyModule_Check(builtins))
			builtins = PyModule_GetDict(builtins);
	}
	else {
		/* If we share the globals, we share the builtins.
		   Save a lookup and a call. */
		builtins = back->f_builtins;
	}
	if (builtins != NULL && !PyDict_Check(builtins))
		builtins = NULL;
	if (free_list == NULL) {
		/* PyObject_New is inlined */
		f = (PyFrameObject *)
			PyObject_MALLOC(sizeof(PyFrameObject) +
			       extras*sizeof(PyObject *));
		if (f == NULL)
			return (PyFrameObject *)PyErr_NoMemory();
		PyObject_INIT(f, &PyFrame_Type);
	}
	else {
		f = free_list;
		free_list = free_list->f_back;
		if (f->f_nlocals + f->f_stacksize < extras) {
			f = (PyFrameObject *)
				PyObject_REALLOC(f, sizeof(PyFrameObject) +
					extras*sizeof(PyObject *));
			if (f == NULL)
				return (PyFrameObject *)PyErr_NoMemory();
		}
		else
			extras = f->f_nlocals + f->f_stacksize;
		PyObject_INIT(f, &PyFrame_Type);
	}
	if (builtins == NULL) {
		/* No builtins!  Make up a minimal one. */
		builtins = PyDict_New();
		if (builtins == NULL || /* Give them 'None', at least. */
		    PyDict_SetItemString(builtins, "None", Py_None) < 0) {
			Py_DECREF(f);
			return NULL;
		}
	}
	else
		Py_XINCREF(builtins);
	f->f_builtins = builtins;
/* CT 990620 this doesn't happen any longer.
   It must be done instead before the current frame is released
	Py_XINCREF(back);
*/
	/* y2k0410 extra referer field */
	f->f_referers = 0;
	f->f_back = back;
	if(back)
		++back->f_referers;
	Py_INCREF(code);
	f->f_code = code;
	Py_INCREF(globals);
	f->f_globals = globals;
	if (code->co_flags & CO_NEWLOCALS) {
		if (code->co_flags & CO_OPTIMIZED)
			locals = NULL; /* Let fast_2_locals handle it */
		else {
			locals = PyDict_New();
			if (locals == NULL) {
				Py_XINCREF(f->f_back); /* CT 990621 */
				Py_DECREF(f);
				return NULL;
			}
		}
	}
	else {
		if (locals == NULL)
			locals = globals;
		Py_INCREF(locals);
	}
	f->f_locals = locals;
	f->f_trace = NULL;
	f->f_exc_type = f->f_exc_value = f->f_exc_traceback = NULL;
	f->f_tstate = tstate;

	f->f_lasti = 0;
	f->f_lineno = code->co_firstlineno;
	f->f_restricted = (builtins != tstate->interp->builtins);
	f->f_iblock = 0;
	f->f_nlocals = code->co_nlocals;
	f->f_stacksize = extras - code->co_nlocals;

	while (--extras >= 0)
		f->f_localsplus[extras] = NULL;

	f->f_valuestack = f->f_localsplus + f->f_nlocals;

	/* stackless support */
	f->f_stackpointer = f->f_valuestack;
	_PyCode_GETCODEPTR(code, &f->f_next_instr);
	f->f_first_instr = f->f_next_instr;

	/* 
	  this hook allows us to keep objects alive which are needed during
	  the frame's lifetime. Functions which used to hold a ref to some
	  parameters while calling into eval can now instead return a prepared
	  frame and let it execute later. 
	 */
	f->f_hold_ref = NULL;

	/*  
	  status flags belong to the evaluator. eval_code2_loop
	  uses this field to determine how to resume when a frame
	  is re-entered.
	 */
	f->f_statusflags = 0;


	/* 
	  f_temp_val is a temporary slot, in order to return a value.
	  This field is only needed when the evaluator needs to return
	  a Py_UnwindToken *and* a value
	 */
	f->f_temp_val = NULL; 

	/* 
	  f_execute is not initialized here, since eval_code2_loop is
	  private to ceval.c (yet).
	 */

	/*
	  The call guard will be called by eval_code before a new frame
	  will be called. This allows to monitor the execution state.
	  Programs will usually not need this, unless they want to
	  implement special control flow. (I don't say the word but you know)
	 */
	f->f_callguard = NULL;

	/* Mostly used for debugging and some post-processing.
	  If this function returns non-zero, deallocation is skipped.
	 */
	f->f_dealloc = NULL;

	/* auxiliary memory. Some evaluators like map need to keep memory. */
	f->f_memory = NULL;

	/* general helpers for extensions. Initialized to signal no extension */
	f->f_node = NULL;
	f->f_co = NULL;
	f->f_coframes = NULL;

	/* registers 1, 2, 3 not initialized, free usage */

#ifdef FRAME_DEBUG
	++PyFrame_NumberOfFrames;
#endif
	return f;
}

/* 
  try to keep an object during a frame's lifetime.
  This can only fail if the f_hold_ref is occupied
  and no tuple can be allocated.
  Does never change the object's refcount.
*/

int
PyFrame_HoldReference(f, ob)
	PyFrameObject *f;
	PyObject *ob;
{
	if (f->f_hold_ref == NULL) {
		f->f_hold_ref = ob;
		return 0;
	}
	else {
		PyObject * hold = PyTuple_New(2);
		if (hold != NULL) {
			PyTuple_SET_ITEM(hold, 0, ob);
			PyTuple_SET_ITEM(hold, 1, f->f_hold_ref);
			f->f_hold_ref = hold;
			return 0;
		}
	}
	return -1;
}
	
	

/* Block management */

void
PyFrame_BlockSetup(PyFrameObject *f, int type, int handler, int level)
{
	PyTryBlock *b;
	if (f->f_iblock >= CO_MAXBLOCKS)
		Py_FatalError("XXX block stack overflow");
	b = &f->f_blockstack[f->f_iblock++];
	b->b_type = type;
	b->b_level = level;
	b->b_handler = handler;
}

PyTryBlock *
PyFrame_BlockPop(PyFrameObject *f)
{
	PyTryBlock *b;
	if (f->f_iblock <= 0)
		Py_FatalError("XXX block stack underflow");
	b = &f->f_blockstack[--f->f_iblock];
	return b;
}

/* Convert between "fast" version of locals and dictionary version */

void
PyFrame_FastToLocals(PyFrameObject *f)
{
	/* Merge fast locals into f->f_locals */
	PyObject *locals, *map;
	PyObject **fast;
	PyObject *error_type, *error_value, *error_traceback;
	int j;
	if (f == NULL)
		return;
	locals = f->f_locals;
	if (locals == NULL) {
		locals = f->f_locals = PyDict_New();
		if (locals == NULL) {
			PyErr_Clear(); /* Can't report it :-( */
			return;
		}
	}
	if (f->f_nlocals == 0)
		return;
	map = f->f_code->co_varnames;
	if (!PyDict_Check(locals) || !PyTuple_Check(map))
		return;
	PyErr_Fetch(&error_type, &error_value, &error_traceback);
	fast = f->f_localsplus;
	j = PyTuple_Size(map);
	if (j > f->f_nlocals)
		j = f->f_nlocals;
	for (; --j >= 0; ) {
		PyObject *key = PyTuple_GetItem(map, j);
		PyObject *value = fast[j];
		if (value == NULL) {
			PyErr_Clear();
			if (PyDict_DelItem(locals, key) != 0)
				PyErr_Clear();
		}
		else {
			if (PyDict_SetItem(locals, key, value) != 0)
				PyErr_Clear();
		}
	}
	PyErr_Restore(error_type, error_value, error_traceback);
}

void
PyFrame_LocalsToFast(PyFrameObject *f, int clear)
{
	/* Merge f->f_locals into fast locals */
	PyObject *locals, *map;
	PyObject **fast;
	PyObject *error_type, *error_value, *error_traceback;
	int j;
	if (f == NULL)
		return;
	locals = f->f_locals;
	map = f->f_code->co_varnames;
	if (locals == NULL || f->f_code->co_nlocals == 0)
		return;
	if (!PyDict_Check(locals) || !PyTuple_Check(map))
		return;
	PyErr_Fetch(&error_type, &error_value, &error_traceback);
	fast = f->f_localsplus;
	j = PyTuple_Size(map);
	if (j > f->f_nlocals)
		j = f->f_nlocals;
	for (; --j >= 0; ) {
		PyObject *key = PyTuple_GetItem(map, j);
		PyObject *value = PyDict_GetItem(locals, key);
		Py_XINCREF(value);
		if (value != NULL || clear) {
			Py_XDECREF(fast[j]);
			fast[j] = value;
		}
	}
	PyErr_Restore(error_type, error_value, error_traceback);
}

/* Clear out the free list */
/* and call another finalizer if set */

PyFrame_FiniChainFunc * PyFrame_FiniChain = NULL;

void
PyFrame_Fini(void)
{
	PyFrame_FiniChainFunc * hold = NULL;
	while (free_list != NULL) {
		PyFrameObject *f = free_list;
		free_list = free_list->f_back;
		PyObject_DEL(f);
	}
	/* Y2K0305 dispatchers are gone
	PyDispatcher_Fini();
	 */
	/* call other finalizers until chain is empty or same */
	while (PyFrame_FiniChain != NULL && PyFrame_FiniChain != hold) {
		hold = PyFrame_FiniChain;
		hold();
	}
}

/* 
stackless support:

The following code was borrowed from object.c, in order to create
another "None"-like object.

Unlike None, this object should never be visible anywhere,
it doen't use refcounts, it's only purpose is to be a valid
PyObject which indicates that a Python function was not called,
but wants the evaluator to be restarted.
*/

static PyObject *
unwind_repr(PyObject *op)
{
	return PyString_FromString(

		"The invisible unwind token. If you ever should see this,\n"

		"please report the error to tismer@tismer.com"

	);
}

/* dummy deallocator, just in case */
static void unwind_dealloc(PyObject *op) {
}

static PyTypeObject PyUnwindToken_Type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,
	"UnwindToken",
	0,
	0,
	(destructor)unwind_dealloc, /*tp_dealloc*/ /*should never be called*/
	0,		/*tp_print*/
	0,		/*tp_getattr*/
	0,		/*tp_setattr*/
	0,		/*tp_compare*/
	(reprfunc)unwind_repr, /*tp_repr*/
	0,		/*tp_as_number*/
	0,		/*tp_as_sequence*/
	0,		/*tp_as_mapping*/
	0,		/*tp_hash */
};

PyObject _Py_UnwindTokenStruct = {
	PyObject_HEAD_INIT(&PyUnwindToken_Type)
};

#include "opcode.h"

int
PyFrame_PreviousInstr (PyFrameObject *f, int lasti)
{
#define INSTR_OFFSET()	(next_instr - first_instr)
	unsigned char *first_instr = f->f_first_instr;
	unsigned char *next_instr = first_instr;
	int opcode = 0;
	lasti = abs(lasti);
	if (lasti > PyObject_Size(f->f_code->co_code))
		return -1;
	while (INSTR_OFFSET() < lasti) {
		opcode = *next_instr;
		next_instr += (HAS_ARG(opcode) ? 3 : 1);
	}
	lasti = INSTR_OFFSET() - (HAS_ARG(opcode) ? 3 : 1);
	return lasti;
}
