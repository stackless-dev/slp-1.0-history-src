#undef BUILD_TIMING
#undef CHECK_SAFETY

/* Continuation objects */

/*

  Major rewrite as of 990722. The implementation of
  continuations is now clear for me. Many comments on
  considerations have been removed, also lots of code.

  ----------------------------------------------------------------

  CT 990619 - 990722

  After lots of thought, I came up with this:
  Frames can be switched and treated like continuations in many
  cases. They actually are continuations, with one restriction.
  If a frame needs to be returned to more than once, we get the
  problem that we need more than one stack/IP state.

  An obvious way to achieve this would be to save the execution
  state within the callee instead to keep it in the caller.
  This would just be a major drawback since we would have to
  change lots of code, and most probably we would loose speed.
  Instead, I was searching for a better solution.
  Rule: Programs which don't need first class continuations
  should not suffer from an unnecessary feature.

  Solution to this problem:
  Whenever the current continuation is requested from Python code,
  we take the current frame and duplicate it. Duplication
  occours at the f_back pointer: A copy of the frame is spliced
  in there. This keeps all references correct.
  The former current frame is now transformed into a continuation
  frame.
  Continuation frames have a special f_execute entry.
  Their only purpose is to parameterize the true frame
  and run it.
  The copy-into-the-behind trick is always performed, when
  a continuation frame is run. At the same time, it is ensured
  that any true frame which is invoked by a continuation is
  itself pointing back to a continuation.
  By means of this, it appears as if the whole execution tree
  were built with continuations sliced between normal frames.
  But this happens only on demand, in a lazy manner.

  I'm sure I have reached the next level of madness by that.

  Christian Tismer
  June 19, 1999

  Addendum 990620: It was crucial to change the frame refcounting
  policy, in order to get this to work painlessly.
  Frames now have usually always a refcount of 1.
  On return, f_back is incref'd, then the current frame decref'd.

  990624 still hunting the last refcount faults since 5 days++.

  990625 refcount faults solved. I did not understand that
  a frame's stacksize field should never be written.
  See frameobject.c for Guido's comments.

  Added proper handling of recursion depth. It is simple
  and cheap: Save and restore the recursion level in the
  continuation frames.

  990626 it seems to be very stable now. But:

  -------------------------------------------------------------
  Version 0.2

  All the problems have been solved so far.
  After some testing and more reading about Scheme, I
  realized that the evaluation stack is a really bad thing!
  By copying stacks back, we are restoring old information
  which is wrong. We have to use the current information.
  Example: Loops carry their state on the stack. It is
  a bad idea to restore that using old values.

  Also, creating new push back frames all the time is not
  the best idea, since there should be only one continuation
  frame for every continuation entry point.

  In an ideal world, every entry point would not have a
  stack, but a number of registers which are associated
  with the program location. These registers should always
  carry current values. How can we do this?

  A complete solution seems to be impossible without
  rewriting half of the Python kernel. All we can do
  is trying to make sure that every program location
  which can be reached by its continuation uses the
  same, freshest version of data.

  990627 No, it *is* possible. We need a small change
  of eval_code to allow for something being called
  before a new frame is run. This means to add
  one new field to frames: f_callguard.

  Did it. It works. It is no complete solution, but at least
  "for" loops behave correctly in simple cases.
  The true solution is: Either turn the eval interpreter
  into a register machine, or do the following:
  On demand, a data flow analysis must be done for the
  stack in order to find the lifetime of stack variables.
  This is not too hard. I will not do this until I changed
  the overall concept again: Frame interpreters must become
  object by themselves. Necessary actions for continuations
  will then become methods of these objects.
  This is the way to go, for Version 0.3.

  -------------------------------------------------------------
  Version 0.3

  990710 dropped the extra hidden register detection mess.
  Let's forget about the above considerations.

  Stackless Python got a new mutable counter object for the
  for-loops, and we're done. Also reworked the detection
  of different dispatcher instances quite much.
  Going to release some stuff soon.

  990711 Now I *thought* it should also work to save
  continuations inside of nested dispatchers. Actually,
  it doesn't. My preventive continuation frame creation
  which I do for callers is broken, if an eval_code instance
  calls a function which doesn't generate unwind tokens.
  That means, eval_code continues to run this frame, although
  it had been turned into a continuation frame. This leads to
  funny effects and is bad.

  But now I realized what the *real* way is, and that it is
  so much easier than everything. We use the f_callguard,
  which now makes very much sense. The callguard becomes
  a handler which checks wether the current frame has a
  refcount of more than one. If so, then it is turned into
  a continuation frame, and we unwind with the pushed back
  frame.

  990712 This is done now. The callguard is now also used to
  check for function calls which did not unwind the stack.
  In other words: We can capture the continuations correctly,
  also if we are called recursively by an __init__ context.

  990722 Stackless Python is now always polling f_callguard.
  This makes the protection of multiple continuations very
  easy. The code of this module has been shrunk drastically.

  991029 My paper on Stackless Python and Continuation
  was luckily accepted, rating 1x10 and 3x9, and this with
  the unfinished, half-baked draft stuff. Guido had an insight
  with my problems with Felix' Leukaemia and gave me the chance.
  I should be quite happy, and yes I am, except for Felix!
  Now I have to prepare the final paper quickly. I started
  to use SP for all my projects, in order to get the bugs
  out. This was a good idea, see below. :-)

  991031 Corrected some minor bugs in the dispatcher object
  code (frameobject.c) and a silly refcount bug here in putcc.
  Sam's coroutine example now runs smoothly under PyWin 127.

  -------------------------------------------------------------
  Version 0.4

  Instead of finishing the paper, I could not resist to
  improve this module.

  991119 Corrected recursion depth. Had to be set it putcc.
  Protection is now set on every getcc, putcc and throw.
  Also needed to change aquiring dead frames since PythonWin
  might change your thread when run interactively.

  991121 Duplication now no longer occours at the f_back
  pointer, but with f_hold_ref. This gives us the advantage
  that a) the frame chain looks the same length as without
  continuation frames, b) we can have different returns
  for a single frame with multiple continuations.

  991121 Big renaming and new functions. Continuations are
  now callable objects. We simply return methods bound to
  frames. The former "getcc" and "putcc" have been renamed
  to be frame oriented.

  991122 Continuations can now be called as functions. This
  works even when calling a frame recursively from itself,
  by the new dynamic relinking feature of f_back.

  991123 The duplication at f_hold_ref was wrong since I'm
  not allowed to abuse this pointer. But the idea was kept,
  and yet another frame slot was added. Frames will finally
  need to be rewritten as more dynamic objects

  Growing memory with callcc has shown that we really need
  continuation objects now. Bound methods aren't enough,
  since they don't give us a chance to release the frame.
  Uhmm, not quite true. Copy the real one and modify
  then the given continuation. Sigh...

  991125 Now I have true continuation objects ready. They
  behave quite well. Still, one has to keep an eye on all
  the references which might be held by continuation
  objects. One must be careful to delete stuff early.

  -------------------------------------------------------------
  Version 0.5

  991126 (early morning)
  The final cut is there. Frame objects have yet
  another field to hold an arbitrary pointer. In our case,
  this pointer is a list of weak references to continuations
  which are active for a true frame. This allows to keep
  all co-objects fresh.

  99126 (day)
  A major redesign is due. After some sleep, I discovered
  that things are much easier if we can avoid growing
  frame chains at all. It is so simple:

  Every frame gets a weak slot for a co-object. The co-object
  is created when necessary. It removes itself from the frame
  when destructed.
  Whenever a new continuation is built, the co-object is
  adjusted to point to the real frame. Continuation frames do
  no longer link to frames, but to such a co-object.

  991130 Solved some trouble with moving targets. Co-objects
  were both internal dynamic nodes and exposed as user objects.
  This is wrong. The internal logic ensures that a dynamic node
  always monitors the current true frame. User links are *meant*
  as links to co-frames. But since we create them lazily, we
  end up with a mixture of internal and external links.
  This lead to situations where no continuation frame could be
  created since it had no user reference to spend.
  Solution: Whenever a user link is becoming an internal node,
  double the object. When the real frame becomes a co-frame,
  the doublette is spread over the frames.

  991203 A stable version with tiny control objects is ready.
  Neat, with a little overhead for the extra objects. I should
  take complete control of frames, cache them alone and use
  them liberally. There is no need to keep their type and all
  the fields, since in the context of a traceback, there are
  always true frames.

  991207 Again I could not resist and simplified the control
  objects very much. Also I clarified the frame transfer
  protocol, and the new leave_this_frame function does the real
  thing. Creation of co-frames is now avoided as much as possible.

  Timing of frame creation.

  2k0120 Release as version 0.6

  2k0131 corrected bug in co_update (thanks to M. Hudson), also
  added forgotten function continuation.current

  2k0216 Hairy oversight in protect_this_frame:
  It can happen that a running frame was turned into a
  co-frame while it was calling a method by a new
  interpreter instance. In this case we must unwind
  immediately. Frame protection needs to protect
  co-frames as well.

  2k0220 The end of the hairy story: It may happen that we are
  in a recursive call and the frame chain is changed meanwhile.
  This means we are running the wrong frame. Currently, there
  is no other chance that to adjust this back to the running
  frame. We raise an exception if the top of the frame chain
  wasn't just a continuation pointing to the real frame.

  This problem will vanish with the next dramatic change
  to Stackless Python: We will no longer keep track of
  dispatchers. Instead, we will just watch for the right
  frame to appear and then return to its interpreter.
  Frame compatibility will be no longer an issue.

  2k0226 Updated everything to today's Python CVS version.

  2k0227 Heureka, dispatcher objects are out!

  2k0228 This is version 0.7, not released.
  We are going to do a merger with Will Ware's microthreads!

  2k0229 New functions for easy popping off of a function
  with a ready-to-run continuation.
  return_current(mode=0)

  2k0302 introducing continuation.timeslice(), to support
  Will Ware's microthread activity.

  2k0305 after some heavy changes, microthreads are flying.
  The f_dispatcher field is also gone.
  Instead we use just a flag whether a frame is locked.

  2k0309 By default, continuations destroy themselves now
  after their jump. This makes it much easier to hold them
  in local variables. Behavior can be controlled via the
  charge attribute. uthread_reset() returns the continuation
  from the pending interrupt. This allows for a clean way
  to resume the scheduler loop.

  2k0311 A new strategy for co.caller:
  We now allow to assign None to a frame's caller
  (only if it is not on the current return path, of course :-) ).
  Whenever a continuation is thrown, f_back is checked, and
  if it is zero, we assign the current frame. This automagically
  turns a jump to a caller-less continuation into a call.

  Continuations without a caller behave like functions.

  Removed the charge concept. The AUTO_DESTRUCT flag is
  now always checked on a jump/call. Uthreads always set
  this flag.

  New protection layout for locked frames:
  A frame that caused an interpreter recursion has the
  lock flag set. It also has one extra refcount. This
  ensures that frame protection will try to build
  a continuation frame. If the frame lock is set, an
  exception is thrown.

  This makes version 0.8 now.

  y2k0326 Last change for this version: uthreads are
  locked whenever an exception is in progress.
  I had some delay, since I had to do a compression
  of the unicode database. I could not stand the
  code bloat. :-)
  Going to release this stuff now.

  -------------------------------------------------------------

  y2k0405 Just van Rossum has ported this stuff to the Mac!
  After hitting the recursion depth, there are two bottlenecks:
  An exception is created, which in turn creates a needless
  continuation frame. This happens for the whole big chain.
  Furthermore, for every frame on this chain, recursion
  depth was computed again. I changed both:
  Recusrion_depth is now taken from tstate, if we are *the*
  frame. protect_this_frame now checks for an error
  and increases its expected ref count, in order to avoid
  additional continuation frames.

  New idea: We can get rid of traceback objects by 
  incorporating them into frames. Guido said that this
  isn't prossible for orthodox Python, since more than
  one traceback can linger around for a frame.
  But: If we can make sure that frame protection does a
  frame copy when the embedded traceback is occupied,
  then it is possible. I'm just yet not sure how to
  do this without merging continuationmodule into
  the core.

  y2k0407 Problem with caller = None promiscuous mode.
  If the root frame is a jump target, things go wrong.
  Change: We record this with an IS_A_FUNCTION flag.

  y2k0408 found out why it is sometimes hard to get
  rid of references: f_hold_ref must be cleared whenever
  we leave a frame.

  y2k0409 moved getstack and getlocals into co-attributes.
  Allowed to modify the running chain as well, with few
  restrictions. Turned continuations with no caller
  into pure functions: They clean up their f_back after
  a call.

  y2k0909 Tracebacks are a bad thing at the moment. In a
  later version, they must be rewritten. Actually, there
  can be only one traceback object per thread, which
  makes it necessary to block microthreads when there
  is a traceback.
  The hard patch: clear exceptions on every continuation jump!

  This is version 0.9 now.
  -------------------------------------------------------------

  y2k1112
  Removed the f_extra alias f_data field. We do no
  longer carry things around.

  Introducing weak references.
  We want to get more freedom in passing continuations
  around as local variables, without creating cycles.

  20010105
  Added some more safety checks.
  Adopted new coding style for Python 2.0.
  New version is 1.0, but this module is going to be
  deprecated, soon! Microthreads will be implemented
  without continuations until end of June, 2001.

  20010418
  Small change: Clearing exceptions on curexc_type now. This
  seems to be more correct than checking exc_type .

  20010514
  Small clean-up for debug compilation.
*/

#define MODULE_VERSION_STR "1.0"
#define MODULE_VERSION_ID  _1_0


#ifndef MAX_RECURSION_DEPTH
/* #define MAX_RECURSION_DEPTH 10000 */
/* just for fun some more here: */
#define MAX_RECURSION_DEPTH 123456
#endif

#define Py_FRAME_EXTENSION
#include "Python.h"

#include "compile.h"

/* have to move this into a sys attribute, finally */

/* moved continuation data structures into frameobject.h,
   since debugging was much too complicated.
   */

#include "frameobject.h"

/* modes for continuations */

#define DO_CALL       1
#define RETURN_CO     2
#define AUTO_DESTRUCT 4
#define IS_A_FUNCTION 8

#define FLAG_SHIFT 4

/* #debugging data */

union any;

typedef struct smalltup {
	PyObject_VAR_HEAD
	union any *x0;
	union any *x1;
	union any *x2;
	union any *x3;
	union any *x4;
} smalltup;

typedef union any {
	PyIntObject i;
	PyStringObject s;
	PyFrameObject f;
	PyCoObject co;
	PyCoNode node;
	struct smalltup tup;
} any;

/* #debugging refs :-) */

#ifdef CHECK_SAFETY
static PyObject * _badthing = NULL;
static void my_decref(void * thing) {
	PyObject * ob = (PyObject*) thing;
	if (!ob ||ob->ob_refcnt <= 0) {
		_badthing = ob; /* place a breakpoint here */
	}
	Py_DECREF(ob);
}

static void my_xdecref(void * thing) {
	PyObject * ob = (PyObject*) thing;
	if (!ob) return;
	if (ob->ob_refcnt <= 0) {
		_badthing = ob; /* place a breakpoint here */
	}
	Py_DECREF(ob);
}

#undef Py_DECREF
#undef Py_XDECREF

#define Py_DECREF(x) my_decref(x)
#define Py_XDECREF(x) my_xdecref(x)

#endif

/* 
	the following macros assume that target fields are empty
	and source fields might be empty.
	Used for frame copying.
 */
	
#define COPY_FIELD(fnew, f, field) \
	{fnew->field = f->field;}

#define MOVE_FIELD(fnew, f, field) \
	{fnew->field = f->field; f->field = 0;}

#define COPY_OBREF(fnew, f, field) \
	{Py_XINCREF(f->field); COPY_FIELD(fnew, f, field);}

#define MOVE_OBREF(fnew, f, field) MOVE_FIELD(fnew, f, field);

#define DONT_TOUCH(fnew, f, field) ;

/* general object assignment */

/* be careful for side effects since we don't use a temp var! */

#define ASSIGN(target, source) \
	{Py_INCREF(source);Py_DECREF(target);(PyObject*)(target) = (PyObject*)(source);}

#define XASSIGN(target, source) \
	{Py_INCREF(source);Py_XDECREF(target);(PyObject*)(target) = (PyObject*)(source);}

#define ASSIGNX(target, source) \
	{Py_XINCREF(source);Py_DECREF(target);(PyObject*)(target) = (PyObject*)(source);}

#define XASSIGNX(target, source) \
	{Py_XINCREF(source);Py_XDECREF(target);(PyObject*)(target) = (PyObject*)(source);}

/* assigning with no incref */

#define ASSIGNO(target, source) \
	{Py_DECREF(target);(PyObject*)(target) = (PyObject*)(source);}

/* assigning with no decref */

#define OASSIGN(target, source) \
	{Py_INCREF(source); (PyObject*)(target) = (PyObject*)(source);}

#define XASSIGNO(target, source) \
	{Py_XDECREF(target);(PyObject*)(target) = (PyObject*)(source);}

/* assigning NULL not supported here :-) */

/* forward */
PyObject * throw_continuation(PyFrameObject *, PyObject *);

staticforward PyTypeObject PyContinuation_Type;
staticforward PyTypeObject PyCoNode_Type;

#define PyCo_Check(o) (o->ob_type==&PyContinuation_Type)

#define PyCoframe_Check(o) (o->f_execute==throw_continuation)

static PyCoObject * continuation_new(PyFrameObject*);

static PyCoNode * conode_new(PyFrameObject*);


PyObject * stack_to_tuple(PyFrameObject *f)
{
	PyObject **p = f->f_stackpointer;
	int size = p - f->f_valuestack;
	PyObject *ret = PyTuple_New(size);
	if (ret==NULL)
		return NULL;
	for ( ; size>0 ;) {
		PyObject *item = *--p;
		Py_INCREF(item);
		PyTuple_SET_ITEM(ret, --size, item);
	}
	return ret;
}


	
PyFrameObject * set_execution_state(PyFrameObject * f, PyFrameObject * ftrue, int back)
{
	PyObject ** fsp;
	PyObject * item;

#ifdef CHECK_SAFETY
	if (!PyCoframe_Check(f)) {
		PyErr_SetString (PyExc_SystemError, "wrong execution source frame");
		return NULL;
	}
	if (PyCoframe_Check(ftrue)) {
		PyErr_SetString (PyExc_SystemError, "wrong execution target frame");
		return NULL;
	}
#endif

	if (back) {
		PyFrameObject * hold = f;
		f = ftrue;
		ftrue = hold;
	}

	/* adjust f_back linkage */
	if (f->f_back != ftrue->f_back)
		XASSIGNX(ftrue->f_back, f->f_back);

	/* copy exception blocks */
	if (f->f_iblock > 0) {
		ftrue->f_iblock = f->f_iblock;
		memcpy(&(ftrue->f_blockstack), &(f->f_blockstack), f->f_iblock*sizeof(PyTryBlock));
	}

	/* clear the target stack */
	while (ftrue->f_stackpointer > ftrue->f_valuestack) {
		item = *--(ftrue->f_stackpointer);
		Py_XDECREF(item);
	}
	/* copy the stack */
	fsp = f->f_valuestack;
	while (fsp < f->f_stackpointer) {
		item = *(fsp++);
		Py_INCREF(item);
		*(ftrue->f_stackpointer++) = item;
	}

	/* copy additional info */
	COPY_FIELD(ftrue, f, f_lasti);
	COPY_FIELD(ftrue, f, f_lineno);
	/* instruction pointer */
	COPY_FIELD(ftrue, f, f_next_instr);
	/* copy auxiliary registers */
	COPY_FIELD(ftrue, f, f_statusflags);
	/* but not the registers */
	return ftrue; /* success flag */
}


/*
  The idea of build_continuation_frame is
  to take a frame and duplicate it at its f_back pointer.
  The execution pointer is changed to throw_continuation.
  Since the frame *becomes* the continuation, which later
  falls back to the true action, all references to the
  frame stay intact. 
  This is a "create continuation on demand" concept :-)
 */

int find_recursion_depth(PyFrameObject * f)
{
	/* 
	  we either count until the top of the chain, 
	  or until we find another continuation frame 
	  which tells us by its f_depth field. 
	*/
	int depth = 0;
	while (f != NULL) {
		if (PyCoframe_Check(f)) {
			depth += f->f_depth;
			break;
		}
		f = f->f_back;
		depth++;
	}
	return depth;
}

void init_recursion_depth(PyFrameObject * f)
{
	PyFrameObject * hold = f;
	int depth = 0;
	while (f != NULL) {
		f = f->f_back;
		++depth;
	}
	f = hold;
	while (f != NULL) {
		if (PyCoframe_Check(f))
			f->f_depth = depth;
		--depth;
		f = f->f_back;
	}
}

static int framecount = 0;
static int framecountmax = 0;
static int framesbuilt = 0;

/* extend frame deallocation to keep track of continuation frames */

int
my_frame_dealloc(PyFrameObject *f, PyFrameObject **freelist)
{
	if (f->f_node) {
		PyFrameObject **g = &f->f_node->frame->f_coframes;
		while (*g) {
			if (*g == f) {
				*g = f->f_coframes;
				break;
			}
			g = &(*g)->f_coframes;
		}
		--f->f_node->count;
#ifdef CHECK_SAFETY
		if (f->f_node->ob_refcnt <=0)
			_badthing = (PyObject*) f;
#endif
		Py_DECREF(f->f_node);
	}
	if (f->f_co)
		f->f_co->frame = NULL;
	/* inhibit referer counting in frame_dealloc */
	if (f->f_back) {
		Py_DECREF(f->f_back);
		f->f_back = NULL;
	}
	--framecount; 
	return 0;
}

/* monitor frame deallocation of push-back frames as well.
   y2k1112 this now also allows us to clean up co-objects
   when we are in weak reference mode.

 */

int
pushback_frame_dealloc(PyFrameObject *f, PyFrameObject **freelist)
{
	while (f->f_coframes) {
		PyFrameObject *cof = f->f_coframes;
		if (cof->f_co) {
			PyCoObject *co = cof->f_co;
			cof->f_co = NULL;
			ASSIGNO(co->frame, NULL);
		}
		f->f_coframes = cof->f_coframes;
	}
	return 0;
}

/* forward */

int protect_this_frame(PyFrameObject*, int); /* f, phase */


PyFrameObject * build_continuation_frame(PyFrameObject *f)
{
	PyFrameObject * fnew;
	int i;

	if (f==NULL)
		return NULL;

	if (PyCoframe_Check(f))
		return f;

	if (f->f_statusflags & FRAME_IS_LOCKED) {
		PyErr_SetString(PyExc_ValueError, 
			"cannot turn a locked frame into a continuation frame");
		return NULL;  /* y2k0312 protect frames with invalid state */
	}

	++framecount;
	++framesbuilt;
	if (framecount > framecountmax)
		framecountmax = framecount;

	fnew = PyFrame_New(f->f_tstate, f->f_code, f->f_globals, f->f_locals);
	if (fnew == NULL)
		return NULL ;

	/* y2k0410 counting referrers */
	if (fnew->f_back)
		--fnew->f_back->f_referers; /* from frame_new */
	/* f_back was from frame_new but with no incref */
	COPY_OBREF(fnew, f, f_back);
	DONT_TOUCH(fnew, f, f_code);		/* from frame_new */
	DONT_TOUCH(fnew, f, f_builtins);	/* from frame_new */
	DONT_TOUCH(fnew, f, f_globals);		/* from frame_new */
	DONT_TOUCH(fnew, f, f_locals);		/* from frame_new */
	DONT_TOUCH(fnew, f, f_valuestack);	/* from frame_new */
	COPY_OBREF(fnew, f, f_trace);
	if (f->f_exc_type) {
		/* y2k1009 moving these instead?
		COPY_OBREF(fnew, f, f_exc_type);
		COPY_OBREF(fnew, f, f_exc_value);
		COPY_OBREF(fnew, f, f_exc_traceback);
		*/
		MOVE_OBREF(fnew, f, f_exc_type);
		MOVE_OBREF(fnew, f, f_exc_value);
		MOVE_OBREF(fnew, f, f_exc_traceback);
	}
	DONT_TOUCH(fnew, f, f_tstate);		/* from frame_new */
	COPY_FIELD(fnew, f, f_lasti);
	COPY_FIELD(fnew, f, f_lineno);	
	COPY_FIELD(fnew, f, f_restricted);
	if (f->f_iblock > 0) {
		COPY_FIELD(fnew, f, f_iblock);
		memcpy(&(fnew->f_blockstack), &(f->f_blockstack), 
			f->f_iblock*sizeof(PyTryBlock));
	}
	DONT_TOUCH(fnew, f, f_nlocals);		/* from frame_new */
	DONT_TOUCH(fnew, f, f_stacksize);	/* from frame_new */
	/* note that f_stacksize can be larger. never touch ! */
	DONT_TOUCH(fnew, f, f_awaited);
	DONT_TOUCH(fnew, f, f_first_instr);	/* from frame_new */
	COPY_FIELD(fnew, f, f_next_instr);
	DONT_TOUCH(fnew, f, f_stackpointer);/* from frame_new */

	/* copy the stack */
	{
		PyObject ** fsp = f->f_valuestack;
		while (fsp < f->f_stackpointer) {
			PyObject * item = *(fsp++);
			Py_INCREF(item);
			*(fnew->f_stackpointer++) = item;
		}
	}

	COPY_FIELD(fnew, f, f_statusflags);
	MOVE_OBREF(fnew, f, f_hold_ref);
	DONT_TOUCH(fnew, f, f_temp_val);	/* transient value */
	COPY_FIELD(fnew, f, f_execute);		/* copy executor */
	f->f_execute = throw_continuation;	/* and put ours */
	MOVE_FIELD(fnew, f, f_callguard);
	f->f_callguard = protect_this_frame;	/* 2k0218 better safe than sorry */
	DONT_TOUCH(fnew, f, f_dealloc);
	f->f_dealloc = my_frame_dealloc;
	MOVE_FIELD(fnew, f, f_memory);
	MOVE_FIELD(fnew, f, f_node);		/* alias f_ext1 */
	DONT_TOUCH(fnew, f, f_co);			/* alias f_ext2 */
	DONT_TOUCH(fnew, f, f_coframes);	
	COPY_FIELD(fnew, f, f_depth);		/* alias f_reg1 */
	COPY_FIELD(fnew, f, f_age);			/* alias f_reg2 */
	COPY_FIELD(fnew, f, f_reg3);

	for (i=0; i<f->f_nlocals; i++) {
		MOVE_OBREF(fnew, f, f_localsplus[i]);
	}

	/* Y2k0405 avoid deep counting when this is the current frame */
	if (f == f->f_tstate->frame)
		f->f_depth = f->f_tstate->recursion_depth;
	else
		f->f_depth = find_recursion_depth(fnew);

	/** the new co-object stuff.
		do we have a controller already?
	 */

	if (!fnew->f_node) {
		if (!conode_new(fnew))
			return NULL;
	}
	else {
		PyCoNode * node = fnew->f_node;
		node->frame = fnew;
		Py_DECREF(f);
	}

	f->f_node = fnew->f_node;
	Py_INCREF(f->f_node);
	++fnew->f_node->count;
	fnew->f_coframes = f;

#ifdef CHECK_SAFETY
	if (f->f_node->frame == f) {
		_badthing = (PyObject*)f;
	}
#endif

	fnew->f_dealloc = pushback_frame_dealloc;

	/* sorry, we do have to change frame dealloc to decrement this */

	return f;
}


/* 
  990701 After my frame deallocation was corrected to
  also clean up stacks, I realized that also putcc
  is not so trivial, since it is run in a context where
  the executor wants to finish its frame, but putcc
  might deallocate it too early.

  One could of course walk around this by creating
  an extra frame which has a callback which...

  But we have this callguard pointer in frames already.
  This is the way to go!
  Whenever a frame is about to be left finally with
  an unwind token, we let it generate a callback
  which destroys it immediately before returning to
  the dispatcher. Phew :-)

  991127 moved this up, just in case. protect_this_frame,
  when called when not necessary, might need to leave
  the frame with a refcount of 1.
*/


/* 991203 allow to update a continuation with the running frame */

int update_continuation(PyFrameObject *f, int phase)
{
	PyFrameObject *fco = f->f_tstate->frame;
#ifdef CHECK_SAFETY
	if (!(fco && PyFrame_Check(fco) && PyCoframe_Check(fco)
		&& fco->f_node == f->f_node)) {
		PyErr_SetString (PyExc_SystemError, 
			"failure during continuation update");
		return -1;
	}
#endif
	set_execution_state(fco, f, 1);
	f->f_callguard = NULL;
	f->f_temp_val = NULL;
	f->f_tstate->frame = f;
	Py_DECREF(fco);
	return 0;
}

/*
	A frame will check wether it can be reached twice by a
	return jump before it is run. If so, it creates a
	continuation frame from itself, puts the pushed back
	version on the frame stack and jumps off.
	This is, the frame stays in place but is turned into a
	continuation frame, with a copy of its original version
	pushed into b_back. Therefore I call them push-back frames.
	This action is allowed since all pointers to our frame
	stay intact. Note that this push-back step makes an explicit
	continuation from our frame. Note also that this situation
	does not occour often and will be avoided most of the time.

	phase 0: frame is about to leave with the unwind token
	phase 1: frame is running the interpreter loop
	phase 2: frame has just done a true function call

  991206 The criteria has improved a little:
  Execution of the frame gives one secure refcount.
  Existence of a node adds another secure refcount.
  A continuation object which adds another ref is at
  the moment considered unsafe, since it needs to
  be turned into a continuation soon. I don't know
  if and how to change this. Later...

  In order to have consistent decisions about refcount, we
  no longer switch the current frame from a function, but
  leave refcounts intact and use a callback.
*/

int protect_this_frame(PyFrameObject *f, int phase)
{
	PyFrameObject *ftrue;
	
	static int lastrefcnt;
	lastrefcnt = f->ob_refcnt;

#ifdef CHECK_SAFETY
	/* auwaa: */
	if (!PyFrame_Check(f)) {
		_badthing = (PyObject*) f;
	}
#endif

	/** 991031 for now, we just remove the guard since
		we are done. But I think we should later use
		a method chain instead which allows for more
		actions like tracing, switching nano-threads
		every n opcodes and such.
	*/

	/* y2k0303 make sure to protect back as well */
	if (f->f_back && !f->f_back->f_callguard)
		f->f_back->f_callguard = protect_this_frame;

	if (PyCoframe_Check(f)) {
		/* we have been turned into a co-frame while
		we were waiting for some method to return. This
		can happen when a recursive interpreter instance
		was created. I hope it suffices to unwind in this
		situation. It *can* be necessary to do such a check
		after each possible recursion, but let's see...
		*/
		if (phase == 0)
			return 0;
		return -42;
	}

	if (f != f->f_tstate->frame) {
		/* Y2K0220 this was very hard to detect.
		like above: We are returning from a recursion,
		but meanwhile the frame chain has changed.
		Our caller is running the wrong frame. 
		
		Seems to be solved!
		After redesigning the dispatcher behavior,
		it does no longer occour.
		We keep the check and treat it as an error,
		let's see...
		*/
		PyErr_SetString(PyExc_ValueError,
			"cannot change running frame in recursive call");
		return -1;
	}

	f->f_callguard = NULL;

	if (!f->f_referers) { 
		/* cannot be reached twice by a return */
		return 0;
	}

	if(!build_continuation_frame(f)) {
		f->f_callguard = protect_this_frame;
		return -1;
	}

	/*  y2k0409 see if we have a continuation which acts
		as a true function. We then have to release f_back.
	 */
	if(f->f_co && f->f_co->flags & IS_A_FUNCTION && f->f_back) {
		--f->f_back->f_referers;
		ASSIGNO(f->f_back, NULL);
	}

	/** On exit, we just protect. That means this frame
		becomes a restartable continuation frame, but
		we simply return to eval_loop, which is about to
		return immediately and will not care about what
		kind of frame this is. Tricky? A bit, but my...
	*/
	if (phase == 0)
		return 0;

	/* on entry, we cause a restart */
	/* we are called with 
	   phase == 1 which is "between opcodes"
	   phase == 2 which is "after function call"
	   but the action appears to be identical.
	*/
	ftrue = f->f_node->frame;
	MOVE_OBREF(ftrue, f, f_temp_val);

#ifdef CHECK_SAFETY
	/* this may not happen: */
	if (!ftrue->f_tstate->frame) {
		_badthing = (PyObject*) ftrue;
	}
	if (PyThreadState_GET() != ftrue->f_tstate) {
		_badthing = (PyObject*) f;
	}
#endif
	ASSIGN(ftrue->f_tstate->frame, ftrue);
	return -42; /* dispatch the new frame */
	
	/** Try to understand this trick.
		(Yes this *is* a trick,	but a good one:)
		eval_loop calls f_callguard. f_callguard returns with
		-42 to tell eval_loop "hey, please stop immediately".
		Meanwhile f_callguard has taken the snapshot of the
		current frame, turned it into a continuation frame,
		and has put the copy in f_back onto the frame stack.
		That means: The next time when eval_loop is run,
		it will continue exactly where it was when we took
		the snapshot.
	*/
}


/** This is the real transfer now! 
	We release all unneeded references early
	and then carry on.
*/

int leave_this_frame(PyFrameObject *f, int phase)
{
	PyThreadState * tstate = f->f_tstate;
	PyFrameObject * target = tstate->frame;
	PyObject * temp_val = target->f_temp_val;
	PyFrameObject * coframe = NULL;

#ifdef CHECK_SAFETY
	if (PyThreadState_GET() != f->f_tstate) {
		_badthing = (PyObject*) f;
	}
#endif

	/*  simply set protection for the source
		and decref it. Transfer the execution before, since
		we might loose all other handles.
	 */

	/*  Py_INCREF(target);
		y2k0308 this is now done early by internal_put_call.
		It is necessary to do it early since the continuation
		might vanish right after that, killing the frame.
	 */
	f->f_callguard = protect_this_frame;
	Py_DECREF(f);

	/* skip a throw_continuation call */
	if (PyCoframe_Check(target)) {
		f = target;
		target = f->f_node->frame;
		target->f_tstate = tstate;
		coframe = f; /* need to delay tstate setting */
		Py_INCREF(target);
		/* Py_DECREF(f);
		   can't do that, need a ref to coframe y2k0311
		 */
	}
	else {
		f = target;
	}

	/* 20010104 make sure that the target is not locked! */
	if (target->f_statusflags & FRAME_IS_LOCKED) {
		PyErr_SetString(PyExc_ValueError, 
			"target of continuation frame has been locked again");
		/* we cannot run either frame. So try the target's back */
		tstate->frame = target->f_back;
		Py_XINCREF(target->f_back);
		Py_DECREF(target);
		return -1;
	}

	/** there should be at most two refs to the target.
		Otherwise we have a node *and* a current co or maybe
		something else like a return.
	*/
	if (target->f_referers) {
		if(!build_continuation_frame(target))
			return -1;
		f = target;
		target = f->f_node->frame;
		target->f_tstate = tstate;
		Py_INCREF(target);
		Py_DECREF(f);
	}
	target->f_callguard = NULL;
	/* y2k0311 now maintain an extra reference
	if (coframe && !set_execution_state(coframe, target, 0))
		return -1;
	 */
	if (coframe) {
		f = coframe;
		if (!set_execution_state(f, target, 0)) {
			Py_DECREF(f);
			return -1;
		}
		else {
			/*  y2k0409 see if we have a continuation which acts
				as a true function. We then have to release f_back.
			 */
			if(f->f_co && f->f_co->flags & IS_A_FUNCTION && f->f_back) {
				--f->f_back->f_referers;
				ASSIGNO(f->f_back, NULL);
			}
			Py_DECREF(f);
		}
	}

	/* everything has been set. */
	target->f_temp_val = temp_val;
	tstate->frame = target;
	if (phase == 0)
		return 0;
	return -42;
}


PyObject * throw_continuation(PyFrameObject *f, PyObject *passed_retval)
	/* passing a function return value back in */
{
	/* 
	  we are the executor of a continuation frame.
	  Since all possible continuations are armed
	  by f_callguard, we have not much to do
	  (compare old version :)
	*/
	PyFrameObject *ftrue;

	ftrue = f->f_node->frame;
	f->f_tstate = ftrue->f_tstate = PyThreadState_GET();

	/* 20010104 make sure that the target is not locked! */
	if (ftrue->f_statusflags & FRAME_IS_LOCKED) {
		PyErr_SetString(PyExc_ValueError, 
			"target of continuation frame has been locked again");
		return NULL;
	}

	/* ensure that back is saved and protected */
	ftrue = set_execution_state(f, ftrue, 0);
#ifdef CHECK_SAFETY	
    if (ftrue==NULL) {
		PyErr_SetString (PyExc_SystemError, "broken continuation");
		Py_XDECREF(passed_retval);
		return NULL;
	} 
#endif
	OASSIGN(f->f_tstate->frame, ftrue);
	ftrue->f_callguard = protect_this_frame; /* ct y2k0220 */
	f->f_tstate->recursion_depth = f->f_depth;
	if(f->f_co && f->f_co->flags & IS_A_FUNCTION && f->f_back) {
		--f->f_back->f_referers;
		ASSIGNO(f->f_back, NULL);
	}
	Py_DECREF(f);
	return passed_retval;
}



void ensure_protected_chain(PyFrameObject *f)
{
	/* activate the callguard security for the whole chain */
	while (f != NULL && f->f_callguard==NULL) {
		f->f_callguard = protect_this_frame;
		f = f->f_back;
	}
}

void grab_foreign_frames(PyFrameObject *f)
{
	PyThreadState *tstate = PyThreadState_GET();
	/* adjust the thread state fields and protect */
	while (f != NULL) {
		f->f_tstate = tstate;
		f->f_callguard = protect_this_frame;
		f = f->f_back;
	}
}

/*
  getcc is no longer tricky. 
  Sam tried to fetch the current frame directly in the function.
  This is possible now, we just have to install the callguards properly.

  991128 this is no longer exposed but used internally only.
  991205 no longer does an incref or protection.
*/

static PyObject *
get_parent (PyObject *self, PyObject *args)
{
	PyFrameObject *f;
	PyThreadState *tstate = PyThreadState_GET();
	int level = 1;
#ifdef USE_PARSETUPLE
	if (!PyArg_ParseTuple (args, "|i:caller", &level)) {
		return NULL;
	} 
#else
	switch (args ? PyTuple_GET_SIZE(args) : 0) {
		PyObject * v;
	case 0: 
		break;
	case 1: 
		v = PyTuple_GET_ITEM(args, 0);
		if (PyInt_Check(v)) {
			level = PyInt_AS_LONG(v);
			break;
		}
	default:
		PyArg_ParseTuple (args, "|i:caller", &level);
		return NULL;
	}
#endif
	/* return a frame as a continuation (default: true caller) */
	f = tstate->frame;
	while (f != NULL && level--) 
		f = f->f_back;
	if (f==NULL) {
		PyErr_SetString (PyExc_ValueError, "parameter exceeds current nesting level");
		return NULL;
	}
	return (PyObject *) f;
}



/*****************************************************************

  co_object CT 991124

  A co_object is basically a wrapper around frames, very
  similar to bound method objects.

  The idea is to give fast access to frames, while staying
  able to replace frames. By accessing frames through co_objects,
  there is no problem to store such objects or their bound
  methods, since the frames can be changed dynamically.

******************************************************************/

/* co_object object implementation */

/*	991126
	A co-object is created for a frame only once, and only
	if needed. Every true frame needs to create a co-object
	before continuations can be attached.
	The count fields says how many continuations we control.
	If count is 0, this co-object is not a master controller.

	When a user requests a co-object, the already assigned
	object is returned.
	There is a special case: If the user requests a co-object
	of a true frame, he will get one with count=0. If that
	frame becomes a continuation later, the co-object will
	stay there. In other words: Only auto-created co-objects
	are continuation controllers, which move along with the
	real frame. This is determined by count.
*/


static PyCoObject *co_free_list = NULL;
static PyCoNode *node_free_list = NULL;

static PyFrame_FiniChainFunc * PyFrame_FiniChain_save = NULL;

/* a conode is created without any references */

static PyCoNode *
conode_new(PyFrameObject * frame)
{
	PyCoNode *op;
	if (!frame)
		return NULL;
	op = node_free_list;
	if (op != NULL) {
		node_free_list = (PyCoNode *)(op->frame);
		op->ob_type = &PyCoNode_Type;
		_Py_NewReference((PyObject*)op);
	}
	else {
		op = PyObject_NEW(PyCoNode, &PyCoNode_Type);
		if (op == NULL)
			return NULL;
	}
	op->count = 0;
	op->ob_refcnt = 0;
	op->weakflag = 0;
	op->frame = frame;
	frame->f_node = op;
	op->freelist = NULL;
	return op;
}

/* simple access object */

static PyCoObject *
continuation_new(PyFrameObject * frame)
{
	PyCoObject *op;
	if (!frame)
		return NULL;
	op = co_free_list;
	if (op != NULL) {
		co_free_list = (PyCoObject *)(op->frame);
		op->ob_type = &PyContinuation_Type;
		_Py_NewReference((PyObject*)op);
	}
	else {
		op = PyObject_NEW(PyCoObject, &PyContinuation_Type);
		if (op == NULL)
			return NULL;
	}
	op->frame = frame;
	op->flags = 0;
	Py_INCREF(frame);
	frame->f_co = op;
	return op;
}


static PyObject *
builtin_caller(PyObject *self, PyObject *args)
{
	PyCoObject *co;
	PyFrameObject *f = (PyFrameObject*)self; /* internal fast call */
	if(!f) {
		f = (PyFrameObject*)get_parent(NULL, args);
		if (!f) return NULL;
	}

	co = f->f_co;
	if (co) {
		Py_INCREF(co);
		return (PyObject*) co;
	}
	co = continuation_new(f);
	if (!co)
		return NULL;
	++co->frame->f_referers;
	ensure_protected_chain(f);
	return (PyObject *)co;
}

static PyObject *
builtin_current(PyObject *self, PyObject *args)
{
	PyObject *ret;
	int flags = 0;
	switch (args ? PyTuple_GET_SIZE(args) : 0) {
		PyObject * v;
	case 0: 
		break;
	case 1: 
		v = PyTuple_GET_ITEM(args, 0);
		if (PyInt_Check(v)) {
			flags = PyInt_AS_LONG(v);
			break;
		}
	default:
		PyArg_ParseTuple (args, "|i:current", &flags);
		return NULL;
	}
	ret = builtin_caller((PyObject*)PyThreadState_GET()->frame, NULL);
	if (ret) {
		PyCoObject *co = (PyCoObject *) ret;
		co->flags = flags;
	}
	return ret;
}

static PyObject *
builtin_return_current(PyObject *self, PyObject *args)
{
	PyObject *ret;
	PyThreadState *tstate = PyThreadState_GET();
	ret = builtin_current(NULL, args);
	--tstate->recursion_depth;
	if (ret) {
		PyFrameObject *f = tstate->frame;
		PyFrameObject *fb = f->f_back;
		if (fb) {
			PyCoObject *co = (PyCoObject*)ret;
			tstate->frame = fb;
			fb->f_temp_val = ret;
			Py_INCREF(fb); /* y2k0308 do it early */
			f->f_callguard = leave_this_frame;
			if (f->f_back)
				--f->f_back->f_referers;
			ASSIGNO(f->f_back, NULL);
			co->flags |= IS_A_FUNCTION;
			return Py_UnwindToken;
		}
	}
	return ret;
}

static PyObject *
builtin_return_value(PyObject *self, PyObject *args)
{
	PyObject *ret;
	PyThreadState *tstate = PyThreadState_GET();
	PyFrameObject *f = tstate->frame;
	PyFrameObject *fb = f->f_back;

	switch (PyTuple_GET_SIZE(args)) {
	case 0:
		ret = Py_None;
		break;
	case 1:
		ret = PyTuple_GET_ITEM(args, 0);
		break;
	default:
		ret = args;
	}
	Py_INCREF(ret);
	--tstate->recursion_depth;
	if (fb) {
		tstate->frame = fb;
		fb->f_temp_val = ret;
		Py_INCREF(fb); /* y2k0308 do it early */
		f->f_callguard = leave_this_frame;
		return Py_UnwindToken;
	}
	return ret;
}


static void
co_dealloc(PyCoObject *co)
{
	if (co->frame) {
		--co->frame->f_referers;
		co->frame->f_co = NULL;
		Py_DECREF(co->frame);
	}
	co->frame = (void *)co_free_list;
	co_free_list = co;
}


static void
node_dealloc(PyCoNode *node)
{
	node->frame->f_node = NULL;
	if (!node->weakflag)
		Py_DECREF(node->frame);
	node->frame = (void *)node_free_list;
	node_free_list = node;
}

/* y2k001010
	temporary hack to handle exceptions *somehow*.
	We have to rewrite them later.
	Right now, we clear exceptions on every jump.
*/

static void
reset_exc_info(PyThreadState *tstate)
{
	PyFrameObject *frame;
	PyObject *tmp_type, *tmp_value, *tmp_tb;
	frame = tstate->frame;
	if (frame->f_exc_type != NULL) {
		/* This frame caught an exception */
		tmp_type = tstate->exc_type;
		tmp_value = tstate->exc_value;
		tmp_tb = tstate->exc_traceback;
		Py_XINCREF(frame->f_exc_type);
		Py_XINCREF(frame->f_exc_value);
		Py_XINCREF(frame->f_exc_traceback);
		tstate->exc_type = frame->f_exc_type;
		tstate->exc_value = frame->f_exc_value;
		tstate->exc_traceback = frame->f_exc_traceback;
		Py_XDECREF(tmp_type);
		Py_XDECREF(tmp_value);
		Py_XDECREF(tmp_tb);
		/* For b/w compatibility */
		PySys_SetObject("exc_type", frame->f_exc_type);
		PySys_SetObject("exc_value", frame->f_exc_value);
		PySys_SetObject("exc_traceback", frame->f_exc_traceback);
	}
	tmp_type = frame->f_exc_type;
	tmp_value = frame->f_exc_value;
	tmp_tb = frame->f_exc_traceback;
	frame->f_exc_type = NULL;
	frame->f_exc_value = NULL;
	frame->f_exc_traceback = NULL;
	Py_XDECREF(tmp_type);
	Py_XDECREF(tmp_value);
	Py_XDECREF(tmp_tb);
}

static PyObject *
internal_put_call (PyCoObject *self, PyObject *args, int do_call, int return_co)
{
	PyFrameObject * k, * f;
	PyObject * v = NULL;
	PyThreadState *tstate = PyThreadState_GET();

	if (!self->frame) {
		PyErr_SetString (PyExc_TypeError, "continuation has no frame object");
		return NULL;
	}
#ifdef USE_PARSETUPLE
	if (!PyArg_ParseTuple (args, "|O:jump/call", &v)) {
		return NULL;
	}
#else
	switch (PyTuple_GET_SIZE(args)) {
	case 0: v = Py_None;
		break;
	case 1: v = PyTuple_GET_ITEM(args, 0);
		break;
	default:
		/* y2k0328 on behalf of Sam Rushing, we allow
		   for multiple arguments now. I hope this
		   will not cause prosecution by the Spanish Inquisition,
		   since this feature just has been deprecated for
		   list.append() and friends. But, in this case we have
		   no ambiguity, so let's go...
		PyArg_ParseTuple (args, "|O:jump/call", &v);
		return NULL;
		*/
		v = args;
	}
#endif
	k = self->frame;
	f = tstate->frame;

	if (k->f_tstate != tstate)
		grab_foreign_frames(k);

	/* auto-call if there is no f_back  y2k0311 */
	/* but not for the root frame y2k0407 */
	/* always if it is a function y2k0410 */
	if (self->flags & IS_A_FUNCTION)
		do_call = DO_CALL;

	if (do_call) {
		k = build_continuation_frame(k);
		if (++k->f_depth > MAX_RECURSION_DEPTH)
			init_recursion_depth(k);
		if (k->f_depth > MAX_RECURSION_DEPTH) {
			PyErr_SetString(PyExc_RuntimeError,
					"Maximum recursion depth exceeded (continuation)");
			return NULL;
		}
		if(!k) 
			return NULL;
		f->f_callguard = protect_this_frame;
		if (k->f_back)
			--k->f_back->f_referers;
		XASSIGN(k->f_back, f);
		++f->f_referers;
	}
	else if (tstate->curexc_type)  /* 20010418 correct? was exc-type */
		reset_exc_info(tstate);
	/* v can be NULL. This means the continuation throws an exception */
	Py_XINCREF(v);
	if (return_co && v != NULL) {
		PyObject * co = builtin_caller((PyObject*)f, NULL);
		PyObject *tup = PyTuple_New(2);
		if (!(co&&tup)) return NULL; /* must be mem error */
		((PyCoObject*)co)->flags = self->flags >> FLAG_SHIFT;
		PyTuple_SET_ITEM(tup, 0, co);
		PyTuple_SET_ITEM(tup, 1, v);
		v = tup;
	}
	k->f_temp_val = v;
	tstate->frame = k;
	tstate->recursion_depth = find_recursion_depth(k);
	f->f_callguard = leave_this_frame;
	Py_INCREF(k); /* y2k0308 do this early */

	if (self->flags & AUTO_DESTRUCT) {
		/* inhibit unnecessary protection */
		--k->f_referers;
		/* destroy the continuation */
		k->f_co = NULL;
		ASSIGNO(self->frame, NULL);
	}
	return Py_UnwindToken;
}

static PyObject *
co_jump(PyCoObject * self, PyObject * args)
{
	return internal_put_call (self, args, 0, 0);
}

static PyObject *
co_call(PyCoObject * self, PyObject * args)
{
	return internal_put_call (self, args, 1, 0);
}

static PyObject *
co_jump_cv(PyCoObject * self, PyObject * args)
{
	return internal_put_call (self, args, 0, 1);
}

static PyObject *
co_call_cv(PyCoObject * self, PyObject * args)
{
	return internal_put_call (self, args, 1, 1);
}


static PyObject *
co_update (PyCoObject *self, PyObject *args)
{
	PyFrameObject *f, *ftrue;
	PyObject * v = NULL;

	if (!PyFrame_Check (self->frame)) {
		PyErr_SetString (PyExc_TypeError, "continuation has no frame object");
		return NULL;
	}
#ifdef USE_PARSETUPLE
	if (!PyArg_ParseTuple (args, "|O:update", &v)) {
		return NULL;
	}
#else
	switch (PyTuple_GET_SIZE(args)) {
	case 0: v = Py_None;
		break;
	case 1: v = PyTuple_GET_ITEM(args, 0);
		break;
	default:
		PyArg_ParseTuple (args, "|O:update", &v);
		return NULL;
	}
#endif
	if (PyFrame_Check(self->frame) && self->frame->f_node) {
		/* if there is no node, nothing to update */
		f = self->frame;
		ftrue = f->f_node->frame;
		if (f != ftrue) {
			if (ftrue != PyThreadState_GET()->frame)
				set_execution_state(f, ftrue, 1);
			else {
				/* for catching the current frame, we must do it at the
				next cycle. We quickly abuse tstate to pass the frame. */
				ftrue->f_tstate->frame = f;
				ftrue->f_callguard = update_continuation;
				Py_INCREF(f);
			}
		}
	}
	if (!v)
		v = Py_None;
	Py_INCREF(v);
	return v;
}


static PyMethodDef co_methods[] = {
	{"jump",			(PyCFunction)co_jump, 1,
	 "jump(value=None) -- jump to the continuation with an optional\n"
	 "value. The value will appear as the return value of the\n"
	 "target expression.\n"
	 "\n"
	 "Continuation objects are callable.\n"
	 "jump is the default action if a continuation is called.\n"
	 "The flags attribute allows to override this behavior:\n"
	 "flags & 1 -- do a call              [see call, call_cv]\n"
	 "flags & 2 -- return (cont, value)   [see jump_cv, call_cv]"
	 "flags & 4 -- self-destruct after the jump/call"
	},
	{"call",			(PyCFunction)co_call, 1,
	 "call(value=None) -- like jump, but leave the current frame\n"
	 "on the stack. This is an optimization to eliminate tiny\n"
	 "stub methods, which would end with a jump."
	},
	{"jump_cv",			(PyCFunction)co_jump_cv, 1,
	 "jump_cv(value=None) -- like jump, but the target receives\n"
	 "a tuple (cont, value) where cont is the source continuation."
	},
	{"call_cv",			(PyCFunction)co_call_cv, 1,
	 "call_cv(value=None) -- like call, see jump_cv.\n"
	},
	{"update",			(PyCFunction)co_update, 1,
	 "update(value=None) -- update a continuation with the current frame\n"
	 "state. This is especially useful when dealing with the current\n"
	 "frame. After an assignment\n"
	 "  cont = continuation.current(mode=0)\n"
	 "the continuation is still in that position. After\n"
	 "  k = cont.update(value)\n"
	 "value is passed through to k, but cont is now adjusted to\n"
	 "this assignment and ready to accept new values."
	},
	{NULL,			NULL}		/* sentinel */
};

/**

  Continuation attributes.

  The user always has access to two co-objects which make up
  one structure for him. The data field exists only once for
  him, the other one will allow different use for us.

  frame			the real frame, always exists
  coframe		continuation frame which might exist
  cocount		number of coframes
  refcount		number of references to the co-object
  data			the one visible data object

  There is no other way to access these objects.
  */

static PyObject *
co_getattr(PyCoObject *co, char *name)
{
	PyCoNode *node;
	PyObject *ret;
	if (co->frame == NULL) {
		PyErr_SetString(PyExc_AttributeError, name);
		return NULL;
	}
	node = co->frame->f_node;

restart:
	switch (name[0]) {
	case '_':
		if (strcmp(name, "__members__") == 0) {
			return Py_BuildValue("[sssssssssssssss]", 
				"frame", "coframe", "continuations", "caller", "refcount", 
				"cocount", "link", "__doc__", "default", "__name__", 
				"locals", "stack", "flags", "referers", "referers_co");
		}
		if (strcmp(name, "__doc__") == 0) {
			return PyString_FromString("I am a continuation object, "
				"Deleting 'link' kills me.");
		}
		if (strcmp(name, "__name__") == 0) {
			name = "default";
			goto restart;
		}
		break;
	case 'f':
		if (strcmp(name, "frame") == 0) {
			if (PyCoframe_Check(co->frame))
				ret = (PyObject *)node->frame;
			else
				ret = (PyObject *)co->frame;
			Py_INCREF(ret);
			return ret;
		}
		if (strcmp(name, "flags") == 0) {
			return PyInt_FromLong(co->flags);
		}
		break;
	case 'l':
		if (strcmp(name, "link") == 0) {
			return PyString_FromString(
				PyCoframe_Check(co->frame) ? "coframe" : "frame" );
		}
		if (strcmp(name, "locals") == 0) {
			PyFrameObject *f = co->frame;
			if (f) {
				if (PyCoframe_Check(f))
					f = f->f_node->frame;
				PyFrame_FastToLocals(f);
				ret = f->f_locals;
				f->f_locals = NULL;
			}
			else {
				ret = Py_None;
				Py_INCREF(ret);
			}
			return ret;
		}
		break;
	case 's':
		if (strcmp(name, "stack") == 0) {
			if (co->frame) {
				ret = stack_to_tuple(co->frame);
			}
			else {
				ret = Py_None;
				Py_INCREF(ret);
			}
			return ret;
		}
		break;
	case 'd':
		if (strcmp(name, "default") == 0) {
			char * name;
			switch (co->flags & 3) {
			case 0: 
				name = "jump";
				break;
			case 1:
				name = "call";
				break;
			case 2:
				name = "jump_cv";
				break;
			case 3:
				name = "call_cv";
				break;
			}
			return PyString_FromString(name);
		}
		break;
	case 'c':
		if (strcmp(name, "caller") == 0) {
			PyObject * arg;
			arg = (PyObject*) co->frame->f_back;
			if (!arg) {
				Py_INCREF(Py_None);
				return Py_None;
			}
			return builtin_caller(arg, NULL);
		}
		if (strcmp(name, "cocount") == 0) {
			return PyInt_FromLong(node ? node->count : 0);
		}
		if (strcmp(name, "coframe") == 0) {
			if(PyCoframe_Check(co->frame))
				ret = (PyObject*)co->frame;
			else 
				ret = Py_None;
			Py_INCREF(ret);
			return ret;
		}
		if (strcmp(name, "continuations") == 0) {
			int i;
			PyFrameObject *next;
			PyObject *ret;
			if (node)
				next = node->frame;
			else
				next = co->frame;
			ret = PyTuple_New(node->count+1);
			if (!ret) return ret;
			for (i=0; i<= node->count; i++) {
				/* continuations *and* the real frame */
				if (next->f_co) {
					Py_INCREF(next->f_co);
				}
				else {
					continuation_new(next);
				}
				PyTuple_SET_ITEM(ret, i, (PyObject*)next->f_co);
				next = next->f_coframes;
			}
			return ret;
		}
		break;
	case 'r':
		if (strcmp(name, "referers") == 0) {
			return PyInt_FromLong(node ? node->frame->f_referers : co->frame->f_referers);
		}
		if (strcmp(name, "referers_co") == 0) {
			return PyInt_FromLong(node ? co->frame->f_referers : -1);
		}
		if (strcmp(name, "refcount") == 0) {
			return PyInt_FromLong(co->ob_refcnt);
		}
		break;
	}
	return Py_FindMethod(co_methods, (PyObject *)co, name);
}

static int
is_caller_of(PyFrameObject *er, PyFrameObject *ee)
{
	int caller, callee, level;
	if (!er || !ee)
		return 0;
	caller = find_recursion_depth(er);
	callee = find_recursion_depth(ee);
	for (level = callee; level > caller; level--)
		ee = ee->f_back;
	if (PyCoframe_Check(ee))
		ee = ee->f_node->frame;
	if (PyCoframe_Check(er))
		er = er->f_node->frame;
	if (er == ee)
		return callee-caller+1;
	return 0;
}

static int
contains_locked_frames(PyFrameObject *f, int len)
{
	while (len--) {
		if (f->f_statusflags & FRAME_IS_LOCKED)
			return 1;
		if (PyCoframe_Check(f) &&
				f->f_node->frame->f_statusflags & FRAME_IS_LOCKED)
			return 1;
		if (f->f_back)
			f=f->f_back;
	}
	return 0;
}

void
ensure_true_root(PyFrameObject *f)
{
	while(f) {
		if (!f->f_back) {
			/* is there a continuation that thinks this is no root */
			if (f->f_co)
				f->f_co->flags &= ~IS_A_FUNCTION;
		}
		f = f->f_back;
	}
}

static int
co_setattr(PyCoObject *co, char *name, PyObject *value)
{
	if (co->frame == NULL) {
		PyErr_SetString(PyExc_AttributeError, name);
		return -1;
	}

	switch (name[0]) {
	case 'l':
		if (strcmp(name, "link") == 0) {
			if (value && value != Py_None) {
				PyErr_SetString(PyExc_TypeError, 
					"you can only use del with the link field");
				return -1;
			}
			co->frame->f_co = NULL;
			ASSIGNO(co->frame, NULL);
			return 0;
		}
		break;
	case 'f':
		if (strcmp(name, "flags") == 0) {
			if (!PyInt_Check(value)) {
				PyErr_SetString(PyExc_TypeError, 
					"assigned flags must be integer");
				return -1;
			}
			co->flags = PyInt_AS_LONG(value);
			return 0;
		}
		break;
	case 'c':
		if (strcmp(name, "caller") == 0) {
			PyFrameObject *f, *k;
			PyFrameObject *ftrue;
			if (! ( value && PyCo_Check(value) || value == Py_None || !value) ) {
				PyErr_SetString(PyExc_TypeError, 
					"assigned object must be a continuation or None");
				return -1;
			}
			f = co->frame;
			if (!f) {
				PyErr_SetString(PyExc_ValueError, 
					"assigned continuation object must be bound");
				return -1;
			}

			f->f_callguard = protect_this_frame;
			f = build_continuation_frame(f);
			if (!f)
				return -1;

			if (value == Py_None ||!value) {
				if (is_caller_of(f, f->f_tstate->frame)) {
					PyErr_SetString(PyExc_ValueError, 
						"you cannot cut a running frame chain");
					return -1;
				}
				if (f->f_back) {
					--f->f_back->f_referers;
					ASSIGNO(f->f_back, NULL);
				}
				ftrue = f->f_node->frame;
				/* see if we may get rid of more */
				if (!ftrue->f_referers) {
					if (ftrue->f_back) {
						--ftrue->f_back->f_referers;
						ASSIGNO(ftrue->f_back, NULL);
					}
				}
				co->flags |= IS_A_FUNCTION;
				return 0;
			}
			k = ((PyCoObject*)value)->frame;
			if (!PyFrame_Check(k)) {
				PyErr_SetString(PyExc_ValueError, 
					"target continuation object must be bound");
				return -1;
			}

			if (is_caller_of(f, k)) {
				PyErr_SetString(PyExc_ValueError, 
					"your caller assignment would be a cycle");
				return -1;
			}
			{
				/*  y2k0409 relaxing the caller assignment a little.
					Modifying the running frame chain gives me a bit
					of headache. I think it makes sense to allow it
					only if there are no locked frames in between.
					It is probably bad if locked frames are not
					processed in the order which is dictated by
					recursive interpreter calls.
				*/
				PyFrameObject *current = PyThreadState_GET()->frame;
				int len = is_caller_of(k, current);
				if (len && contains_locked_frames(current, len)) {
					PyErr_SetString(PyExc_ValueError, 
						"your caller assignment tries to bypass locked frames");
					return -1;
				}
				if (len)
					ensure_true_root(k);
			}

			k->f_callguard = protect_this_frame;
			k = build_continuation_frame(k);
			if(!k) 
				return -1;
			if (f->f_back)
				++f->f_back->f_referers;
			XASSIGN(f->f_back, k);
			++k->f_referers;
			ftrue = f->f_node->frame;
			if (!ftrue->f_referers) {
				if (ftrue->f_back)
					--ftrue->f_back->f_referers;
				XASSIGN(ftrue->f_back, k);
				++k->f_referers;
			}
			co->flags &= ~IS_A_FUNCTION;

			return 0;
		}
		break;
	}
	PyErr_SetString(PyExc_AttributeError, name);
	return -1;
}

static PyObject *
co_repr(PyCoObject *co)
{
	char buf[200];
	if (!co->frame)
		sprintf(buf, "<Continuation object (killed)>");
	else {
		sprintf(buf,
			"<Continuation object of %.80s() frame at %lx>",
			PyString_AS_STRING(co->frame->f_code->co_name),
			(long)co->frame);
	}
	return PyString_FromString(buf);
}

static PyObject*
co_jump_direct(PyCoObject *co, PyObject *args, PyObject *kw)
{
	if(kw) {
			PyErr_SetString(PyExc_TypeError,
				   "this function takes no keyword arguments");
			return NULL;
	}
	return internal_put_call(co, args, co->flags & DO_CALL, co->flags & RETURN_CO);
}


static PyTypeObject PyContinuation_Type = {
	PyObject_HEAD_INIT(0)
	0,
	"Continuation",
	sizeof(PyCoObject),
	0,
	(destructor)co_dealloc, /*tp_dealloc*/
	0,		/*tp_print*/
	(getattrfunc)co_getattr, /*tp_getattr*/
	(setattrfunc)co_setattr,	/*tp_setattr*/
	0,		/*tp_compare*/
	(reprfunc)co_repr,		/*tp_repr*/
	0,		/*tp_as_number*/
	0,		/*tp_as_sequence*/
	0,		/*tp_as_mapping*/
	0,		/*tp_hash*/
	(ternaryfunc)co_jump_direct,		/*tp_call*/
};

static PyTypeObject PyCoNode_Type = {
	PyObject_HEAD_INIT(0)
	0,
	"ContinuationNode",
	sizeof(PyCoNode),
	0,
	(destructor)node_dealloc, /*tp_dealloc*/
	0,		/*tp_print*/
	0,		/*tp_getattr*/
	0,		/*tp_setattr*/
	0,		/*tp_compare*/
	0,		/*tp_repr*/
	0,		/*tp_as_number*/
	0,		/*tp_as_sequence*/
	0,		/*tp_as_mapping*/
	0,		/*tp_hash*/
	0,		/*tp_call*/
};


/* Clear out the free list */

void
PyContinuation_Fini(void)
{
	while (co_free_list) {
		PyCoObject *v = co_free_list;
		co_free_list = (PyCoObject *)(v->frame);
		PyMem_DEL(v);
	}
	while (node_free_list) {
		PyCoNode *v = node_free_list;
		node_free_list = (PyCoNode *)(v->frame);
		PyMem_DEL(v);
	}
}

static PyObject *
builtin_framecount(PyObject *self, PyObject *args)
{
	if (!PyArg_NoArgs(args))
		return NULL;
	return Py_BuildValue("(iii)", framecount, framecountmax, framesbuilt);
}

static PyObject *
builtin_clearcount(PyObject *self, PyObject *args)
{
	PyObject * ret = NULL;
	if (!PyArg_NoArgs(args))
		return NULL;
	ret = builtin_framecount(NULL, NULL);
	framecount = 0;
	framecountmax = 0;
	framesbuilt = 0;
	return ret;
}

/***** microthread support */
static PyObject *
builtin_uthread_lock(PyObject *self, PyObject *flag)
{
	PyObject *ret;
	PyThreadState *tstate = PyThreadState_GET();
	if (! (flag && PyInt_Check(flag)) ) {
		PyErr_SetString(PyExc_TypeError, "uthread_lock needs exactly one integer");
		return NULL;
	}
	ret = PyInt_FromLong(tstate->uthread_lock);
	tstate->uthread_lock = PyInt_AS_LONG(flag);
	return ret;
}

/*
  time slicing idea:
  A scheduler function in Python does an endless loop and
  calls builtin_timeslice with a continuation to be run.
  The return value is the continuation of the suspended
  uthread.

  What we have to do:
  1) Figure out the current uthread and save it away.
  2) grab the passed continuation and prepare our caller
     (which is meant to be the scheduler) by leave_this_frame
  3) install a callback function in the tstate. eval_loop
     will call it after the uthread_ticker interval.

*/

static PyObject *
uthread_interrupt(PyCoObject *self, PyObject *args)
{
	/* we are called via a method wrapper, so self contains
	   the continuation of our scheduler.
	   There is nothing to do than to catch the current state
	   of the running task and to pass it to the scheduler
	   continuation.
	 */
	PyObject *task, *ret;
	PyObject *arg = PyTuple_New(1);
	PyThreadState *tstate = PyThreadState_GET();
	if (!arg) return NULL;
	task = builtin_caller((PyObject*)tstate->frame, NULL);
	/* if this goes wrong, the scheduler will get an exception */
	PyTuple_SET_ITEM(arg, 0, task);
	ret = internal_put_call(self, arg, 0, 0);
	Py_DECREF(arg);
	return ret;
}

static PyMethodDef interrupt_methods[] = {
  {"interrupt",	(PyCFunction)uthread_interrupt, 0, },
  {NULL,		NULL}		/* sentinel */
};

static PyObject *
builtin_timeslice(PyObject *self, PyObject *args)
{
	PyObject *interrupt, *target, *scheduler, *arg, *ret;
	PyThreadState *tstate = PyThreadState_GET();
	PyCoObject *co;
	int ticks = 100;
	if (!PyArg_ParseTuple (args, "O!|i:timeslice", &PyContinuation_Type, &target, &ticks))
		return NULL;

	scheduler = builtin_caller((PyObject*)tstate->frame, NULL);
	if (!scheduler) return NULL;
	interrupt = PyCFunction_New(&interrupt_methods[0], scheduler);
	if (!interrupt) {
		Py_DECREF(scheduler);
		return NULL;
	}
	Py_DECREF(scheduler);
	Py_XDECREF(tstate->uthread_func);
	tstate->uthread_func = NULL;
	arg = PyTuple_New(0);
	co = (PyCoObject*) target;
	co->flags |= AUTO_DESTRUCT;
	ret = internal_put_call(co, arg, 0, 0);
	Py_DECREF(arg);
	if (ret) {
		tstate->uthread_func = interrupt;
		tstate->uthread_ticker += ticks;
		tstate->uthread_lock = 0;
	}
	else
		Py_DECREF(interrupt);
	return ret;
}

static PyObject *
builtin_uthread_reset(PyObject *self, PyObject *args)
{
	PyThreadState *tstate = PyThreadState_GET();
	PyObject *ret;
	ret = tstate->uthread_func;
	tstate->uthread_func = 0;
	tstate->uthread_ticker = 0;
	tstate->uthread_lock = 1;
	if (ret) {
		PyCoObject *co = (PyCoObject*)((PyCFunctionObject*) ret)->m_self;
		Py_INCREF (co);
		co->flags = AUTO_DESTRUCT;
		Py_DECREF(ret);
		ret = (PyObject*) co;
	}
	else {
		ret = Py_None;
		Py_INCREF(ret);
	}
	return ret;
}


static PyMethodDef continuation_methods[] = {
  {"caller",			(PyCFunction)builtin_caller, 1,
   "caller(n=1) -- get the n'th parent of the current frame as a continuation"
  },
  {"current",			(PyCFunction)builtin_current, 1,
   "current(mode=0) -- get the current continuation. See the update method to make\n"
   "use of this. See the module doc for mode."
  },
  {"return_current",	(PyCFunction)builtin_return_current, 1,
   "return_current(mode=0) -- get the current continuation and return it to the calling\n"
   "function. See the module doc for mode."
  },
  {"return_value",	(PyCFunction)builtin_return_value, 1,
   "return_value(value=None) -- return to the caller, like the return statement.\n"
   "This is experimental stuff."
  },
  {"framecount",		(PyCFunction)builtin_framecount, 0,
   "framecount() -- debugging: give counter tuple (current, max, total) of created\n"
   "continuation frames."
  },
  {"clearcount",		(PyCFunction)builtin_clearcount, 0,
   "clearcount() -- debugging: like framecount, but resets counters to 0."
  },
  {"uthread_lock",		(PyCFunction)builtin_uthread_lock, 0,
   "uthread_lock(flag) -- set microthread locking status and return current one.\n"
   "This is an atomic operation for building higher level locks upon.\n"
   "flag must be integer, locking if non-zero."
  },
  {"uthread_reset",		(PyCFunction)builtin_uthread_reset, 1,
  "uthread_reset(sendto=None) -- clear any pending interrupt. Returns the scheduler\n"
  "continuation or None.\n"
  "The optional sendto parameter specifies the distance to a caller, who should exchange\n"
  "his data attribute with the scheduler."
  },
  {"timeslice",			(PyCFunction)builtin_timeslice, 1,
   "remainder = timeslice(newtask, ticks=100) -- Schedule a new\n"
   "continuation for about 'ticks' ticks.\n"
   "Tries to set an interrupt after the specified number of ticks, but\n"
   "the ticker accuracy is dominated by the system ticker, usually 10.\n"
   "'remainder' is the continuation of 'newtask', after it has run for\n"
   "the allowed number of ticks."
  },
  {NULL,		NULL}		/* sentinel */
};


/* Initialization function for the module (*must* be called initcontinuation) */

/* version check */

int check_version(void)
{
	char shortver[100];
 	const char *sys = Py_GetVersion();
	char * space = strchr(sys, ' ');
	if (space) {
		int len = space-sys;
		strncpy(shortver, sys, len % 100);
		shortver[len] = '\0';
		sys = shortver;
	}
	if (strncmp(sys, PY_VERSION, strlen(PY_VERSION)+1) != 0) {
		/* do not use Py_FatalError here since PythonWin can't handle it */
		char buf[400];
		sprintf(buf, "Incompatible Python version %.100s found.\n"
			"I was compiled for Stackless Python version %.100s", sys, PY_VERSION);
		PyErr_SetString (PyExc_ImportError, buf);
		return 0;
	}
	return 1;
}

static char continuation_doc[] =
"Continuation objects for Stackless Python.\n\
\n\
You can get the continuation of a frame on the stack by using\n\
continuation.caller(n=1) -- get my caller, 0 acts like current\n\
continuation.current(mode=0)   -- get myself.\n\
See the update() method to make sense of the latter.\n\
\n\
For an easy way to return a runnable continuation in\n\
the middle of a function call, use\n\
continuation.return_current(mode=0)  -- return myself as callable\n\
\n\
mode consists of two flags that define the default action of a call.\n\
mode	behaves like method\n\
   0	jump\n\
   1	call\n\
   2	jump_cv\n\
   3	call_cv\n\
\n\
Micro-thread support:\n\
see timeslice(), uthread_lock(), uthread_reset()\
";

#ifdef _MSC_VER
_declspec(dllexport)
#endif
void
initcontinuation()
{
	PyObject *m, *d;

	if (!check_version()) {
		return;
	}

	/* Create the module and add the functions */
	PyContinuation_Type.ob_type = &PyType_Type;
	PyCoNode_Type.ob_type = &PyType_Type;
	m = Py_InitModule3("continuation", continuation_methods, continuation_doc);

	/* Add some symbolic constants to the module */
	d = PyModule_GetDict(m);
#if 0
	ErrorObject = PyErr_NewException("continuation.error", NULL, NULL);
	PyDict_SetItemString(d, "error", ErrorObject);
#endif

	/* add our cleanup proc */
	PyFrame_FiniChain_save = PyFrame_FiniChain;
	PyFrame_FiniChain = PyContinuation_Fini;
}

/* end of module */
