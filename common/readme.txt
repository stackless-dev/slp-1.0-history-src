Build Instructions

This information only applies for the Windows platforms,
using Visual Studio 6. At the moment, I do not support
building for Unixen, but it should be simple to do.

Stackless Python replaces several modules of Standard Python.
The directories are named like in the distribution.

Option One (simple):
Simply copy the files into the distribution directories.
This should also be alll you have to do for Unix.

Option Two (tricky):
This what I do. The standard distribution is not changed at
all. It is reached through the SLPY project, but include
paths are changed so that the compiler first looks into my
directories. That is the reqason why I needed to put a copy
of python.h into my include directory, to force a lookup
in the proper order.

The project file for Visual Studio assumes the following:
Standard Python is in                   d:/python/python-1.5.2
Stackless and continuation stuff is in  d:/python/spc/src

If you can manage to use this layout, you can compile
right away.

Have fun, and let me know whether you find this software useful.

Christian Tismer, January 2000
Member of the
Mission Impossible 5oftware Team

Last update: y2k1010
